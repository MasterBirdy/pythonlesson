import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const HamburgerButton = ({ test }) => {
    return (
        <div className="icon-holder" onClick={test}>
            <FontAwesomeIcon icon={faBars} color="white" size="2x"></FontAwesomeIcon>
        </div>
    );
};

export default HamburgerButton;
