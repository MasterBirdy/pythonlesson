import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTimes, faChartBar } from "@fortawesome/free-solid-svg-icons";
import { parseDate } from "../../utils/helpers";
import { TitleBoxDouColorListEntry, TitleBoxDouColorListText, IconsContainer } from "../../elements/components";
import styled from "styled-components";

const AssignmentListEntry = ({ assignmentID, dueDate, name }) => {
    const { date, AMPM, hour, minutes, time, realMonth } = parseDate(dueDate);

    return (
        <TitleBoxAssignmentListEntry>
            <div>
                <TitleBoxDouColorListText className={date < Date.now() ? "due-assignment" : ""}>
                    {name}
                </TitleBoxDouColorListText>
                <AssignmentLine className={date < Date.now() ? "due" : ""}>
                    Due: {realMonth}/{date.getDate()}/{date.getFullYear()}, {time}
                </AssignmentLine>
            </div>
            <IconsContainer>
                <Link to={`/viewassignmentstats/${assignmentID}`} title="See Assignment Completion">
                    <FontAwesomeIcon icon={faChartBar} size="lg"></FontAwesomeIcon>
                </Link>
                <Link to={`/editassignment/${assignmentID}`} title="Edit Assignment">
                    <FontAwesomeIcon icon={faEdit} size="lg"></FontAwesomeIcon>
                </Link>
                <Link to={`/deleteassignment/${assignmentID}`} title="Delete Assignment">
                    <FontAwesomeIcon icon={faTimes} size="lg"></FontAwesomeIcon>
                </Link>
            </IconsContainer>
        </TitleBoxAssignmentListEntry>
    );
};

export const TitleBoxAssignmentListEntry = styled(TitleBoxDouColorListEntry)`
    display: flex;
    justify-content: space-between;
    &:last-child {
        border: 0;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
`;

export const AssignmentLine = styled(TitleBoxDouColorListText)`
    font-size: 1.2rem;
    color: #666;
    &.due {
        color: rgba(131, 35, 35, 0.856);
    }
`;

export default AssignmentListEntry;
