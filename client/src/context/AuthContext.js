import React, { useState, createContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

const AuthContext = createContext();
const { Provider } = AuthContext;

const AuthProvider = ({ children }) => {
    const userInfoTemp = JSON.parse(localStorage.getItem("userInfo")) || {};
    const expiresAtTemp = JSON.parse(localStorage.getItem("expiresAt")) || 0;

    const history = useHistory();
    const [userInfo, setUserInfo] = useState(userInfoTemp);
    const [expiresAt, setExpiresAt] = useState(expiresAtTemp);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(false);
    }, []);

    const setTheAuth = (uInfo, eAt) => {
        localStorage.setItem("expiresAt", eAt);
        localStorage.setItem("userInfo", JSON.stringify(uInfo));
        setUserInfo(uInfo);
        setExpiresAt(eAt);
    };

    const logout = () => {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("expiresAt");
        setUserInfo({});
        setExpiresAt(0);
        axios({
            method: "POST",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/logout`,
            withCredentials: true,
        })
            .then((res) => {
                if (res.status === 200) {
                    history.push("/login");
                } else {
                    throw new Error("Error with logging out");
                }
            })
            .catch((err) => {
                console.log(err);
            });
    };

    const isAuthenticated = () => {
        if (!userInfo || !expiresAt) {
            return false;
        }

        return new Date().getTime() / 1000 < expiresAt;
    };

    return <Provider value={{ userInfo, loading, setTheAuth, logout, isAuthenticated }}>{children}</Provider>;
};

export { AuthContext, AuthProvider };
