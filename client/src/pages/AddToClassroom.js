import React, { useState, useEffect } from "react";
import axios from "axios";
import AddStudentForm from "../components/classroom/AddStudentForm";
import AddedStudentForm from "../components/classroom/AddedStudentForm";
import AltButton from "../components/ui/AltButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { makeToast, makeInfoToast } from "../utils/helpers";
import {
    faAngleLeft,
    faAngleDoubleLeft,
    faAngleRight,
    faAngleDoubleRight,
    faQuestion,
} from "@fortawesome/free-solid-svg-icons";
import Button from "../components/ui/Button";
import { useHistory } from "react-router-dom";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    IconsContainer,
    Container,
} from "../elements/components/";
import { below } from "../elements/utilities";
import styled from "styled-components";

const AddToClassroom = () => {
    const [backupAvailableStudents, setBackupAvailableStudents] = useState([]);
    const [availableStudents, setAvailableStudents] = useState([]);

    const [chosenAvailableStudents, setChosenAvailableStudents] = useState([]);
    const [selectedStudents, setSelectedStudents] = useState([]);
    const [chosenSelectedStudents, setChosenSelectedStudents] = useState([]);
    const [teacherStudentList, setTeacherStudentList] = useState([]);
    const [grade, setGrade] = useState("");
    const [name, setName] = useState("");
    const history = useHistory();
    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/getstudentsofschool/`,
            withCredentials: true,
        })
            .then((res) => {
                if (res.status === 200) {
                    setBackupAvailableStudents(res.data.students);
                    setAvailableStudents(res.data.students);
                    setSelectedStudents(res.data.teacherStudents);
                    setTeacherStudentList(res.data.teacherStudents);
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const filteredStudents = availableStudents.filter((student) => {
        if (grade) {
            if (student.grade !== parseInt(grade)) return false;
        }
        return (student.first_name.toLowerCase() + " " + student.last_name.toLowerCase()).includes(name);
    });

    const singleArrowRightHandler = () => {
        const tempIDHolder = {};
        chosenAvailableStudents.forEach((key) => {
            tempIDHolder[key] = 1;
        });
        let tempSelectedStudents = [];
        let tempAvailableStudents = availableStudents.filter((student) => {
            if (tempIDHolder[student.id]) {
                tempSelectedStudents.push(student);
            }
            return !tempIDHolder[student.id];
        });
        setSelectedStudents((prevState) => [...prevState, ...tempSelectedStudents]);
        setAvailableStudents(tempAvailableStudents);
    };

    const singleArrowLeftHandler = () => {
        const tempIDHolder = {};
        chosenSelectedStudents.forEach((key) => {
            tempIDHolder[key] = 1;
        });
        let tempAvailableStudents = [];
        let tempSelectedStudents = selectedStudents.filter((student) => {
            if (tempIDHolder[student.id]) {
                tempAvailableStudents.push(student);
            }
            return !tempIDHolder[student.id];
        });
        setSelectedStudents(tempSelectedStudents);
        setAvailableStudents((prevState) => [...prevState, ...tempAvailableStudents]);
    };

    const doubleArrowLeftHandler = () => {
        setSelectedStudents([]);
        setAvailableStudents((prevState) => [...prevState, ...selectedStudents]);
    };

    const doubleArrowRightHandler = () => {
        setAvailableStudents([]);
        setSelectedStudents((prevState) => [...prevState, ...availableStudents]);
    };

    const submitHandler = async () => {
        let difference = teacherStudentList
            .filter((x) => !selectedStudents.includes(x))
            .concat(selectedStudents.filter((x) => !teacherStudentList.includes(x)));
        let addingStudents = difference.filter((x) => selectedStudents.includes(x));
        let removingStudents = difference.filter((x) => !selectedStudents.includes(x));
        try {
            const res = await axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/modifystudentlist`,
                data: {
                    addingStudents,
                    removingStudents,
                },
                withCredentials: true,
            });
            if (res.status === 200) {
                setBackupAvailableStudents(res.data.students);
                setAvailableStudents(res.data.students);
                setSelectedStudents(res.data.teacherStudents);
                setTeacherStudentList(res.data.teacherStudents);
                makeToast("Student list changes made!");
            }
        } catch (err) {
            console.log(err);
        }
    };

    const resetHandler = () => {
        setAvailableStudents(backupAvailableStudents);
        setSelectedStudents(teacherStudentList);
        setName("");
        setGrade("");
    };

    return (
        <Container>
            <AddToClassroomGrid>
                <AddToClassroomGridItem>
                    <AddToClassroomTitleBox maxWidth="40rem">
                        <TitleBoxTitleContainer>
                            <TitleBoxTitle>Add Students</TitleBoxTitle>
                            <IconsContainer>
                                <FontAwesomeIcon
                                    icon={faQuestion}
                                    onClick={() =>
                                        makeInfoToast(
                                            `ℹ️ This shows all students registered at your school who are currently not assigned to a classroom. Use the arrow buttons to add/remove students to your classroom, then press submit.`
                                        )
                                    }
                                ></FontAwesomeIcon>
                            </IconsContainer>
                        </TitleBoxTitleContainer>
                        <TitleBoxContainer>
                            <AddStudentForm
                                filteredAvailableStudents={filteredStudents}
                                chosenAvailableStudents={chosenAvailableStudents}
                                setChosenAvailableStudents={setChosenAvailableStudents}
                                name={name}
                                setName={setName}
                                grade={grade}
                                setGrade={setGrade}
                            ></AddStudentForm>
                        </TitleBoxContainer>
                    </AddToClassroomTitleBox>
                </AddToClassroomGridItem>
                <AltButtonHolder>
                    <AltButton aFunction={singleArrowLeftHandler}>
                        <FontAwesomeIcon icon={faAngleLeft}></FontAwesomeIcon>
                    </AltButton>
                    <AltButton aFunction={singleArrowRightHandler}>
                        <FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>
                    </AltButton>
                    <AltButton aFunction={doubleArrowLeftHandler}>
                        <FontAwesomeIcon icon={faAngleDoubleLeft}></FontAwesomeIcon>
                    </AltButton>
                    <AltButton aFunction={doubleArrowRightHandler}>
                        <FontAwesomeIcon icon={faAngleDoubleRight}></FontAwesomeIcon>
                    </AltButton>
                </AltButtonHolder>
                <AddToClassroomGridItem>
                    <AddToClassroomTitleBox maxWidth="40rem">
                        <TitleBoxTitleContainer>
                            <TitleBoxTitle>Student List</TitleBoxTitle>
                            <IconsContainer>
                                <FontAwesomeIcon
                                    icon={faQuestion}
                                    onClick={() =>
                                        makeInfoToast(
                                            `ℹ️ This shows all the students that are currently in your classroom roster.`
                                        )
                                    }
                                ></FontAwesomeIcon>
                            </IconsContainer>
                        </TitleBoxTitleContainer>
                        <TitleBoxContainer>
                            <AddedStudentForm
                                selectedStudents={selectedStudents}
                                chosenSelectedStudents={chosenSelectedStudents}
                                setChosenSelectedStudents={setChosenSelectedStudents}
                            ></AddedStudentForm>
                        </TitleBoxContainer>
                    </AddToClassroomTitleBox>
                </AddToClassroomGridItem>
                <AddToClassroomButtonHolder>
                    <div>
                        <Button aFunction={submitHandler}>Submit</Button>
                        <Button aFunction={resetHandler}>Reset</Button>
                    </div>
                    <Button aFunction={() => history.push("/classroom")}>Back</Button>
                </AddToClassroomButtonHolder>
            </AddToClassroomGrid>
        </Container>
    );
};

export const AddToClassroomGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 10rem 1fr;
    font-family: "PT Sans", sans-serif;
    font-weight: 400;
    margin-top: 2rem;
    ${below.s`
        grid-template-columns: 1fr;
    `}
`;

export const AddToClassroomGridItem = styled.div`
    display: flex;
    justify-content: center;
    ${below.s`
    &:not(:last-child) {
        margin-bottom: 1.25rem;
    }
    `}
`;

export const AddToClassroomTitleBox = styled(TitleBox)`
    width: 100%;
`;

export const AddToClassroomButtonHolder = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 1.33rem auto 0;
    width: 100%;
    max-width: 38rem;
    button:not(:last-child) {
        margin-right: 1rem;
    }
    ${below.s`
    order: 1;
    margin: 0 auto;
    `}
`;

export const AltButtonHolder = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    button:not(:last-child) {
        margin-bottom: 1rem;
    }

    ${below.s`
    flex-direction: row;
    margin-bottom: 1.25rem;

    button:not(:last-child) {
    margin-bottom: 0rem;
    margin-right: 1rem;
        }
    `}
`;

export default AddToClassroom;
