import React, { useState, useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { withRouter, Redirect } from "react-router-dom";
import axios from "axios";
import LoginForm from "../components/login/LoginForm";
import { Link } from "react-router-dom";
import { passwordRegex } from "../utils/helpers";
import {
    Container,
    TitleBox,
    TitleBoxContainer,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    Errors,
} from "../elements/components";
import styled from "styled-components";

//eslint-disable-next-line
const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

const Login = (props) => {
    const auth = useContext(AuthContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errorMessages, setErrorMessages] = useState([]);
    const [errors, setErrors] = useState({
        email: false,
        password: false,
    });

    const loginHandler = (e) => {
        e.preventDefault();
        if (validate()) {
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/login`,
                data: {
                    email,
                    password,
                },
                withCredentials: true,
            })
                .then((res) => {
                    if (res.status === 200) {
                        auth.setTheAuth(res.data.userInfo, res.data.expiresAt);
                    } else {
                        throw new Error("error");
                    }
                })
                .catch((err) => {
                    if (err.response) {
                        setErrorMessages((prevState) => [...prevState, err.response.data.err]);
                    }
                });
        }
    };

    const setEmailHandler = (email) => {
        setEmail(email);
    };

    const setPasswordHandler = (password) => {
        setPassword(password);
    };

    const validate = () => {
        const errorArray = [];
        const errorObject = {
            email: false,
            password: false,
        };
        if (!emailRegex.test(email)) {
            errorArray.push("You must put in a valid e-mail address!");
            errorObject.email = true;
        }
        if (!passwordRegex.test(password)) {
            errorArray.push("You must put in a valid password!");
            errorObject.password = true;
        }
        setErrorMessages(errorArray);
        setErrors(errorObject);
        return errorArray.length === 0;
    };

    if (auth.loading) {
        return <div className="loader"></div>;
    } else {
        if (auth.isAuthenticated()) {
            return <Redirect to="/profile"></Redirect>;
        } else {
            return (
                <Container>
                    <TitleBox marginTop="2" maxWidth="40rem">
                        <TitleBoxTitleContainer>
                            <TitleBoxTitle>Login</TitleBoxTitle>
                        </TitleBoxTitleContainer>
                        <TitleBoxContainer>
                            <LoginForm
                                email={email}
                                setEmail={setEmailHandler}
                                password={password}
                                setPassword={setPasswordHandler}
                                submit={loginHandler}
                                errors={errors}
                            ></LoginForm>
                            <LinksHelper>
                                <Link to="/forgotpassword">Forgot password?</Link>
                            </LinksHelper>
                        </TitleBoxContainer>
                    </TitleBox>
                    {errorMessages.length > 0 && (
                        <Errors>
                            <h2>Errors</h2>
                            <ul>
                                {errorMessages.map((error) => (
                                    <li key={error}>{error}</li>
                                ))}
                            </ul>
                        </Errors>
                    )}
                </Container>
            );
        }
    }
};

export const LinksHelper = styled.div`
    margin-top: 1rem;
    a {
        text-decoration: none;
        margin-left: 0.25rem;
        font-size: 1.2rem;
        font-family: "Lato";
    }
`;

export default withRouter(Login);
