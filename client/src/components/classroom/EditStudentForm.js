import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Input } from "../../elements/components";

const EditStudentForm = ({
    firstName,
    setFirstName,
    lastName,
    setLastName,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    errors,
    submit,
}) => {
    console.log(errors);
    return (
        <>
            <Form>
                <FlexLine>
                    <InputLineGrow>
                        <Label htmlFor="first_name" className={errors.firstName ? "error" : ""}>
                            First Name
                        </Label>
                        <Input
                            type="text"
                            id="first_name"
                            name="first_name"
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                            className={errors.firstName ? "error" : ""}
                            required
                        ></Input>
                    </InputLineGrow>
                    <InputLineGrow>
                        <Label htmlFor="last_name" className={errors.lastName ? "error" : ""}>
                            Last Name
                        </Label>
                        <Input
                            type="text"
                            id="last_name"
                            name="last_name"
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            className={errors.lastName ? "error" : ""}
                            required
                        ></Input>
                    </InputLineGrow>
                </FlexLine>

                <FlexLine>
                    <InputLineGrow>
                        <Label htmlFor="password" className={errors.password ? "error" : ""}>
                            Password
                        </Label>
                        <Input
                            type="password"
                            id="password"
                            name="password"
                            value={password}
                            required
                            onChange={(e) => setPassword(e.target.value)}
                            className={errors.password ? "error" : ""}
                        ></Input>
                    </InputLineGrow>
                    <InputLineGrow>
                        <Label htmlFor="last_name" className={errors.password ? "error" : ""}>
                            Confirm Password
                        </Label>
                        <Input
                            type="password"
                            id="confirm_password"
                            name="confirm_password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            required
                            className={errors.password ? "error" : ""}
                        ></Input>
                    </InputLineGrow>
                </FlexLine>
            </Form>
            <div className="margin-top-small button-toolbar-helper">
                <Button aFunction={submit}>Update</Button>
            </div>
        </>
    );
};

export default EditStudentForm;
