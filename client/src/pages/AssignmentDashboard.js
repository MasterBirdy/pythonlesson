import React, { useState, useEffect } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { makeInfoToast } from "../utils/helpers";
import HomeworkList from "../components/assignments/HomeworkList";
import Pagination from "../components/search/Pagination";
import { usePagination } from "../hooks/usePagination";
import useMobile from "../hooks/useMobile";
import { TitleBox, TitleBoxTitle, TitleBoxTitleContainer, IconsContainer, Container } from "../elements/components/";
const pageLimit = 5;

const AssignmentDashboard = () => {
    const [loading, setLoading] = useState(true);
    const [uncompleteAssignments, setUncompleteAssignments] = useState([]);
    const [completeAssignments, setCompleteAssignments] = useState([]);
    const {
        currentPage: currentUncompleteAssignmentPage,
        currentLimit: currentUncompleteAssignmentLimit,
        paginationHandler: uncompleteAssignmentPaginationHandler,
    } = usePagination(pageLimit, uncompleteAssignments.length);
    const {
        currentPage: currentCompleteAssignmentPage,
        currentLimit: currentCompleteAssignmentLimit,
        paginationHandler: completeAssignmentPaginationHandler,
    } = usePagination(pageLimit, completeAssignments.length);
    const [isMobile] = useMobile();

    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/getassignmentstats/`,
            withCredentials: true,
        })
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    let assignments = res.data.assignments
                        .sort((a, b) => {
                            if (a.lessonName < b.lessonName) return -1;
                            if (a.lessonName > b.lessonName) return 1;
                            return 0;
                        })
                        .sort((a, b) => {
                            const aDate = new Date(a.due_date);
                            const bDate = new Date(b.due_date);
                            if (aDate > bDate) return 1;
                            if (aDate < bDate) return -1;
                            return 0;
                        });
                    setUncompleteAssignments(
                        assignments.filter((assignment) => assignment.total_units !== assignment.units_completed)
                    );
                    setCompleteAssignments(
                        assignments.filter((assignment) => assignment.total_units === assignment.units_completed)
                    );
                }
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    if (loading) {
        return <div className="loader"></div>;
    }

    return (
        <Container>
            <TitleBox maxWidth="55rem" marginTop="2" marginBottom="2">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Assignments Due</TitleBoxTitle>
                    <IconsContainer>
                        <FontAwesomeIcon
                            icon={faQuestion}
                            onClick={() =>
                                makeInfoToast(`ℹ️ This is the current list of assignments that you have due.`)
                            }
                        ></FontAwesomeIcon>
                    </IconsContainer>
                </TitleBoxTitleContainer>
                <HomeworkList yourAssignments={uncompleteAssignments}></HomeworkList>
            </TitleBox>
            <Pagination
                currentPage={currentUncompleteAssignmentPage}
                currentLimit={currentUncompleteAssignmentLimit}
                totalNumber={uncompleteAssignments.length}
                pageLimit={pageLimit}
                paginationHandler={uncompleteAssignmentPaginationHandler}
                isMobile={isMobile}
            ></Pagination>
            <TitleBox maxWidth="55rem" marginTop="1.5" marginBottom="2">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle style={{ display: "inline" }}>Assignments Completed</TitleBoxTitle>
                    <FontAwesomeIcon
                        icon={faQuestion}
                        onClick={() =>
                            makeInfoToast(`ℹ️ This is the current list of assignments that you've completed.`)
                        }
                    ></FontAwesomeIcon>
                </TitleBoxTitleContainer>
                <HomeworkList yourAssignments={completeAssignments}></HomeworkList>
            </TitleBox>
            <Pagination
                currentPage={currentCompleteAssignmentPage}
                currentLimit={currentCompleteAssignmentLimit}
                totalNumber={completeAssignments.length}
                pageLimit={pageLimit}
                paginationHandler={completeAssignmentPaginationHandler}
                isMobile={isMobile}
            ></Pagination>
        </Container>
    );
};

export default AssignmentDashboard;
