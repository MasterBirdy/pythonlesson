import React from "react";

const AddStudentForm = ({
    filteredAvailableStudents,
    chosenAvailableStudents,
    setChosenAvailableStudents,
    filterOnName,
    grade,
    setGrade,
    listOfNames,
    errors,
    name,
    setName,
}) => {
    return (
        <form className="form-component">
            <div className="flex-line margin-bottom-small">
                <div className="input-item-grow">
                    <label htmlFor="students">Students</label>
                    <select
                        className="middle-multiple-items"
                        name="students"
                        id="students"
                        value={chosenAvailableStudents}
                        multiple
                        onChange={(e) => {
                            setChosenAvailableStudents([].slice.call(e.target.selectedOptions).map((o) => o.value));
                        }}
                    >
                        {filteredAvailableStudents.map((student) => {
                            return (
                                <option value={student.id} key={student.id}>
                                    {student.first_name + " " + student.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
            </div>
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={name}
                        required
                        onChange={(e) => setName(e.target.value)}
                    ></input>
                </div>
            </div>
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="grade">Grade</label>
                    <select name="grade" id="grade" value={grade} onChange={(e) => setGrade(e.target.value)}>
                        <option value="">All</option>
                        <option value="3">3rd</option>
                        <option value="4">4th</option>
                        <option value="5">5th</option>
                        <option value="6">6th</option>
                        <option value="7">7th</option>
                        <option value="8">8th</option>
                    </select>
                </div>
                <div className="input-item-grow"></div>
            </div>
        </form>
    );
};

export default AddStudentForm;
