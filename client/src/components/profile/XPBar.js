import React from "react";
import { createUseStyles } from "react-jss";

const height = "1.7rem";

const useStyles = createUseStyles({
    bar: (props) => ({
        marginTop: ".25rem",
        position: "relative",
        borderRadius: "20px",
        background: "#e8e8e8",
        display: "flex",
        fontSize: ".9rem",
        color: "black",
        justifyContent: "center",
        alignItems: "flex-start",
        width: "15rem",
        height: height,
        "&:after": {
            background: "#ff6565cf",
            content: "''",
            display: "flex",
            height: height,
            width: `${((props.current - props.maxXPLastLevel) * 100) / (props.minNeeded - props.maxXPLastLevel)}%`,
            position: "absolute",
            borderRadius: "20px",
            top: 0,
            left: 0,
        },
    }),
    xp: {
        zIndex: "1",
        fontWeight: "700",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
    },
});

const XPBar = (props) => {
    const styles = useStyles(props);
    return (
        <div className={styles.bar}>
            <p className={styles.xp}>
                {props.current} / {props.minNeeded} XP
            </p>
        </div>
    );
};

export default XPBar;
