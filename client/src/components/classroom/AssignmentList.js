import React from "react";
import AssignmentListEntry, { TitleBoxAssignmentListEntry } from "./AssignmentListEntry";
import {
    TitleBoxDuoColorListContainer,
    TitleBoxDouColorListEntry,
    TitleBoxDouColorListText,
} from "../../elements/components";

const AssignmentList = ({ assignments }) => {
    return (
        <TitleBoxDuoColorListContainer>
            {assignments.length ? (
                assignments.map((assignment) => {
                    return (
                        <AssignmentListEntry
                            assignmentID={assignment.lesson_id}
                            dueDate={assignment.due_date}
                            name={assignment.name}
                            key={assignment.lesson_id}
                        ></AssignmentListEntry>
                    );
                })
            ) : (
                <TitleBoxAssignmentListEntry>
                    <TitleBoxDouColorListText>Looks like no students have been added!</TitleBoxDouColorListText>
                </TitleBoxAssignmentListEntry>
            )}
        </TitleBoxDuoColorListContainer>
    );
};

export default AssignmentList;
