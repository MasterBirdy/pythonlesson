import React from "react";
import { FlexLine, InputLineGrow, Label, Input } from "../../elements/components";

const TimeFormElement = ({ label, inputValue, inputValueHandler, type, disabled }) => {
    return (
        <FlexLine>
            <InputLineGrow>
                <Label htmlFor="standard">{label}</Label>
                <Input
                    type={type}
                    id="timestamp"
                    name="timestamp"
                    disabled={disabled ? disabled : false}
                    value={inputValue}
                    onChange={(e) => inputValueHandler(e.target.value)}
                ></Input>
            </InputLineGrow>
        </FlexLine>
    );
};

export default TimeFormElement;
