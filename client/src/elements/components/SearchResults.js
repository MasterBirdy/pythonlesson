import styled from "styled-components";
import { colors, below } from "../utilities";
import { lighten, darken } from "polished";

export const SearchResults = styled.div`
    border: 1px solid ${colors.searchResultsBorder};
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    border-top: 0;
`;

export const SearchResultRow = styled.div`
    display: grid;
    grid-template-columns: repeat(11, 1fr);
    background-color: ${darken(0.04, colors.secondaryColor)};
    color: ${colors.searchResultsColor};
    font-family: "PT Sans", sans-serif;
    text-align: center;
    border-bottom: 1px solid rgba(#fff, 0.8);
    grid-column: span ${(props) => (props.span ? props.span : 1)};

    ${below.s`
    font-size: 1.3rem;
    `}

    &.empty {
        display: block;
        padding: 0.5rem 1rem;
        font-size: 1.3rem;
    }
`;

export const SearchResultRowHeading = styled(SearchResultRow)`
    border-bottom: 3px solid ${colors.primaryColor};
    cursor: pointer;
`;
