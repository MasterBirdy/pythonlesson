import React from "react";
import SectionToolbar from "./SectionToolbar";

const CanvasOverlay = ({ canvasRef, openCanvasToolbar, playItAgain, openCanvasHandler, canvasChevronRef }) => {
    return (
        <>
            <SectionToolbar
                name={"Canvas"}
                type={"canvasToolbar"}
                isOpen={openCanvasToolbar}
                openToolbarHandler={openCanvasHandler}
                toolbarChevronRef={canvasChevronRef}
            ></SectionToolbar>
            <div className={["canvas-container", openCanvasToolbar ? "visible" : ""].join(" ")}>
                <div
                    ref={canvasRef}
                    id="mycanvas"
                    className={["main-canvas", openCanvasToolbar ? "visible" : ""].join(" ")}
                ></div>
            </div>
        </>
    );
};

export default CanvasOverlay;
