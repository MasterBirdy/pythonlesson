import React from "react";

const Button = (props) => {
    return (
        <button className="button" onClick={props.aFunction}>
            {props.children}
        </button>
    );
};

export default Button;
