import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { passwordRegex, makeToast, makeInfoToast } from "../utils/helpers";
import EditStudentForm from "../components/classroom/EditStudentForm";
import AddBadges from "../components/profile/AddBadges";
import axios from "axios";
import Button from "../components/ui/Button";
import ErrorComponent from "../components/ui/ErrorComponent";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import {
    Container,
    TitleBox,
    TitleBoxContainer,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    Errors,
} from "../elements/components";

const EditStudent = () => {
    const { id } = useParams();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [errors, setErrors] = useState({
        firstName: false,
        lastName: false,
        password: false,
    });
    const [errorMessages, setErrorMessages] = useState([]);
    const [loading, setLoading] = useState(true);
    const [invalid, setInvalid] = useState(false);
    const [invalidMessage, setInvalidMessage] = useState("");
    const [ownedBadges, setOwnedBadges] = useState([]);
    const [notOwnedBadges, setNotOwnedBadges] = useState([]);
    const [addBadge, setAddBadge] = useState("");
    const [removeBadge, setRemoveBadge] = useState("");
    const history = useHistory();

    useEffect(() => {
        const promises = [
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getstudentinfo/${id}`,
                withCredentials: true,
            }),
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getbadges/${id}`,
                withCredentials: true,
            }),
        ];

        Promise.all(promises)
            .then((res) => {
                if (res[0].status === 200 && res[1].status === 200) {
                    setFirstName(res[0].data.firstName);
                    setLastName(res[0].data.lastName);
                    setOwnedBadges(res[1].data.ownedBadges);
                    setNotOwnedBadges(res[1].data.notOwnedBadges);
                }
            })
            .catch((err) => {
                setInvalid(true);
                if (err.response) {
                    if (err.response.status === 401 || err.response.status === 403) {
                        setInvalidMessage("You are unauthorized to view this page.");
                    } else {
                        setInvalidMessage(err.response.data.err);
                    }
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, [id]);

    const submitHandler = () => {
        if (validate()) {
            const data = { id, firstName, lastName };
            if (password.length) {
                data.password = password;
            }
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/editstudentinfo/`,
                data,
                withCredentials: true,
            })
                .then((res) => {
                    if (res.status === 200) {
                        makeToast(`${firstName}'s profile was updated!`);
                        history.push("/classroom");
                    }
                })
                .catch((err) => {
                    if (err.response.data) {
                        setErrorMessages(err.response.data.err);
                    }
                });
        }
    };

    const submitBadgeHandler = (e) => {
        e.preventDefault();
        let promises = [];
        if (addBadge) {
            promises.push(
                axios({
                    method: "POST",
                    url: `${
                        process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                    }/api/addbadge/${id}`,
                    data: {
                        badge: addBadge,
                    },
                    withCredentials: true,
                })
            );
        }
        if (removeBadge) {
            promises.push(
                axios({
                    method: "POST",
                    url: `${
                        process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                    }/api/removebadge/${id}`,
                    data: {
                        badge: removeBadge,
                    },
                    withCredentials: true,
                })
            );
        }
        if (promises.length) {
            Promise.all(promises)
                .then((res) => {
                    makeToast("Badges were successfully updated.");
                    history.push("/classroom");
                })
                .catch((err) => {
                    setInvalid(true);
                    setInvalidMessage(err.response.data.err);
                });
        }
    };

    const validate = () => {
        const errorArray = [];
        const errorObject = {
            firstName: false,
            lastName: false,
            password: false,
        };
        if (firstName.length < 2) {
            errorArray.push("First name must be longer than 2 characters.");
            errorObject.firstName = true;
        }
        if (lastName.length < 2) {
            errorArray.push("Last name must be longer than 2 characters.");
            errorObject.lastName = true;
        }

        if (password) {
            if (!passwordRegex.test(password)) {
                errorArray.push(
                    "If a password is inputted, it must be at least 4 characters and have 1 letter and number."
                );
                errorObject.password = true;
            }
        }
        if (password !== confirmPassword) {
            errorArray.push("Password and confirm password must match!");
            errorObject.password = true;
        }
        setErrorMessages(errorArray);
        setErrors(errorObject);
        return errorArray.length === 0;
    };

    if (loading) {
        return <div className="loader"></div>;
    }
    if (invalid) {
        return (
            <div className="container">
                <ErrorComponent errorMessage={invalidMessage}></ErrorComponent>
            </div>
        );
    }
    return (
        <Container>
            <TitleBox marginTop="2" maxWidth="45rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Edit Student</TitleBoxTitle>
                    <FontAwesomeIcon
                        icon={faQuestion}
                        onClick={() =>
                            makeInfoToast(
                                `ℹ️ You can edit your student's profile info by inputting values and pressing the update button.`
                            )
                        }
                    ></FontAwesomeIcon>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <EditStudentForm
                        firstName={firstName}
                        setFirstName={setFirstName}
                        lastName={lastName}
                        setLastName={setLastName}
                        password={password}
                        setPassword={setPassword}
                        confirmPassword={confirmPassword}
                        setConfirmPassword={setConfirmPassword}
                        errors={errors}
                        submit={submitHandler}
                    ></EditStudentForm>
                </TitleBoxContainer>
            </TitleBox>

            <TitleBox marginTop="2" maxWidth="45rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Edit Badges</TitleBoxTitle>
                    <FontAwesomeIcon
                        icon={faQuestion}
                        onClick={() =>
                            makeInfoToast(
                                `ℹ️ Reward your students with badges for their profile by selecting them here! You can also remove badges if necessary.`
                            )
                        }
                    ></FontAwesomeIcon>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <AddBadges
                        currentBadges={ownedBadges}
                        notCurrentBadges={notOwnedBadges}
                        addBadge={addBadge}
                        setAddBadge={setAddBadge}
                        removeBadge={removeBadge}
                        setRemoveBadge={setRemoveBadge}
                        submitBadgeHandler={submitBadgeHandler}
                    ></AddBadges>
                </TitleBoxContainer>
            </TitleBox>
            <div className="margin-top-1-5">
                <Button aFunction={() => history.push("/classroom")}>Back</Button>
            </div>
            {errorMessages.length > 0 && (
                <Errors>
                    <h2>Errors</h2>
                    <ul>
                        {errorMessages.map((error) => (
                            <li>{error}</li>
                        ))}
                    </ul>
                </Errors>
            )}
        </Container>
    );
};

export default EditStudent;
