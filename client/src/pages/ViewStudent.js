import React, { useState, useEffect, useMemo } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import ViewStudentForm from "../components/classroom/AllParametersForm";
import InfoComponent from "../components/profile/InfoComponent";
import ViewStudentCourseInfo from "../components/classroom/ViewStudentCourseInfo";
import ErrorComponent from "../components/ui/ErrorComponent";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { makeInfoToast } from "../utils/helpers";
import Pagination from "../components/search/Pagination";
import StandardsAutoComplete from "../components/search/StandardsAutoComplete";
import useMobile from "../hooks/useMobile";
import { usePagination } from "../hooks/usePagination";
import { useAutoComplete } from "../hooks/useAutoComplete";
import { below } from "../elements/utilities";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    IconsContainer,
    Container,
} from "../elements/components/";
import styled from "styled-components";
const pageLimit = 5;

const ViewStudent = () => {
    const { id } = useParams();
    const [parameters, setParameters] = useState("");
    const [loading, setLoading] = useState(true);

    const [grade, setGrade] = useState("");
    const [subjectAreas, setSubjectAreas] = useState("");
    const [chosenSubjectAreas, setChosenSubjectAreas] = useState([]);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [avatar, setAvatar] = useState(-1);
    const [level, setLevel] = useState(1);
    const [currentXP, setCurrentXP] = useState(0);
    const [minXPNeeded, setMinXPNeeded] = useState(1);
    const [maxXPLastLevel, setMaxXPLastLevel] = useState(0);
    const [units, setUnits] = useState({});
    const [lessonsCompleted, setLessonsCompleted] = useState(0);
    const [unitsCompleted, setUnitsCompleted] = useState(0);
    const [invalid, setInvalid] = useState(false);
    const [invalidMessage, setInvalidMessage] = useState("");
    const [isMobile] = useMobile();
    const [filteredUnits, setFilteredUnits] = useState([]);
    const { currentPage, currentLimit, paginationHandler } = usePagination(pageLimit, filteredUnits.length);
    const [
        showAutoComplete,
        standardInput,
        setStandardInput,
        autoCompleteRef,
        standardInputHandler,
        autoCompleteHandler,
    ] = useAutoComplete();

    useEffect(() => {
        const promises = [
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/search`,
                withCredentials: true,
            }),
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getstudentinfo/${id}`,
                withCredentials: true,
            }),
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getstudentstats/${id}`,
                withCredentials: true,
            }),
        ];
        Promise.all(promises)
            .then((res) => {
                if (res[0].status === 200 && res[1].status === 200 && res[2].status === 200) {
                    setSubjectAreas(res[0].data.subjectAreas);
                    setAvatar(res[1].data.avatar);
                    setFirstName(res[1].data.firstName);
                    setLastName(res[1].data.lastName);
                    setLevel(res[1].data.level[0].level);
                    setCurrentXP(res[1].data.currentXP);
                    setMinXPNeeded(res[1].data.minXP);
                    setMaxXPLastLevel(res[1].data.maxXP);
                    setUnits(res[2].data.unitsInfoObject);
                }
            })
            .catch((err) => {
                setInvalid(true);
                if (err.response) {
                    if (err.response.status === 401 || err.response.status === 403) {
                        setInvalidMessage("You are unauthorized to view this page.");
                    } else {
                        setInvalidMessage(err.response.data.err);
                    }
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, [id]);

    const clearHandler = (e) => {
        e.preventDefault();
        setParameters("");
        setGrade("");
        setChosenSubjectAreas([]);
        setStandardInput("");
    };

    useEffect(() => {
        setLessonsCompleted(
            Object.keys(units).reduce((acc, currKey) => {
                if (units[currKey].total_units === units[currKey].units_completed) {
                    return acc + 1;
                }
                return acc;
            }, 0)
        );
    }, [units]);

    useEffect(() => {
        setUnitsCompleted(
            Object.keys(units).reduce((acc, currKey) => {
                return acc + units[currKey].units_completed;
            }, 0)
        );
    }, [units]);

    useEffect(() => {
        setFilteredUnits(
            Object.keys(units)
                .filter((key) => {
                    console.log(units[key].name);
                    if (!units[key].name.toLowerCase().includes(parameters.toLowerCase())) {
                        return false;
                    }
                    if (grade && !units[key].grade.includes(parseInt(grade))) {
                        return false;
                    }
                    if (chosenSubjectAreas.length) {
                        if (!chosenSubjectAreas.some((area) => units[key].subject_area_id === parseInt(area))) {
                            return false;
                        }
                    }
                    if (standardInput.length) {
                        if (
                            !units[key].standard_name ||
                            !units[key].standard_name.toLowerCase().includes(standardInput.toLowerCase())
                        ) {
                            return false;
                        }
                    }
                    return true;
                })
                .sort((a, b) => {
                    if (units[a].name.toLowerCase() < units[b].name.toLowerCase()) {
                        return -1;
                    }
                    if (units[a].name.toLowerCase() > units[b].name.toLowerCase()) {
                        return 1;
                    }
                    return 0;
                })
        );
    }, [units, parameters, grade, chosenSubjectAreas, standardInput, currentPage, pageLimit]);

    let filteredStandards = filteredUnits.reduce((acc, cur) => {
        if (units[cur].standard_id && units[cur].standard_name && units[cur].standards_title) {
            acc.push({ id: units[cur].standard_id, name: units[cur].standard_name, title: units[cur].standards_title });
        }
        return acc;
    }, []);

    filteredStandards = filteredStandards.filter(
        (standard) => standard.name.toLowerCase().indexOf(standardInput.toLowerCase()) > -1
    );

    filteredStandards.sort((a, b) => {
        if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
        }
        if (b.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
        }
        return 0;
    });

    if (loading) {
        return <div className="loader"></div>;
    }

    if (invalid) {
        return (
            <div className="container">
                <ErrorComponent errorMessage={invalidMessage}></ErrorComponent>
            </div>
        );
    }

    let results = <ViewStudentEmptyText>This student has not completed any lessons yet!</ViewStudentEmptyText>;

    if (unitsCompleted) {
        if (!filteredUnits.length) {
            results = <p>No courses match the selected criteria!</p>;
        } else {
            results = filteredUnits
                .slice(currentPage * pageLimit, currentPage * pageLimit + pageLimit)
                .map((unitKey) => {
                    return (
                        <ViewStudentCourseInfo
                            title={units[unitKey].name}
                            key={units[unitKey].lesson_id}
                            lessonID={units[unitKey].lesson_id}
                            totalUnits={units[unitKey].total_units}
                            completedUnits={units[unitKey].units_completed}
                            subject={units[unitKey].subject_name}
                            subjectArea={units[unitKey].subject_area_name}
                            coursesTrack={units[unitKey].course_tracker}
                            standard={units[unitKey].standard_name}
                        ></ViewStudentCourseInfo>
                    );
                });
        }
    }

    return (
        <Container>
            <ViewStudentGrid>
                <ViewStudentTitleBox>
                    <TitleBoxTitleContainer>
                        <TitleBoxTitle className="view-student-form-title">Search Courses</TitleBoxTitle>
                        <IconsContainer>
                            <FontAwesomeIcon
                                icon={faQuestion}
                                onClick={() =>
                                    makeInfoToast(
                                        `ℹ️ View a student's progress and stats in course modules by typing into the input fields.`
                                    )
                                }
                            ></FontAwesomeIcon>
                        </IconsContainer>
                    </TitleBoxTitleContainer>
                    <TitleBoxContainer>
                        <ViewStudentForm
                            parameters={parameters}
                            setParameters={setParameters}
                            grade={grade}
                            setGrade={setGrade}
                            subjectAreas={subjectAreas}
                            setSubjectAreas={setSubjectAreas}
                            chosenSubjectAreas={chosenSubjectAreas}
                            setChosenSubjectAreas={setChosenSubjectAreas}
                            clear={clearHandler}
                        >
                            <StandardsAutoComplete
                                standardInputHandler={standardInputHandler}
                                autoCompleteRef={autoCompleteRef}
                                standardInput={standardInput}
                                standards={filteredStandards}
                                showAutoComplete={showAutoComplete}
                                autoCompleteHandler={autoCompleteHandler}
                            ></StandardsAutoComplete>
                        </ViewStudentForm>
                    </TitleBoxContainer>
                </ViewStudentTitleBox>
                <InfoComponent
                    avatar={avatar}
                    firstName={firstName}
                    lastName={lastName}
                    level={level}
                    currentXP={currentXP}
                    minXPNeeded={minXPNeeded}
                    maxXPLastLevel={maxXPLastLevel}
                    role={"student"}
                    altMinBox={true}
                    lessonsCompleted={lessonsCompleted}
                    unitsCompleted={unitsCompleted}
                ></InfoComponent>
            </ViewStudentGrid>
            <Pagination
                currentPage={currentPage}
                currentLimit={currentLimit}
                totalNumber={filteredUnits.length}
                pageLimit={pageLimit}
                paginationHandler={paginationHandler}
                isMobile={isMobile}
            ></Pagination>
            <FilteredUnitsContainer>{results}</FilteredUnitsContainer>
        </Container>
    );
};

const ViewStudentGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 33%;
    padding-bottom: 0.5rem;
    margin-top: 2rem;

    ${below.s`
    margin: 2rem auto 0;
    grid-template-columns: 1fr;
    `}
`;

const FilteredUnitsContainer = styled.div`
    margin-top: 1.5rem;
    font-family: "PT Sans", sans-serif;
`;

const ViewStudentTitleBox = styled.div`
    ${below.s`
    order: 2;
    margin-top: 2rem;
    `}
`;

const ViewStudentEmptyText = styled.p`
    font-family: "PT Sans", sans-serif;
    font-size: 1.4rem;
`;

export default ViewStudent;
