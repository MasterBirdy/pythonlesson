import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import ResetPasswordForm from "../components/resetpassword/ResetPasswordForm";
import useQuery from "../hooks/useQuery";
import axios from "axios";
import { makeToast, passwordRegex } from "../utils/helpers";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    Container,
    Errors,
} from "../elements/components/";

const ResetPassword = () => {
    const { token } = useParams();

    const query = useQuery();
    const email = query.get("email");
    const history = useHistory();

    const [errors, setErrors] = useState({
        password: false,
        confirmPassword: false,
    });
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [confirmPassword, setConfirmPassword] = useState("");
    const [errorMessages, setErrorMessages] = useState([]);

    const changePasswordHandler = (e) => {
        e.preventDefault();
        if (validate()) {
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/confirmpasswordtoken`,
                data: {
                    token,
                    email,
                    password,
                    confirmPassword,
                },
            })
                .then((res) => {
                    if (res.status === 200) {
                        makeToast(res.data.message);
                        history.push("/login");
                    }
                })
                .catch((err) => {
                    console.log(err);
                    setErrorMessages((prevState) => [...prevState, err.response.data.err]);
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    };

    const validate = () => {
        const errorArray = [];
        const errorObject = {
            password: false,
            confirmPassword: false,
        };
        if (password !== confirmPassword) {
            errorArray.push("Password and Confirm Password must match!");
            errorObject.password = true;
            errorObject.confirmPassword = true;
        }
        if (!passwordRegex.test(password)) {
            errorArray.push("Passwords must be at least 4 characters and have 1 letter and number.");
            errorObject.password = true;
        }
        setErrorMessages(errorArray);
        setErrors(errorObject);
        return errorArray.length === 0;
    };

    return (
        <Container>
            <TitleBox marginTop="2" maxWidth="50rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Change Password</TitleBoxTitle>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <ResetPasswordForm
                        password={password}
                        setPassword={setPassword}
                        confirmPassword={confirmPassword}
                        setConfirmPassword={setConfirmPassword}
                        errors={errors}
                        loading={loading}
                        submit={changePasswordHandler}
                    ></ResetPasswordForm>
                </TitleBoxContainer>
            </TitleBox>
            {errorMessages.length > 0 && (
                <Errors>
                    <h2>Errors</h2>
                    <ul>
                        {errorMessages.map((error) => (
                            <li>{error}</li>
                        ))}
                    </ul>
                </Errors>
            )}
        </Container>
    );
};

export default ResetPassword;
