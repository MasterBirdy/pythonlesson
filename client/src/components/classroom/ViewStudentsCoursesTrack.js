import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import styled from "styled-components";
import { below } from "../../elements/utilities";
import useMobile from "../../hooks/useMobile";

const ViewStudentCoursesTrack = ({ courses, lessonID }) => {
    return (
        <CoursesTrack>
            <CoursesBlocks>
                {courses.map((course, index) => {
                    return (
                        <CourseBlock
                            className={[index < 4 ? "top-row" : ""].join(" ")}
                            key={index}
                            title={`Lesson ${lessonID}-${index + 1}: ${course !== 0 ? "Completed" : "Not Completed"}`}
                        >
                            <FontAwesomeIcon
                                icon={course ? faCheckCircle : faTimesCircle}
                                color={course ? "green" : "red"}
                            ></FontAwesomeIcon>
                        </CourseBlock>
                    );
                })}
            </CoursesBlocks>
        </CoursesTrack>
    );
};

const CoursesTrack = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-end;
    flex-direction: column;
    ${below.s`
    align-items: center;
    `}
`;

const CoursesBlocks = styled.div`
    ${below.s`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(2.25rem, 1fr));
    max-width: 11rem;
    direction: ltr;
    border-left: 1px solid rgb(70, 70, 70);
    `}
`;

const CourseBlock = styled.div`
    cursor: help;
    position: relative;
    display: inline-block;
    background-color: white;
    text-align: center;
    width: 2.25rem;
    height: 2.25rem;
    border: 1px solid rgb(70, 70, 70);
    border-width: 0 1px 1px 0;
    border-top: 1px solid rgb(70, 70, 70);

    &:first-child {
        border-left: 1px solid rgb(70, 70, 70);
    }

    svg {
        margin-left: 0 !important;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    ${below.s`
    border-top: 0;
    &.top-row {
    border-top: 1px solid rgb(70, 70, 70);
    }
    &:first-child {
        border-left: 0;
    }    
    `}
`;

export default ViewStudentCoursesTrack;
