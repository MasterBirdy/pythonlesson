import React from "react";

const Pagination = ({ currentPage, paginationHandler, currentLimit, totalNumber, pageLimit, isMobile }) => {
    let numberButtons = [];

    for (let i = currentLimit; i < Math.min(totalNumber / pageLimit, isMobile ? 5 : 10); i++) {
        numberButtons.push(
            <button key={i} className={currentPage === i ? "selected" : ""} onClick={() => paginationHandler(i)}>
                {i + 1}
            </button>
        );
    }

    return (
        <div className="pagination-toolbar">
            <button className="previous-button" onClick={() => paginationHandler(currentPage - 1)}>
                Previous
            </button>
            {numberButtons}
            <button className="next-button" onClick={() => paginationHandler(currentPage + 1)}>
                Next
            </button>
        </div>
    );
};

export default Pagination;
