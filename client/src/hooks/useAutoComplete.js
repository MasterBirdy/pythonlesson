import { useState, useRef, useEffect } from "react";

export const useAutoComplete = () => {
    const [showAutoComplete, setShowAutoComplete] = useState(false);
    const [autoCompleteInput, setAutoCompleteInput] = useState("");
    const autoCompleteRef = useRef();

    const showAutoCompleteHandler = (name) => {
        setAutoCompleteInput(name);
        setShowAutoComplete(false);
    };

    const autoCompleteInputHandler = (e) => {
        setAutoCompleteInput(e.target.value);
        setShowAutoComplete(true);
    };

    const handleClickOutside = (event) => {
        if (!autoCompleteRef.current || autoCompleteRef.current.contains(event.target)) return;
        setShowAutoComplete(false);
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        return () => document.removeEventListener("mousedown", handleClickOutside);
    }, [autoCompleteRef]);

    return [
        showAutoComplete,
        autoCompleteInput,
        setAutoCompleteInput,
        autoCompleteRef,
        autoCompleteInputHandler,
        showAutoCompleteHandler,
    ];
};
