import React from "react";
import styled from "styled-components";
import { below } from "../../elements/utilities";
import Badge from "./Badge";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    Container,
} from "../../elements/components/";

const BadgeHolder = ({ badges }) => {
    return (
        <TitleBox marginTop="2">
            <TitleBoxTitleContainer>
                <TitleBoxTitle>Badges</TitleBoxTitle>
            </TitleBoxTitleContainer>
            <BadgeGrid>
                {badges.length === 0 && <EmptyText>No badges achieved yet!</EmptyText>}
                {badges.length > 0 &&
                    badges.map((badge) => {
                        return <Badge name={badge.name} id={badge.badge_id} key={badge.badge_id}></Badge>;
                    })}
            </BadgeGrid>
        </TitleBox>
    );
};

const BadgeGrid = styled(TitleBoxContainer)`
    padding: 1rem 1.5rem;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    ${below.s`
        padding: 0.25rem 0.25rem;
        grid-template-columns: repeat(3, 1fr);
    `}
    ${below.xs`
        grid-template-columns: repeat(2, 1fr);
    `}
`;

const EmptyText = styled.p`
    color: rgb(27, 27, 27);
    font-family: "PT Sans", sans-serif;
    font-size: 1.3rem;
    ${below.s`
    padding: 0.5rem 0 0.5rem 1.8rem;
    `}
`;

export default BadgeHolder;
