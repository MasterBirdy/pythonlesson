import React, { useContext } from "react";
import { ProfileContext } from "../context/ProfileContext";
import InfoComponent from "../components/profile/InfoComponent";
import ActivityLog from "../components/profile/ActivityLog";
import BadgeHolder from "../components/profile/BadgeHolder";
import { Container } from "../elements/components/";
import styled from "styled-components";
import { below } from "../elements/utilities";

const Profile = () => {
    const profile = useContext(ProfileContext);
    return (
        <Container>
            <ProfileGrid>
                <ActivityLog log={profile.log}></ActivityLog>
                <InfoComponent
                    avatar={profile.avatar}
                    firstName={profile.firstName}
                    lastName={profile.lastName}
                    level={profile.level}
                    currentXP={profile.currentXP}
                    minXPNeeded={profile.minXPNeeded}
                    maxXPLastLevel={profile.maxXPLastLevel}
                    role={profile.role}
                    grade={profile.grade}
                    schoolName={profile.schoolName}
                ></InfoComponent>
            </ProfileGrid>
            <BadgeHolder badges={profile.badges}></BadgeHolder>
        </Container>
    );
};

const ProfileGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 33%;
    font-family: "PT Sans", sans-serif;
    font-weight: 400;
    margin-top: 2rem;
    ${below.s`
    grid-template-columns: 1fr;
    `}
`;

export default Profile;
