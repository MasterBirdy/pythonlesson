const knex = require("../database/db.js");
const jwt_decode = require("jwt-decode");
const bcrypt = require("bcrypt");
const Joi = require("@hapi/joi");

process.env.SECRET_KEY = "secret";

const passwordRegex = /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}/;

const studentSchema = Joi.object({
    first_name: Joi.string().trim().alphanum().min(3).required(),
    last_name: Joi.string().trim().alphanum().min(3).required(),
    password: Joi.string().trim().regex(passwordRegex).required(),
});

const passwordlessStudentSchema = Joi.object({
    first_name: Joi.string().trim().alphanum().min(3).required(),
    last_name: Joi.string().trim().alphanum().min(3).required(),
});

module.exports = [
    {
        method: "GET",
        path: "/api/schools",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                const schools = await knex.select("id", "name").from("schools");
                return h.response({ schools }).code(200);
            } catch (err) {
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/school",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const school = await knex.select("name").from("schools").where("id", decoded.student_at).limit(1);
                if (school.length <= 0) {
                    throw new Error("No school found!");
                } else {
                    return h.response({ name: school[0].name, message: "Success!" }).code(200);
                }
            } catch (err) {
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getstudentsofschool/",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                let students = await knex
                    .select("id", "email", "first_name", "last_name", "xp", "grade")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", null)
                    .orderBy("last_name")
                    .orderBy("first_name");

                let teacherStudents = await knex
                    .select("id", "email", "first_name", "last_name", "xp", "grade")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .orderBy("last_name")
                    .orderBy("first_name");
                return h.response({ students, teacherStudents }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getstudentinfo/{student_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                if (!Number.isInteger(parseInt(req.params.student_id))) {
                    throw new Error("Invalid ID!");
                }
                const decoded = jwt_decode(req.state.token);
                let student = await knex
                    .select("id", "first_name", "last_name", "password", "avatar", "xp", "grade")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .andWhere("id", req.params.student_id)
                    .limit(1);
                if (!student.length) {
                    throw new Error("Student not found in your classroom!");
                }
                const currentUserInfo = student[0];
                const currentXP = currentUserInfo.xp;
                const level = await knex("levels")
                    .whereRaw(`${currentXP} - min_xp >= 0`)
                    .max({ level: "level", minXP: "min_xp" });
                if (level.length <= 0) {
                    throw new Error("Error with XP");
                }
                const maxXP = await knex("levels").whereRaw(`${currentXP} >= min_xp`).max({ maxXP: "min_xp" });

                const minXP = await knex("levels").whereRaw(`${currentXP} < min_xp`).min({ minXP: "min_xp" });
                if (minXP.length <= 0) {
                    throw new Error("Error with XP");
                }

                return h
                    .response({
                        firstName: student[0].first_name,
                        lastName: student[0].last_name,
                        avatar: student[0].avatar,
                        message: "Success!",
                        currentXP,
                        level,
                        grade: student[0].grade,
                        maxXP: maxXP[0].maxXP,
                        minXP: minXP[0].minXP,
                    })
                    .code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getassignmentstats/",
        config: {
            auth: "student",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                let assignments = await knex.raw(`SELECT assignments.lesson_id, assignments.user_id, assignments.due_date, lesson_table.name AS lessonName, lesson_table.total_units, lessons_subject_areas.subject_area_id AS subjectAreaID, subject_areas.name as subjectName, count(*) as units_completed FROM users
                JOIN assignments
                ON users.id = ${decoded.id}
                AND users.taught_by = assignments.user_id
                JOIN (
                SELECT id, name, COUNT(*) AS total_units FROM lessons
                LEFT JOIN units
                ON units.lesson_id = lessons.id
                GROUP BY lessons.id
                ) as lesson_table
                ON assignments.lesson_id = lesson_table.id
                LEFT JOIN lessons_subject_areas
                ON assignments.lesson_id = lessons_subject_areas.lesson_id
                LEFT JOIN subject_areas
                ON lessons_subject_areas.subject_area_id = subject_areas.id
                LEFT JOIN users_units
                ON users_units.lesson_id = assignments.lesson_id
                GROUP BY assignments.lesson_id`);
                assignments = assignments[0];
                return h.response({ message: "Success!", assignments }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getassignmentcompletion/{assignment_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                let students = await knex.raw(`SELECT id, taught_by, first_name, last_name, total_units, Count(*) as lessons_completed FROM users
                JOIN users_units
                ON users_units.user_id = users.id
                JOIN (
                SELECT id as lessonTableID, name, COUNT(*) AS total_units FROM lessons
                LEFT JOIN units
                ON units.lesson_id = lessons.id
                GROUP BY lessons.id
                ) as lesson_table
                ON users_units.lesson_id = lessonTableId
                GROUP BY lesson_id
                HAVING users_units.lesson_id = ${req.params.assignment_id}
                AND taught_by = ${decoded.id}`);
                students = students[0];
                console.log(students);
                if (!students.length) {
                    return h.response({ students: {} }).code(200);
                }
                let units = await knex("users")
                    .select("id", "first_name", "last_name", "lesson_id", "unit_id")
                    .leftJoin("users_units", "users_units.user_id", "users.id")
                    .where("taught_by", "=", decoded.id)
                    .andWhere("users_units.lesson_id", "=", req.params.assignment_id);
                const studentsObject = {};
                const assignmentLength = students[0].total_units;
                students.forEach((student) => {
                    studentsObject[student.id] = {
                        ...student,
                        unitsCompleted: new Array(assignmentLength).fill(false),
                    };
                });
                units.forEach((unit) => {
                    studentsObject[unit.id].unitsCompleted[unit.unit_id - 1] = true;
                });

                return h.response({ message: "Success!", students: studentsObject }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getstudentstats/{student_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                if (!Number.isInteger(parseInt(req.params.student_id))) {
                    throw new Error("Invalid ID!");
                }
                let units = await knex.raw(`SELECT user_id, users_units.lesson_id, lesson_table.name, lesson_table.total_units, subject_areas.id AS subject_area_id, subject_areas.name AS subject_area_name, lessons_standards.standard_id, standards.name AS standard_name, standards.title AS standards_title, count(*) AS units_completed FROM users_units
                INNER JOIN (
                    SELECT id, name, COUNT(*) AS total_units FROM lessons
                LEFT JOIN units
                ON units.lesson_id = lessons.id
                GROUP BY lessons.id
                ) as lesson_table
                ON lesson_id = lesson_table.id
                LEFT JOIN lessons_subject_areas
                ON users_units.lesson_id = lessons_subject_areas.lesson_id
                LEFT JOIN subject_areas
                ON lessons_subject_areas.subject_area_id = subject_areas.id
                LEFT JOIN lessons_standards
                ON users_units.lesson_id = lessons_standards.lesson_id
                LEFT JOIN standards
                ON lessons_standards.standard_id = standards.id
                GROUP BY users_units.user_id, users_units.lesson_id
                HAVING user_id = ${req.params.student_id}`);

                units = units[0];

                let unitsEach = await knex("users_units")
                    .select("user_id", "users_units.unit_id", "users_units.lesson_id", "lessons_grades.grade_id")
                    .leftJoin("lessons_grades", function () {
                        this.on("users_units.lesson_id", "=", "lessons_grades.lesson_id");
                    })
                    .where("user_id", req.params.student_id);
                const unitsInfoObject = {};
                units.forEach((unit) => {
                    unitsInfoObject[unit.lesson_id] = { ...unit };
                    unitsInfoObject[unit.lesson_id]["course_tracker"] = Array(unit.total_units).fill(0);
                    unitsInfoObject[unit.lesson_id]["grade"] = new Set();
                });
                unitsEach.forEach((unit) => {
                    unitsInfoObject[unit.lesson_id]["course_tracker"][unit.unit_id - 1] = 1;
                    unitsInfoObject[unit.lesson_id]["grade"].add(unit.grade_id);
                });

                Object.keys(unitsInfoObject).forEach((key) => {
                    unitsInfoObject[key]["grade"] = Array.from(unitsInfoObject[key]["grade"]);
                });
                console.log(unitsInfoObject);
                return h.response({ unitsInfoObject }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/editstudentinfo/",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                let userData, validation;
                if (req.payload.password) {
                    userData = {
                        first_name: req.payload.firstName,
                        last_name: req.payload.lastName,
                        password: req.payload.password,
                    };
                    validation = studentSchema.validate(userData, { escapeHtml: true });
                } else {
                    userData = {
                        first_name: req.payload.firstName,
                        last_name: req.payload.lastName,
                    };
                    validation = passwordlessStudentSchema.validate(userData, { escapeHtml: true });
                }
                if (validation.error) {
                    throw new Error("There was an error with validating the submitted data");
                }
                let student = await knex
                    .select("id")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .andWhere("id", req.payload.id)
                    .limit(1);
                if (!student.length) {
                    throw new Error("Student not found!");
                }
                if (req.payload.password) {
                    const hash = await bcrypt.hash(req.payload.password, 10);
                    userData.password = hash;
                }
                let update = await knex("users")
                    .update(userData)
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .andWhere("id", req.payload.id);

                return h.response({ message: "Success!" }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/modifystudentlist",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                if (req.payload.addingStudents.length) {
                    const addedResult = await knex
                        .update({ taught_by: decoded.id })
                        .from("users")
                        .where("taught_by", null)
                        .andWhere("teacher_at", null)
                        .where((queryBuilder) => {
                            req.payload.addingStudents.forEach((student, index) => {
                                if (index === 0) queryBuilder.where("id", student.id);
                                else queryBuilder.orWhere("id", student.id);
                            });
                        });
                }

                if (req.payload.removingStudents.length) {
                    const removingResult = await knex
                        .update({ taught_by: null })
                        .from("users")
                        .where("taught_by", decoded.id)
                        .andWhere("teacher_at", null)
                        .where((queryBuilder) => {
                            req.payload.removingStudents.forEach((student, index) => {
                                if (index === 0) queryBuilder.where("id", student.id);
                                else queryBuilder.orWhere("id", student.id);
                            });
                        });
                }
                let students = await knex
                    .select("id", "email", "first_name", "last_name", "xp", "grade")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", null)
                    .orderBy("last_name")
                    .orderBy("first_name");

                let teacherStudents = await knex
                    .select("id", "email", "first_name", "last_name", "xp", "grade")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .orderBy("last_name")
                    .orderBy("first_name");
                return h.response({ students, teacherStudents }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
];
