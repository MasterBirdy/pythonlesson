import React from "react";
import useMobile from "../hooks/useMobile";
import { useLocation } from "react-router-dom";
import SearchResults from "../components/search/SearchResults";
import Pagination from "../components/search/Pagination";
import { usePagination } from "../hooks/usePagination";
import { useSearchResults } from "../hooks/useSearchResults";
import { TitleBox, TitleBoxTitle, TitleBoxTitleContainer, TitleBoxContainer, Container } from "../elements/components/";

const pageLimit = 10;

const SearchResultsPage = () => {
    const location = useLocation();
    const [isMobile] = useMobile();
    const {
        results,
        sortedResults,
        reverse,
        loading,
        titleSort,
        subjectAreaSort,
        standardSort,
        progressSort,
        lessonSort,
        titleHandler,
        subjectAreaHandler,
        standardHandler,
        progressHandler,
        lessonHandler,
    } = useSearchResults(
        `${
            process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
        }/api/searchresults${location.search}`,
        true
    );
    const { currentPage, currentLimit, paginationHandler } = usePagination(pageLimit, results.length);

    let slicedResults = sortedResults.slice(currentPage * pageLimit, currentPage * pageLimit + pageLimit);

    if (loading) {
        return <div className="loader"></div>;
    }

    return (
        <Container>
            <TitleBox marginTop="1.75" marginBottom="1">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Search Results</TitleBoxTitle>
                </TitleBoxTitleContainer>
                <SearchResults
                    results={slicedResults}
                    titleSort={titleSort}
                    titleHandler={titleHandler}
                    subjectAreaHandler={subjectAreaHandler}
                    subjectAreaSort={subjectAreaSort}
                    standardSort={standardSort}
                    standardHandler={standardHandler}
                    reverse={reverse}
                    isMobile={isMobile}
                    progressSort={progressSort}
                    progressHandler={progressHandler}
                    lessonSort={lessonSort}
                    lessonHandler={lessonHandler}
                    location={location.pathname}
                ></SearchResults>
            </TitleBox>
            <Pagination
                currentPage={currentPage}
                currentLimit={currentLimit}
                totalNumber={results.length}
                pageLimit={pageLimit}
                paginationHandler={paginationHandler}
                isMobile={isMobile}
            ></Pagination>
        </Container>
    );
};

export default SearchResultsPage;
