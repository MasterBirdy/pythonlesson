import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import InfoComponent from "../components/profile/InfoComponent";
import ActivityLog from "../components/profile/ActivityLog";
import BadgeHolder from "../components/profile/BadgeHolder";
import axios from "axios";

const PublicProfile = () => {
    const { id } = useParams();
    const [log, setLog] = useState([]);
    const [badges, setBadges] = useState([]);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [grade, setGrade] = useState("");
    const [schoolName, setSchoolName] = useState("");
    const [level, setLevel] = useState(1);
    const [currentXP, setCurrentXP] = useState(0);
    const [minXPNeeded, setMinXPNeeded] = useState(1);
    const [maxXPLastLevel, setMaxXPLastLevel] = useState(0);
    const [avatar, setAvatar] = useState(-1);
    const [role, setRole] = useState("");
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("Error!");
    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/profileinfo/${id}`,
            withCredentials: true,
        })
            .then((res) => {
                setFirstName(res.data.firstName);
                setLastName(res.data.lastName);
                setGrade(res.data.grade);
                setSchoolName(res.data.schoolName);
                setLevel(res.data.level[0].level);
                setCurrentXP(res.data.currentXP);
                setMinXPNeeded(res.data.minXP);
                setMaxXPLastLevel(res.data.maxXP);
                setRole(res.data.role);
                setAvatar(res.data.avatar);
                setBadges(res.data.badges);
                setLog(res.data.log);
            })
            .catch((err) => {
                setError(true);
                if (err.response) {
                    setErrorMessage(err.response.data.err);
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, [id]);

    if (loading) {
        return <div className="loader"></div>;
    }

    let contents = (
        <>
            <div className="profile-grid">
                <ActivityLog log={log}></ActivityLog>
                <InfoComponent
                    avatar={avatar}
                    firstName={firstName}
                    lastName={lastName}
                    level={level}
                    currentXP={currentXP}
                    minXPNeeded={minXPNeeded}
                    maxXPLastLevel={maxXPLastLevel}
                    role={role}
                    grade={grade}
                    schoolName={schoolName}
                ></InfoComponent>
            </div>
            <BadgeHolder badges={badges}></BadgeHolder>
        </>
    );

    if (error) {
        contents = (
            <div className="blocked">
                <p>{errorMessage}</p>
            </div>
        );
    }
    return <div className="container">{contents}</div>;
};

export default PublicProfile;
