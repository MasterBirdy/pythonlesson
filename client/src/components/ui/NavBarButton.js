import React from "react";

const NavBarButton = (props) => {
    let color = "";

    const colorSwitcher = () => {
        switch (props.color) {
            case "orange":
                color = "navbar-orange";
                break;
            case "yellow":
                color = "navbar-yellow";
                break;
            case "green":
                color = "navbar-green";
                break;
            case "blue":
                color = "navbar-blue";
                break;
            case "purple":
                color = "navbar-purple";
                break;
            case "pink":
                color = "navbar-pink";
                break;
            default:
                color = "navbar-blue";
        }
    };

    colorSwitcher();

    return (
        <button
            className={["navbar-button", color].join(" ")}
            onClick={props.aFunction}
            onMouseEnter={props.mouseEnter ? props.mouseEnter : () => {}}
            onMouseLeave={props.mouseLeave ? props.mouseLeave : () => {}}
        >
            <span>{props.children}</span>
        </button>
    );
};

export default NavBarButton;
