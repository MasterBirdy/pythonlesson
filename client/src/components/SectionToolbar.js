import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

const SectionToolbar = ({ name, type, isOpen, openToolbarHandler, isError }) => {
    return (
        <div
            className={[
                "toolbar",
                type === "consoleToolbar" ? "console-toolbar" : "",
                type === "canvasToolbar" ? "canvas-toolbar" : "",
                isError ? "alert" : "",
            ].join(" ")}
        >
            <div className="center-toolbar-section">
                <span className="toolbar-text">{name}</span>
                <button className="toolbar-button" onClick={(e) => openToolbarHandler(!isOpen)}>
                    <FontAwesomeIcon
                        icon={faChevronDown}
                        size="xs"
                        style={{ verticalAlign: 0 }}
                        className={isOpen ? "tilted" : ""}
                    ></FontAwesomeIcon>
                </button>
            </div>
        </div>
    );
};

export default SectionToolbar;
