import React, { useState, useEffect, useContext, useMemo, useRef } from "react";
import { ProfileContext } from "../context/ProfileContext";
import SearchForm from "../components/search/SearchForm";
import SearchStandardsForm from "../components/search/SearchStandardsForm";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { makeInfoToast } from "../utils/helpers";
import { useAutoComplete } from "../hooks/useAutoComplete";
import { TitleBox, TitleBoxContainer, TitleBoxTitle, TitleBoxTitleContainer, Container } from "../elements/components";

const Search = () => {
    const [parameters, setParameters] = useState("");
    const [loading, setLoading] = useState(true);
    const [grade, setGrade] = useState("");
    const [subjectAreas, setSubjectAreas] = useState("");
    const [chosenSubjectAreas, setChosenSubjectAreas] = useState([]);
    const [standards, setStandards] = useState({});
    const history = useHistory();
    const profile = useContext(ProfileContext);
    const [
        showAutoComplete,
        standardInput,
        setStandardInput,
        autoCompleteRef,
        standardInputHandler,
        autoCompleteHandler,
    ] = useAutoComplete();

    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/search`,
            withCredentials: true,
        })
            .then((res) => {
                setSubjectAreas(res.data.subjectAreas);
                setStandards(res.data.standards);
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    const clearHandler = (e) => {
        e.preventDefault();
        setParameters("");
        setGrade("");
        setChosenSubjectAreas([]);
    };

    const filteredStandards = useMemo(() => {
        if (grade) {
            return standards[grade].filter((standard) => {
                return standard.name.toLowerCase().indexOf(standardInput.toLowerCase()) > -1;
            });
        } else {
            let standardsArray = [];
            Object.keys(standards).forEach((key) => {
                standards[key].forEach((standard) => {
                    if (standard.name.toLowerCase().indexOf(standardInput.toLowerCase()) > -1) {
                        standardsArray.push(standard);
                    }
                });
            });
            return standardsArray.sort((a, b) => {
                if (a.name < b.name) return -1;
                if (a.name > b.name) return 1;
                return 0;
            });
        }
    }, [grade, standards, standardInput]);

    const formHandler = (e) => {
        e.preventDefault();
        setLoading(true);
        let hasBeenSet = false;
        let urlConstructor = ``;
        if (parameters) {
            urlConstructor += `name=${parameters}`;
            hasBeenSet = true;
        }

        if (profile.role === "student") {
            if (hasBeenSet) {
                urlConstructor += `&`;
            }
            urlConstructor += `grade=${profile.grade}`;
            hasBeenSet = true;
        } else {
            if (grade) {
                if (hasBeenSet) {
                    urlConstructor += `&`;
                }
                urlConstructor += `grade=${grade}`;
                hasBeenSet = true;
            }
        }

        if (chosenSubjectAreas.length) {
            if (hasBeenSet) {
                urlConstructor += `&`;
            }
            urlConstructor += `subjectareas=`;
            chosenSubjectAreas.forEach((subjectArea, index) => {
                urlConstructor += `${subjectArea}`;
                if (!(index >= chosenSubjectAreas.length - 1)) {
                    urlConstructor += `,`;
                }
            });
            hasBeenSet = true;
        }

        if (urlConstructor !== ``) {
            urlConstructor = `?` + urlConstructor;
        }
        history.push("/searchresults" + urlConstructor);
    };

    const formHandler2 = (e) => {
        e.preventDefault();
        setLoading(true);
        let urlConstructor = ``;
        if (standardInput) {
            urlConstructor += `?standard=${standardInput}`;
        }
        history.push("/searchresults" + urlConstructor);
    };

    const clearHandler2 = (e) => {
        e.preventDefault();
        setStandardInput("");
    };

    if (loading) {
        return <div className="loader"></div>;
    }

    return (
        <Container>
            <TitleBox maxWidth="50rem" marginTop="2">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Search</TitleBoxTitle>
                    <FontAwesomeIcon
                        icon={faQuestion}
                        onClick={() =>
                            makeInfoToast(
                                `ℹ️ Search using any or all of the shown input fields. You can also wrap keywords in double quotations to search for exact phrases.`
                            )
                        }
                    ></FontAwesomeIcon>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <SearchForm
                        parameters={parameters}
                        setParameters={setParameters}
                        grade={grade}
                        setGrade={setGrade}
                        subjectAreas={subjectAreas}
                        setSubjectAreas={setSubjectAreas}
                        chosenSubjectAreas={chosenSubjectAreas}
                        setChosenSubjectAreas={setChosenSubjectAreas}
                        formHandler={formHandler}
                        clearHandler={clearHandler}
                        role={profile.role}
                    ></SearchForm>
                </TitleBoxContainer>
            </TitleBox>
            {profile.role === "teacher" && (
                <TitleBox maxWidth="50rem" marginTop="2">
                    <TitleBoxTitleContainer>
                        <TitleBoxTitle>Search Standards</TitleBoxTitle>
                        <FontAwesomeIcon
                            icon={faQuestion}
                            onClick={() =>
                                makeInfoToast(`ℹ️ You can search for lessons by typing in a Common Core standard.`)
                            }
                        ></FontAwesomeIcon>
                    </TitleBoxTitleContainer>
                    <TitleBoxContainer>
                        <SearchStandardsForm
                            standardInput={standardInput}
                            setStandardInput={setStandardInput}
                            standards={filteredStandards}
                            formHandler={formHandler2}
                            clearHandler={clearHandler2}
                            autoCompleteHandler={autoCompleteHandler}
                            showAutoComplete={showAutoComplete}
                            autoCompleteHandler={autoCompleteHandler}
                            standardInputHandler={standardInputHandler}
                            autoCompleteRef={autoCompleteRef}
                        ></SearchStandardsForm>
                    </TitleBoxContainer>
                </TitleBox>
            )}
        </Container>
    );
};

export default Search;
