import React from "react";

const AddedStudentForm = ({
    selectedStudents,
    setSelectedStudents,
    chosenSelectedStudents,
    setChosenSelectedStudents,
}) => {
    return (
        <div>
            <form className="form-component">
                <div className="flex-line">
                    <div className="input-item-grow">
                        <label htmlFor="email">Selected Students</label>
                        <select
                            className="big-multiple-items"
                            name="selectedstudents"
                            id="selectedstudents"
                            value={chosenSelectedStudents}
                            multiple
                            size="15"
                            onChange={(e) => {
                                setChosenSelectedStudents([].slice.call(e.target.selectedOptions).map((o) => o.value));
                            }}
                        >
                            {selectedStudents.map((student) => {
                                return (
                                    <option value={student.id} key={student.id}>
                                        {student.first_name + " " + student.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default AddedStudentForm;
