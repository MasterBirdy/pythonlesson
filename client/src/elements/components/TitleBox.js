import styled from "styled-components";
import { colors, flex } from "../utilities";
import { Link } from "react-router-dom";

export const TitleBox = styled.div`
    max-width: ${(props) => (props.maxWidth ? props.maxWidth : "100%")};
    margin-top: ${(props) => (props.marginTop ? props.marginTop : 0)}rem;
    margin-bottom: ${(props) => (props.marginBottom ? props.marginBottom : 0)}rem;
`;

export const TitleBoxTitleContainer = styled.div`
    background: ${colors.titleBox};
    background: linear-gradient(270deg, ${colors.titleBox} 0%, ${colors.titleBoxDark} 100%);
    color: white;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding: 0.75rem 2rem 0.66rem;
    ${flex({ justifyContent: "space-between", alignItems: "center" })}
    svg {
        font-size: 1.5rem;
        cursor: pointer;
    }
`;

export const TitleBoxTitle = styled.h1`
    font-family: "PT Sans", sans-serif;
    line-height: 1.4;
    font-size: 1.85em;
    font-weight: 600;
`;

export const TitleBoxContainer = styled.div`
    padding: 0rem 1.66rem 1rem;
    background-color: ${colors.titleBoxBackground};
    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px;
    box-shadow: 0px 4px 10px -7px ${colors.titleBoxBoxShadow};
`;

export const TitleBoxDuoColorListContainer = styled(TitleBoxContainer)`
    flex-grow: 1;
    padding: 0;
    color: ${colors.titleBoxListEntry};
`;

export const TitleBoxDouColorListEntry = styled.div`
    padding: 0.8rem 2rem 0.5rem;
    border-bottom: 1px solid ${colors.titleBoxListBackground1};
    &:nth-child(even) {
        background-color: ${colors.titleBoxListBackground2};
    }

    &:last-child {
        border: 0;
        padding-bottom: 0.75rem;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
`;

export const TitleBoxDouColorListText = styled.p`
    font-family: "PT Sans";
    font-size: 1.3rem;
    &.due-assignment {
        color: ${colors.titleBoxDueAssignment};
        text-decoration: line-through;
    }
`;

export const IconsContainer = styled.div`
    ${flex({ alignItems: "center" })}
    a {
        color: inherit;
        text-decoration: none;
    }

    a:not(:last-child) {
        margin-right: 0.5rem;
    }
`;

export const AssignmentEntryLink = styled(Link)`
    display: inline-block;
    text-decoration: none;
    &:visited {
        color: rgb(110, 0, 0);
    }
`;

export const AssignmentTextLine = styled.p`
    font-size: 1.2rem;
    color: #444;

    &.time-line {
        color: #666;
        font-size: 1.1rem;
    }
`;
