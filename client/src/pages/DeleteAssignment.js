import React, { useState, useContext, useEffect } from "react";
import Button from "../components/ui/Button";
import { useHistory, useParams } from "react-router-dom";
import { makeToast, makeErrorToast } from "../utils/helpers";
import axios from "axios";
import ErrorComponent from "../components/ui/ErrorComponent";
import { AuthContext } from "../context/AuthContext";
import { ProfileContext } from "../context/ProfileContext";
import { TitleBox, TitleBoxTitle, TitleBoxTitleContainer, TitleBoxContainer, Container } from "../elements/components/";

import styled from "styled-components";

const DeleteAssignment = () => {
    const history = useHistory();
    const { id } = useParams();
    const [invalid, setInvalid] = useState(false);
    const [invalidMessage, setInvalidMessage] = useState("");
    const [loading, setLoading] = useState(true);
    const auth = useContext(AuthContext);
    const profile = useContext(ProfileContext);

    useEffect(() => {
        if (auth.isAuthenticated() && profile.role) {
            if (profile.role !== "teacher") {
                setInvalid(true);
                setInvalidMessage("You are unauthorized to view this page");
            } else {
                setInvalid(false);
            }
            setLoading(false);
        }
    }, [auth, profile]);

    const submitHandler = () => {
        axios({
            method: "POST",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/deleteassignment/${id}`,
            withCredentials: true,
        })
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    makeToast("Assignment successfully deleted.");
                    history.push("/classroom");
                }
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 500) {
                        makeErrorToast(err.response.data.err);
                    }
                }
            });
    };

    if (loading) {
        return <div className="loader"></div>;
    }

    if (invalid) {
        return (
            <div className="container">
                <ErrorComponent errorMessage={invalidMessage}></ErrorComponent>
            </div>
        );
    }

    return (
        <Container>
            <TitleBox marginTop="2" maxWidth="55rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Delete Assignment</TitleBoxTitle>
                </TitleBoxTitleContainer>
                <DeleteAssignmentContainer>
                    <DeleteAssignmentText>
                        This will permanently delete this assignment. Are you sure you wish to do this?
                    </DeleteAssignmentText>
                    <div className="margin-top-small button-toolbar-helper">
                        <Button aFunction={submitHandler}>Yes</Button>
                        <Button aFunction={() => history.push("/classroom")}>No, Go Back</Button>
                    </div>
                </DeleteAssignmentContainer>
            </TitleBox>
        </Container>
    );
};

export const DeleteAssignmentContainer = styled(TitleBoxContainer)`
    padding: 1.25rem 1.66rem 1.25rem;
`;

export const DeleteAssignmentText = styled.p`
    font-family: "PT Sans", sans-serif;
    font-size: 1.3rem;
    margin-bottom: 1.125rem;
`;

export default DeleteAssignment;
