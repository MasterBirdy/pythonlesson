const knex = require("../database/db.js");
const jwt_decode = require("jwt-decode");

module.exports = [
    {
        method: "GET",
        path: "/api/getownassignments/",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const assignments = await knex("assignments")
                    .select("assignments.lesson_id", "assignments.due_date", "lessons.name")
                    .where("user_id", "=", decoded.id)
                    .leftJoin("lessons", "assignments.lesson_id", "lessons.id");

                return h.response({ message: "Success!", assignments }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/getassignment/{lesson_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                let assignment = await knex("assignments")
                    .select("assignments.lesson_id", "assignments.due_date", "lessons.name")
                    .where("user_id", "=", decoded.id)
                    .andWhere("lesson_id", "=", req.params.lesson_id)
                    .leftJoin("lessons", "assignments.lesson_id", "lessons.id");
                if (!assignment.length) {
                    throw new Error("Assignment not found!");
                }
                assignment = assignment[0];
                return h.response({ message: "Success!", assignment }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/editassignment/{assignment_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const data = {
                    due_date: req.payload.dueDate,
                };
                if (!(new Date(req.payload.dueDate).getTime() > 0)) {
                    throw new Error("Could not validate data.");
                }
                const check = await knex("assignments")
                    .where("lesson_id", req.params.assignment_id)
                    .andWhere("user_id", decoded.id)
                    .update(data);
                if (check) {
                    return h.response({ message: "success!" }).code(200);
                } else {
                    throw new Error("Error with updating assignment");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/submitassignment",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                if (!(new Date(req.payload.dueDate).getTime() > 0)) {
                    throw new Error("Could not validate data.");
                }
                const existingAssignments = await knex("assignments")
                    .select()
                    .where("lesson_id", "=", req.payload.lessonID)
                    .andWhere("user_id", "=", decoded.id);
                if (existingAssignments.length) {
                    throw new Error("Assignment already exists!");
                }
                const assignment = await knex("assignments").insert({
                    lesson_id: req.payload.lessonID,
                    user_id: decoded.id,
                    due_date: req.payload.dueDate,
                });
                if (assignment.length) {
                    return h.response({ message: "Success!" }).code(200);
                } else {
                    throw new Error("Something happend with database insertion.");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/deleteassignment/{assignment_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const res = await knex("assignments")
                    .where("user_id", decoded.id)
                    .andWhere("lesson_id", req.params.assignment_id)
                    .del();
                if (res) {
                    return h.response({ message: "Success!" }).code(200);
                } else {
                    throw new Error("Assignment was not found.");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
];
