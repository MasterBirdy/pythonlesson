import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Input } from "../../elements/components";

const LoginForm = ({ submit, email, setEmail, password, setPassword, errors }) => {
    return (
        <Form>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="email" className={errors.email ? "error" : ""}>
                        Email
                    </Label>
                    <Input
                        type="text"
                        id="email"
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className={errors.email ? "error" : ""}
                        required
                    ></Input>
                </InputLineGrow>
            </FlexLine>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="password" className={errors.password ? "error" : ""}>
                        Password
                    </Label>
                    <Input
                        type="password"
                        id="password"
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className={errors.password ? "error" : ""}
                        required
                    ></Input>
                </InputLineGrow>
            </FlexLine>
            <div className="margin-top-small-2">
                <Button aFunction={submit}>Submit</Button>
            </div>
        </Form>
    );
};

export default LoginForm;
