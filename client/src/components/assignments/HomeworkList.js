import React from "react";
import HomeworkEntry from "./HomeworkEntry";
import {
    TitleBoxDuoColorListContainer,
    TitleBoxDouColorListEntry,
    TitleBoxDouColorListText,
} from "../../elements/components";

const AssignmentList = ({ yourAssignments }) => {
    return (
        <TitleBoxDuoColorListContainer>
            {yourAssignments.length !== 0 ? (
                yourAssignments.map((assignment) => {
                    return (
                        <HomeworkEntry
                            key={assignment.lesson_id}
                            id={assignment.lesson_id}
                            dueDate={assignment.due_date}
                            name={assignment.lessonName}
                            subjectName={assignment.subjectName}
                            totalUnits={assignment.total_units}
                            unitsCompleted={assignment.units_completed}
                        ></HomeworkEntry>
                    );
                })
            ) : (
                <TitleBoxDouColorListEntry>
                    <TitleBoxDouColorListText>Looks like no students have been added!</TitleBoxDouColorListText>
                </TitleBoxDouColorListEntry>
            )}
        </TitleBoxDuoColorListContainer>
    );
};

export default AssignmentList;
