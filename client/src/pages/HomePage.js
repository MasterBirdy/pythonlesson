import React from "react";

import { useHistory } from "react-router-dom";

import turtle from "../images/turtle-swim.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLaptopCode } from "@fortawesome/free-solid-svg-icons";
import Button from "../components/ui/Button";
import { Container } from "../elements/components";
import { below } from "../elements/utilities";
import styled, { keyframes } from "styled-components";

const HomePage = () => {
    const history = useHistory();

    return (
        <HomeBackground>
            <Container>
                <HomeMain>
                    <HomeHeader1>Learn Python Today!</HomeHeader1>
                    <HomeHeader2>Explore the seas of coding!</HomeHeader2>
                    <HomeGrid>
                        <InformationBox>
                            <FontAwesomeIcon icon={faLaptopCode} size="8x" color="#1b3e7a"></FontAwesomeIcon>
                            <InformationTextBox>
                                <ul>
                                    <li>
                                        <p>
                                            Learn Python while reinforcing concepts of math, science, and English
                                            through interactive, online lessons!
                                        </p>
                                    </li>
                                    <li>
                                        <p>Level up to earn avatars and badges by completing quests!</p>
                                    </li>
                                    <li>
                                        <p>Explore new worlds!</p>
                                    </li>
                                </ul>
                                <div className="button-toolbar-helper">
                                    <Button aFunction={() => history.push("/register")}>Register</Button>
                                    <Button aFunction={() => history.push("/login")}>Login</Button>
                                </div>
                            </InformationTextBox>
                        </InformationBox>
                        <ImageGridItem>
                            <ImageWrapper>
                                <BlobSVG
                                    viewBox="0 0 500 500"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="100%"
                                    id="blobSvg"
                                >
                                    <path
                                        id="blob"
                                        d="M436.5,298Q416,346,383,384Q350,422,300,446.5Q250,471,202,443.5Q154,416,137,370.5Q120,325,89.5,287.5Q59,250,63.5,197.5Q68,145,103.5,101Q139,57,194.5,61Q250,65,293,83Q336,101,383,123.5Q430,146,443.5,198Q457,250,436.5,298Z"
                                        fill="#d1d8e0"
                                    ></path>
                                </BlobSVG>
                                <TurtleImg src={turtle} alt="Swimming turtle"></TurtleImg>
                            </ImageWrapper>
                        </ImageGridItem>
                    </HomeGrid>
                </HomeMain>
            </Container>
            <Waves>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path
                        fill="#0099ff"
                        fillOpacity="0.4"
                        d="M0,192L48,165.3C96,139,192,85,288,96C384,107,480,181,576,192C672,203,768,149,864,117.3C960,85,1056,75,1152,69.3C1248,64,1344,64,1392,64L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
                    ></path>
                </svg>
                <Waves2>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                        <path
                            fill="#a2d9ff"
                            fillOpacity="0.4"
                            d="M0,288L48,277.3C96,267,192,245,288,202.7C384,160,480,96,576,101.3C672,107,768,181,864,229.3C960,277,1056,299,1152,266.7C1248,235,1344,149,1392,106.7L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
                        ></path>
                    </svg>
                </Waves2>
            </Waves>
        </HomeBackground>
    );
};

export const HomeBackground = styled.div`
    position: relative;
    box-sizing: border-box;

    min-height: 100vh;
    background: linear-gradient(#ffffff 50%, rgba(255, 255, 255, 0) 0) 0 0,
        radial-gradient(circle closest-side, #ffffff 53%, rgba(255, 255, 255, 0) 0) 0 0,
        radial-gradient(circle closest-side, #ffffff 50%, rgba(255, 255, 255, 0) 0) 55px 0 #48b;
    background-size: 110px 200px;
    background-repeat: repeat-x;
    .waves {
        position: fixed;
        bottom: -2rem;
        width: 100%;
        z-index: 2;

        .waves-2 {
            position: absolute;
            top: 2rem;
            left: 0;
            width: 100%;
        }
    }
`;

export const Waves = styled.div`
    position: fixed;
    bottom: -2rem;
    width: 100%;
    z-index: 2;
`;

export const Waves2 = styled.div`
    position: absolute;
    top: 2rem;
    left: 0;
    width: 100%;
`;

export const HomeMain = styled.main`
    text-align: center;
    padding-top: 15rem;
`;

export const HomeHeader1 = styled.h1`
    color: white;
    text-shadow: 1px 0px 1px black;
    font-family: "Montserrat", sans-serif;
    display: inline-block;
    text-shadow: 4px 3px 0px rgb(73, 73, 73), 9px 8px 0px rgba(0, 0, 0, 0.15);
    font-size: 6.75em;
    transition: all 0.3s ease;
    letter-spacing: 0px;
    z-index: 100;

    ${below.s`
        font-size: 4rem;
    `}

    &:hover {
        letter-spacing: 3px;
    }
`;

export const HomeHeader2 = styled.h2`
    color: white;
    font-family: "Montserrat", sans-serif;
    margin-top: 1rem;
    font-size: 2.75rem;
    text-shadow: 2px 2px 0px rgb(73, 73, 73);
    transition: all 0.3s ease;

    ${below.s`
    font-size: 2rem;
    `}

    &:hover {
        letter-spacing: 1px;
    }
`;

const HomeGrid = styled.div`
    display: grid;
    margin: 1rem auto 0;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 1rem;
    z-index: 20;
    ${below.s`
    grid-template-columns: 1fr;
    margin: 2rem auto 0;
    `}
`;

const InformationBox = styled.article`
    background-color: rgb(204, 222, 236);
    max-width: 40rem;
    max-height: 34rem;
    height: 100%;
    width: 100%;
    margin: auto;
    padding: 1.5rem 1rem;
    border-radius: 8rem;
    border: 3px solid rgb(63, 111, 148);
    box-shadow: 2px 2px 1px rgb(63, 116, 156);
    z-index: 20;

    ${below.s`
     padding: 1.5rem 0.75rem;
    `}
`;

const InformationTextBox = styled.div`
    margin: 0 auto;
    width: 100%;
    max-width: 30rem;
    margin-top: 1.75rem;
    ${below.s`
    max-width: 35rem;
    `}
    ul {
        text-align: left;
        margin-bottom: 1.5rem;
        li {
            p {
                margin-left: 1rem;
            }
            line-height: 1.2;
            font-size: 1.675em;
            font-family: "PT Sans", sans-serif;
            list-style: none;

            ${below.s`
            font-size: 1.5rem;
            `}
        }

        li:not(:last-child) {
            margin-bottom: 1rem;
        }
    }
`;

const ImageGridItem = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const ImageWrapper = styled.div`
    position: relative;
    max-width: 42.5rem;
    flex-grow: 1;
    z-index: 20;
`;

const BlobSVG = styled.svg`
    width: 100%;
    path {
        fill: rgba(54, 208, 219, 0.623);
    }
`;

const TurtleAnimation = keyframes`
    0% {
        transform: translate(-50%, -50%) rotateZ(0deg);
    }

    40% {
        transform: translate(-42%, -38%) rotateZ(7deg);
    }

    70% {
        transform: translate(-45%, -45%) rotateZ(-10deg);
    }

    100% {
        transform: translate(-50%, -50%) rotateZ(0deg);
    }
`;

const TurtleImg = styled.img`
    position: absolute;
    width: 90%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    &:hover {
        animation: ${TurtleAnimation} 1s ease-out;
        animation-iteration-count: 1;
    }
`;

export default HomePage;
