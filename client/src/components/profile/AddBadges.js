import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Select } from "../../elements/components";

const AddBadges = ({
    currentBadges,
    notCurrentBadges,
    addBadge,
    removeBadge,
    setAddBadge,
    setRemoveBadge,
    submitBadgeHandler,
}) => {
    return (
        <Form>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="addbadge">Add Badge</Label>
                    <Select
                        name="addbadge"
                        id="addbadge"
                        value={addBadge}
                        onChange={(e) => setAddBadge(e.target.value)}
                    >
                        <option value="">-- Select a Badge --</option>
                        {notCurrentBadges.map((badge) => {
                            return (
                                <option value={badge.id} key={badge.id}>
                                    {badge.name}
                                </option>
                            );
                        })}
                    </Select>
                </InputLineGrow>
            </FlexLine>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="removebadge">Remove Badge</Label>
                    <Select
                        name="removebadge"
                        id="removebadge"
                        value={removeBadge}
                        onChange={(e) => setRemoveBadge(e.target.value)}
                    >
                        <option value="">-- Select a Badge --</option>
                        {currentBadges.map((badge) => {
                            return (
                                <option value={badge.id} key={badge.id}>
                                    {badge.name}
                                </option>
                            );
                        })}
                    </Select>
                </InputLineGrow>
            </FlexLine>
            <div className="margin-top-small-2">
                <Button aFunction={(e) => submitBadgeHandler(e)}>Update</Button>
            </div>
        </Form>
    );
};

export default AddBadges;
