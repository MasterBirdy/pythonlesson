import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Input, Select } from "../../elements/components";

const EditProfileForm = ({ firstName, lastName, setFirstName, setLastName, grade, setGrade, profileHandler, role }) => {
    return (
        <Form>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="firstname">First Name</Label>
                    <Input
                        type="text"
                        id="firstname"
                        name="firstname"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                    ></Input>
                </InputLineGrow>
                <InputLineGrow>
                    <Label htmlFor="lastname">Last Name</Label>
                    <Input
                        type="text"
                        id="lastname"
                        name="lastname"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                    ></Input>
                </InputLineGrow>
            </FlexLine>
            {role === "student" && (
                <FlexLine>
                    <InputLineGrow>
                        <Label htmlFor="grade">Grade</Label>
                        <Select name="grade" id="grade" value={grade} onChange={(e) => setGrade(e.target.value)}>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="5">5th</option>
                            <option value="6">6th</option>
                            <option value="7">7th</option>
                            <option value="8">8th</option>
                        </Select>
                    </InputLineGrow>
                </FlexLine>
            )}

            <div className="margin-top-small-2">
                <Button aFunction={profileHandler}>Submit</Button>
            </div>
        </Form>
    );
};

export default EditProfileForm;
