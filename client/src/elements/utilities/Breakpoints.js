import { css } from "styled-components";

const size = {
    xxs: 480,
    xs: 600,
    s: 768,
    m: 1024,
    l: 1200,
};

export const below = Object.keys(size).reduce((accumulator, label) => {
    accumulator[label] = (...args) => css`
        @media (max-width: ${size[label]}px) {
            ${css(...args)};
        }
    `;
    return accumulator;
}, {});
