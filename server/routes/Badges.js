const knex = require("../database/db.js");
const jwt_decode = require("jwt-decode");
const badgesToCheck = [1, 2, 3, 4];
const adjectives = [
    "Awesome!",
    "Great!",
    "Super!",
    "Well done!",
    "Amazing!",
    "Incredible!",
    "Excellent!",
    "Impressive!",
    "Wow!",
    "Outstanding!",
];
module.exports = [
    {
        method: "GET",
        path: "/api/getbadges/{user_id}",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const ownedBadges = await knex("users_badges")
                    .select("badge_id AS id", "badges.name")
                    .join("badges", function () {
                        this.on("badges.id", "=", "users_badges.badge_id");
                    })
                    .where("user_id", req.params.user_id);

                const notOwnedBadges = await knex("badges")
                    .select("id", "name")
                    .leftJoin(
                        knex("users_badges").select("*").where("user_id", req.params.user_id).as("x"),
                        "badges.id",
                        "x.badge_id"
                    )
                    .whereNull("user_id");

                if (!ownedBadges && !notOwnedBadges) {
                    throw new Error("Error with retrieving badges!");
                }

                return h
                    .response({
                        ownedBadges,
                        notOwnedBadges,
                    })
                    .code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/checkforbadges",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const badges = await knex("users_badges").where("user_id", decoded.id);
                const badgesObject = {};
                const eventsObject = {};
                badges.forEach((badge) => {
                    badgesObject[badge.badge_id] = 1;
                });

                if (badgesToCheck.some((badge) => !badgesObject[badge])) {
                    const users = await knex("users").select("grade").where("id", decoded.id);
                    if (!users.length) {
                        throw new Error("Could not find user!");
                    }
                    const grade = users[0].grade;
                    if (!badgesObject[1]) {
                        let res = await knex.raw(`SELECT user_id, users_units.lesson_id, lesson_table.name, lesson_table.total_units, count(*) AS units_completed FROM users_units
                        INNER JOIN (
                            SELECT id, name, COUNT(*) AS total_units FROM lessons
                        LEFT JOIN units
                        ON units.lesson_id = lessons.id
                        GROUP BY lessons.id
                        ) as lesson_table
                        ON lesson_id = lesson_table.id
                        GROUP BY users_units.user_id, users_units.lesson_id
                        HAVING user_id = ${decoded.id}
                        AND lesson_id > ${grade > 6 ? 1 : 5}
                        AND lesson_id < ${grade > 6 ? 5 : 9}`);
                        let turtleRes = res[0];

                        if (
                            turtleRes.length === 3 &&
                            turtleRes.every((turtleLesson) => turtleLesson.total_units === turtleLesson.units_completed)
                        ) {
                            await knex("users_badges").insert({ user_id: decoded.id, badge_id: 1 });
                            let randomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                            await knex("event_log").insert({
                                user_id: decoded.id,
                                xp: 0,
                                message: `${randomAdjective} ${decoded.first_name} got the Turtle Quest Conquerer Badge!`,
                            });
                            eventsObject["turtleQuest"] = "You got the Turtle Quest Conquerer Badge! 😁`";
                        }
                    }
                    if (!badgesObject[2] || !badgesObject[3] || !badgesObject[4]) {
                        let res = await knex.raw(`SELECT user_id, users_units.lesson_id, lesson_table.name, lesson_table.total_units, count(*) AS units_completed FROM users_units
                        INNER JOIN (
                            SELECT id, name, COUNT(*) AS total_units FROM lessons
                        LEFT JOIN units
                        ON units.lesson_id = lessons.id
                        GROUP BY lessons.id
                        ) as lesson_table
                        ON lesson_id = lesson_table.id
                        GROUP BY users_units.user_id, users_units.lesson_id
                        HAVING user_id = ${decoded.id}`);
                        let moduleRes = res[0];
                        let randomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                        const total = moduleRes.reduce((acc, cur) => {
                            return cur.total_units === cur.units_completed ? acc + 1 : acc;
                        }, 0);
                        if (!badgesObject[2] && total >= 5) {
                            await knex("users_badges").insert({ user_id: decoded.id, badge_id: 2 });
                            await knex("event_log").insert({
                                user_id: decoded.id,
                                xp: 0,
                                message: `${randomAdjective} ${decoded.first_name} got the 5 Modules Completed Badge!`,
                            });
                            eventsObject["5Modules"] = "You got the 5 Modules Completed Badge! 😁`";
                        } else if (!badgesObject[3] && total >= 10) {
                            await knex("users_badges").insert({ user_id: decoded.id, badge_id: 3 });
                            await knex("event_log").insert({
                                user_id: decoded.id,
                                xp: 0,
                                message: `${randomAdjective} ${decoded.first_name} got the 10 Modules Completed Badge!`,
                            });
                            eventsObject["10Modules"] = "You got the 10 Modules Completed Badge! 😁`";
                        } else if (!badgesObject[4] && total >= 15) {
                            await knex("users_badges").insert({ user_id: decoded.id, badge_id: 4 });
                            await knex("event_log").insert({
                                user_id: decoded.id,
                                xp: 0,
                                message: `${randomAdjective} ${decoded.first_name} got the 15 Modules Completed Badge!`,
                            });
                            eventsObject["5Modules"] = "You got the 15 Modules Completed Badge! 😁`";
                        }
                    }
                }
                return h.response({ message: "Success!", eventsObject }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/addbadge/{user_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                if (!Number.isInteger(parseInt(req.params.user_id))) {
                    throw new Error("Invalid ID!");
                }
                if (!req.payload.badge) {
                    throw new Error("No badge inputted!");
                }
                let student = await knex
                    .select("id")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .andWhere("id", req.params.user_id)
                    .limit(1);
                if (!student.length) {
                    throw new Error("Student not found!");
                }
                const res = await knex.select("name").from("badges").where("id", req.payload.badge);

                if (!res.length) {
                    throw new Error("Badge not found!");
                }
                await knex.raw(
                    knex("users_badges")
                        .insert({ user_id: req.params.user_id, badge_id: req.payload.badge })
                        .toString()
                        .replace("insert", "INSERT IGNORE")
                );
                let randomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                await knex("event_log").insert({
                    user_id: req.params.user_id,
                    xp: 0,
                    message: `${randomAdjective} Your teacher gave you the ${res[0].name} Badge!`,
                });
                return h.response({ message: "Success!" }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/removebadge/{user_id}",
        config: {
            auth: "teacher",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);

                if (!Number.isInteger(parseInt(req.params.user_id))) {
                    throw new Error("Invalid ID!");
                }
                if (!req.payload.badge) {
                    throw new Error("No badge inputted!");
                }
                let student = await knex
                    .select("id")
                    .table("users")
                    .where("student_at", decoded.teacher_at)
                    .andWhere("taught_by", decoded.id)
                    .andWhere("id", req.params.user_id)
                    .limit(1);
                if (!student.length) {
                    throw new Error("Student not found!");
                }
                const res = await knex("users_badges")
                    .where("user_id", req.params.user_id)
                    .andWhere("badge_id", req.payload.badge)
                    .del();
                if (res) {
                    return h.response({ message: "Success!" }).code(200);
                } else {
                    throw new Error("Badge was not found.");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
];
