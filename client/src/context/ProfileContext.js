import React, { useState, useContext, createContext, useEffect } from "react";
import { AuthContext } from "../context/AuthContext";
import { makeToast } from "../utils/helpers";
import axios from "axios";

const ProfileContext = createContext();
const { Provider } = ProfileContext;

const ProfileProvider = ({ children }) => {
    const { isAuthenticated, loading, logout } = useContext(AuthContext);
    const [log, setLog] = useState([]);
    const [badges, setBadges] = useState([]);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [grade, setGrade] = useState("");
    const [schoolName, setSchoolName] = useState("");
    const [level, setLevel] = useState(1);
    const [currentXP, setCurrentXP] = useState(0);
    const [minXPNeeded, setMinXPNeeded] = useState(1);
    const [maxXPLastLevel, setMaxXPLastLevel] = useState(0);
    const [avatar, setAvatar] = useState(-1);
    const [role, setRole] = useState("");

    const update = () => {
        if (!loading && isAuthenticated()) {
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/profileinfo`,
                withCredentials: true,
            })
                .then((res) => {
                    setFirstName(res.data.firstName);
                    setLastName(res.data.lastName);
                    setGrade(res.data.grade);
                    setSchoolName(res.data.schoolName);
                    setLog(res.data.log);
                    setLevel(res.data.level[0].level);
                    setCurrentXP(res.data.currentXP);
                    setMinXPNeeded(res.data.minXP);
                    setMaxXPLastLevel(res.data.maxXP);
                    setRole(res.data.role);
                    setAvatar(res.data.avatar);
                    setBadges(res.data.badges);
                })
                .catch((err) => {
                    console.log(err);
                    if (err.response) {
                        if (err.response.status === 401 || err.response.status === 403) {
                            makeToast("Error with login authorization. Please log in!");
                            logout();
                        }
                    }
                });
        }
    };

    useEffect(() => {
        update();
        //eslint-disable-next-line
    }, [loading, isAuthenticated]);

    return (
        <Provider
            value={{
                log,
                firstName,
                lastName,
                grade,
                schoolName,
                level,
                currentXP,
                minXPNeeded,
                maxXPLastLevel,
                role,
                avatar,
                badges,
                update,
            }}
        >
            {children}
        </Provider>
    );
};

export { ProfileContext, ProfileProvider };
