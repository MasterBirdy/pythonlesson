import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import ViewAssignmentStatsRow from "./ViewAssignmentStatsRow";
import {
    SearchResultsGrid,
    SearchResultsHeading,
    SearchResultsCellHeading,
    SearchResultsText,
} from "../search/SearchResults";
import styled from "styled-components";

const ViewAssignmentStatsResults = ({
    students,
    isMobile,
    firstNameSort,
    setFirstNameSort,
    lastNameSort,
    setLastNameSort,
    gradeSort,
    setGradeSort,
    progressSort,
    setProgressSort,
    unitsSort,
    setUnitsSort,
    reverse,
}) => {
    let results = <div className="view-assignment-stats-row empty">No results returned.</div>;

    if (students.length) {
        results = students.map((student) => {
            return (
                <ViewAssignmentStatsRow
                    key={student.id}
                    firstName={student.first_name}
                    lastName={student.last_name}
                    grade={student.grade}
                    lessonID={student.id}
                    totalUnits={student.total_units}
                    unitsCompletedArray={student.unitsCompleted}
                    unitsCompleted={student.lessons_completed}
                ></ViewAssignmentStatsRow>
            );
        });
    }

    return (
        <SearchResultsGrid>
            <ViewAssignmentHeading>
                <SearchResultsCellHeading col="2">
                    <SearchResultsText>{isMobile ? "First" : "First Name"}</SearchResultsText>
                    {firstNameSort && (
                        <FontAwesomeIcon
                            icon={reverse ? faCaretUp : faCaretDown}
                            color={reverse ? "crimson" : "green"}
                        ></FontAwesomeIcon>
                    )}
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="2">
                    <SearchResultsText>{isMobile ? "Last" : "Last Name"}</SearchResultsText>
                    {lastNameSort && (
                        <FontAwesomeIcon
                            icon={reverse ? faCaretUp : faCaretDown}
                            color={reverse ? "crimson" : "green"}
                        ></FontAwesomeIcon>
                    )}
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="2">
                    <SearchResultsText>{isMobile ? "Gr." : "Grade"}</SearchResultsText>
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="2">
                    <SearchResultsText>{isMobile ? "Les." : "Lessons"}</SearchResultsText>
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="4">
                    <SearchResultsText>Units</SearchResultsText>
                </SearchResultsCellHeading>
            </ViewAssignmentHeading>
            {results}
        </SearchResultsGrid>
    );
};

const ViewAssignmentHeading = styled(SearchResultsHeading)`
    grid-template-columns: repeat(12, 1fr);
`;

export default ViewAssignmentStatsResults;
