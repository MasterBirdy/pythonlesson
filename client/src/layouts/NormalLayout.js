import React from "react";
import NavBar from "../components/ui/NavBar";

const NormalLayout = ({ children }) => {
    return (
        <>
            <NavBar></NavBar>
            {children}
        </>
    );
};

export default NormalLayout;
