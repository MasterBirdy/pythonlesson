import React from "react";
import Button from "../ui/Button";

const SearchForm = ({
    parameters,
    setParameters,
    grade,
    setGrade,
    subjectAreas,
    chosenSubjectAreas,
    setChosenSubjectAreas,
    formHandler,
    clearHandler,

    role,
}) => {
    return (
        <form className="form-component">
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="parameters">Keywords</label>
                    <input
                        type="text"
                        id="parameters"
                        name="parameters"
                        value={parameters}
                        onChange={(e) => setParameters(e.target.value)}
                    ></input>
                </div>
            </div>
            {role === "teacher" && (
                <div className="flex-line">
                    <div className="input-item-grow">
                        <label htmlFor="grade">Grade Level</label>
                        <select name="grade" id="grade" value={grade} onChange={(e) => setGrade(e.target.value)}>
                            <option disabled value="">
                                -- All Grades --
                            </option>
                            <option value="3">3rd</option>
                            <option value="4">4th</option>
                            <option value="5">5th</option>
                            <option value="6">6th</option>
                            <option value="7">7th</option>
                            <option value="8">8th</option>
                        </select>
                    </div>
                </div>
            )}
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="grade">Subjects</label>
                    <select
                        className="multiple-items"
                        name="grade"
                        id="grade"
                        value={chosenSubjectAreas}
                        multiple
                        size="3"
                        onChange={(e) => {
                            setChosenSubjectAreas([].slice.call(e.target.selectedOptions).map((o) => o.value));
                        }}
                    >
                        {Object.keys(subjectAreas).map((key) => {
                            return (
                                <option key={subjectAreas[key].id} value={subjectAreas[key].id}>
                                    {subjectAreas[key].name}
                                </option>
                            );
                        })}
                    </select>
                </div>
            </div>
            <div className="button-toolbar-helper margin-top-small-3">
                <Button aFunction={(e) => formHandler(e)}>Search</Button>
                <Button aFunction={(e) => clearHandler(e)}>Clear</Button>
            </div>
        </form>
    );
};

export default SearchForm;
