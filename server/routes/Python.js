"use strict";

const fs = require("fs").promises;
const jwt_decode = require("jwt-decode");
const knex = require("../database/db.js");
const uuidv4 = /.*[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}.*$/i;
const adjectives = [
    "Awesome!",
    "Great!",
    "Super!",
    "Well done!",
    "Amazing!",
    "Incredible!",
    "Excellent!",
    "Impressive!",
    "Wow!",
    "Outstanding!",
];
const Joi = require("@hapi/joi");
const submissionSchema = Joi.object({
    submission: Joi.string().required(),
});

module.exports = [
    {
        method: "GET",
        path: "/api/lessons/{lesson_id}",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                let units = await knex.select().table("units").where("lesson_id", req.params.lesson_id);
                if (units.length < 1) {
                    throw new Error("Lesson not found!");
                } else {
                    const decoded = jwt_decode(req.state.token);
                    const unitsCompleted = await knex
                        .select("unit_id", "completed_on")
                        .table("users_units")
                        .where("user_id", decoded.id)
                        .andWhere("lesson_id", req.params.lesson_id);
                    const unitsCompletedObject = {};
                    unitsCompleted.forEach((unit) => {
                        unitsCompletedObject[unit.unit_id] = unit.completed_on;
                    });
                    return h.response({ units, unitsCompleted: unitsCompletedObject }).code(200);
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/turtleLessons/{lesson_id}/{unit_id}",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const validation = submissionSchema.validate(
                    { submission: req.payload.submission },
                    { escapeHtml: true }
                );
                if (validation.error) {
                    throw new Error("There was an error with validating the submission.");
                }
                if (uuidv4.test(req.payload.submission)) {
                    const splitArray = req.payload.submission.split("|||");
                    const verifiers = await knex
                        .select("verified_value", "error_message")
                        .table("verifiers")
                        .where("lesson_id", req.params.lesson_id)
                        .andWhere("unit_id", req.params.unit_id)
                        .orderBy("verifier_id");

                    let allAnswersCorrect = true;

                    const anotherRandomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                    let message = anotherRandomAdjective;
                    if (verifiers.length) {
                        verifiers.forEach((verifier, index) => {
                            if (index < splitArray.length) {
                                const testAnswer = splitArray[index].split("!?!?")[1];

                                if (testAnswer !== verifier.verified_value) {
                                    allAnswersCorrect = false;
                                    message = verifier.error_message;
                                }
                            }
                        });
                    }

                    if (allAnswersCorrect) {
                        const decoded = jwt_decode(req.state.token);
                        const checkIfExists = await knex
                            .select()
                            .from("users_units")
                            .where("user_id", decoded.id)
                            .andWhere("lesson_id", req.params.lesson_id)
                            .andWhere("unit_id", req.params.unit_id);
                        if (checkIfExists.length > 0) {
                            throw new Error("Submission already exists!");
                        }

                        const result = await knex("users_units").insert({
                            user_id: decoded.id,
                            lesson_id: req.params.lesson_id,
                            unit_id: req.params.unit_id,
                        });
                        if (result.length > 0) {
                            const unitsCompleted = await knex
                                .select("unit_id", "completed_on")
                                .table("users_units")
                                .where("user_id", decoded.id)
                                .andWhere("lesson_id", req.params.lesson_id);
                            const unitsCompletedObject = {};
                            unitsCompleted.forEach((unit) => {
                                unitsCompletedObject[unit.unit_id] = unit.completed_on;
                            });
                            let units = await knex
                                .select("unit_id")
                                .table("units")
                                .where("lesson_id", req.params.lesson_id);
                            if (Object.keys(unitsCompleted).length === units.length) {
                                let lessonExp = await knex
                                    .select("exp", "name")
                                    .from("lessons")
                                    .where("id", req.params.lesson_id)
                                    .limit(1);
                                if (lessonExp.length <= 0) {
                                    throw new Error("Lesson not found!");
                                }

                                const eventsObject = {};

                                let xpResults = await knex.raw(`SELECT max(level) AS max
                                FROM levels, (SELECT xp FROM users WHERE id = ${decoded.id}) as userXP
                                WHERE xp - min_xp >= 0
                                LIMIT 1`);
                                if (xpResults[0].length <= 0) {
                                    throw new Error("XP not found!");
                                }

                                const previousLevel = xpResults[0][0].max;

                                const lessonExpValue = lessonExp[0].exp;
                                const lessonExpName = lessonExp[0].name;
                                let randomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                                let completed = await knex("event_log").insert({
                                    user_id: decoded.id,
                                    xp: lessonExpValue,
                                    message: `${randomAdjective} ${decoded.first_name} got ${lessonExpValue} XP for completing ${lessonExpName}!`,
                                });

                                randomAdjective = adjectives[Math.floor(Math.random() * adjectives.length)];
                                eventsObject.completedLesson = `${randomAdjective} You completed ${lessonExpName} for ${lessonExpValue} XP! 🎉`;

                                xpResults = await knex.raw(`SELECT max(level) AS max
                                FROM levels, (SELECT xp FROM users WHERE id = ${decoded.id}) as userXP
                                WHERE xp - min_xp >= 0
                                LIMIT 1`);

                                if (xpResults[0].length <= 0) {
                                    throw new Error("XP not found!");
                                }

                                const currentLevel = xpResults[0][0].max;

                                if (currentLevel > previousLevel) {
                                    await knex("event_log").insert({
                                        user_id: decoded.id,
                                        xp: 0,
                                        message: `${randomAdjective} ${decoded.first_name} leveled up to Level ${currentLevel}!`,
                                    });
                                    eventsObject.levelUp = `You leveled up to Level ${currentLevel}! 🤩`;
                                }

                                if (completed.length > 0) {
                                    return h.response({
                                        message,
                                        isCorrect: allAnswersCorrect,
                                        eventsObject,
                                        unitsCompleted: unitsCompletedObject,
                                    });
                                } else {
                                    throw new Error("Error with completion of lesson!");
                                }
                            } else {
                                return h
                                    .response({
                                        message,
                                        isCorrect: allAnswersCorrect,
                                        unitsCompleted: unitsCompletedObject,
                                    })
                                    .code(200);
                            }
                        } else {
                            throw new Error("Error with database submission");
                        }
                    }
                    return h.response({ message, isCorrect: allAnswersCorrect }).code(200);
                } else {
                    throw new Error("Invalid Submission");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
];
