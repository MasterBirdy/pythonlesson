import React from "react";
import Button from "../ui/Button";
import StandardsAutoComplete from "./StandardsAutoComplete";

const SearchStandardsForm = ({
    standardInput,

    standards,
    formHandler,
    clearHandler,
    showAutoComplete,
    autoCompleteHandler,
    standardInputHandler,
    autoCompleteRef,
}) => {
    return (
        <form className="form-component">
            <StandardsAutoComplete
                standardInputHandler={standardInputHandler}
                autoCompleteRef={autoCompleteRef}
                standardInput={standardInput}
                standards={standards}
                showAutoComplete={showAutoComplete}
                autoCompleteHandler={autoCompleteHandler}
            ></StandardsAutoComplete>

            <div className="button-toolbar-helper margin-top-small-3">
                <Button aFunction={(e) => formHandler(e)}>Search</Button>
                <Button aFunction={(e) => clearHandler(e)}>Clear</Button>
            </div>
        </form>
    );
};

export default SearchStandardsForm;
