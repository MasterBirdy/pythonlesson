import React from "react";

const AltButton = ({ children, aFunction }) => {
    return (
        <button className="alt-button" onClick={aFunction}>
            {children}
        </button>
    );
};

export default AltButton;
