import React from "react";

import Button from "../ui/Button";

const RegisterForm = ({
    firstName,
    setFirstName,
    lastName,
    setLastName,
    email,
    setEmail,
    password,
    setPassword,
    confirmPassword,
    setConfirmPassword,
    registerHandler,
    role,
    roleHandler,
    passcode,
    setPasscode,
    grade,
    gradeHandler,
    schools,
    currentSchool,
    schoolHandler,
    errors,
}) => {
    return (
        <>
            <form className="form-component">
                <div className="flex-line">
                    <div className="input-item-grow">
                        <label htmlFor="first_name" className={errors.firstName ? "error" : ""}>
                            First Name
                        </label>
                        <input
                            type="text"
                            id="first_name"
                            name="first_name"
                            value={firstName}
                            required
                            onChange={(e) => setFirstName(e.target.value)}
                            className={errors.firstName ? "error" : ""}
                        ></input>
                    </div>
                    <div className="input-item-grow">
                        <label htmlFor="last_name" className={errors.lastName ? "error" : ""}>
                            Last Name
                        </label>
                        <input
                            type="text"
                            id="last_name"
                            name="last_name"
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                            required
                            className={errors.lastName ? "error" : ""}
                        ></input>
                    </div>
                </div>
                <div>
                    <label htmlFor="email" className={errors.email ? "error" : ""}>
                        Email
                    </label>
                    <input
                        type="text"
                        id="email"
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                        className={errors.email ? "error" : ""}
                    ></input>
                </div>
                <div className="flex-line">
                    <div className="input-item-grow">
                        <label htmlFor="password" className={errors.password ? "error" : ""}>
                            Password
                        </label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            value={password}
                            required
                            onChange={(e) => setPassword(e.target.value)}
                            className={errors.password ? "error" : ""}
                        ></input>
                    </div>
                    <div className="input-item-grow">
                        <label htmlFor="last_name" className={errors.password ? "error" : ""}>
                            Confirm Password
                        </label>
                        <input
                            type="password"
                            id="confirm_password"
                            name="confirm_password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            required
                            className={errors.password ? "error" : ""}
                        ></input>
                    </div>
                </div>
                <div>
                    <div className="flex-line">
                        <div className="input-item-grow">
                            <label htmlFor="school" className={errors.school ? "error" : ""}>
                                School
                            </label>
                            <select
                                name="school"
                                id="school"
                                value={currentSchool}
                                className={errors.school ? "error" : ""}
                                onChange={(e) => schoolHandler(e)}
                            >
                                <option disabled value="">
                                    -- Please Select --
                                </option>
                                {schools.map((school) => (
                                    <option key={school.id} value={school.id}>
                                        {school.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                </div>
                <div className="flex-line">
                    <div className="input-item-grow">
                        <label htmlFor="role" className={errors.role ? "error" : ""}>
                            Are you a...?
                        </label>
                        <select
                            name="role"
                            id="role"
                            className={errors.role ? "error" : ""}
                            onChange={(e) => roleHandler(e)}
                            value={role}
                        >
                            <option disabled value="">
                                -- Please Select --
                            </option>
                            <option value="teacher">Teacher</option>
                            <option value="student">Student</option>
                        </select>
                    </div>
                </div>
                {role === "teacher" && (
                    <div className="flex-line">
                        <div className="input-item-grow">
                            <label htmlFor="passcode" className={errors.passcode ? "error" : ""}>
                                Great! Enter a passcode for your school.
                            </label>
                            <input
                                type="password"
                                id="passcode"
                                name="passcode"
                                value={passcode}
                                className={errors.passcode ? "error" : ""}
                                required
                                onChange={(e) => setPasscode(e.target.value)}
                            ></input>
                        </div>
                    </div>
                )}
                {role === "student" && (
                    <div className="flex-line">
                        <div className="input-item-grow">
                            <label htmlFor="grade" className={errors.grade ? "error" : ""}>
                                Awesome! What grade are you in?
                            </label>
                            <select
                                name="grade"
                                id="grade"
                                value={grade}
                                className={errors.grade ? "error" : ""}
                                onChange={(e) => gradeHandler(e)}
                            >
                                <option disabled value="">
                                    -- Please Select --
                                </option>
                                <option value="3">3rd</option>
                                <option value="4">4th</option>
                                <option value="5">5th</option>
                                <option value="6">6th</option>
                                <option value="7">7th</option>
                                <option value="8">8th</option>
                            </select>
                        </div>
                    </div>
                )}
            </form>
            <div className="margin-top-small">
                <Button aFunction={registerHandler}>Register</Button>
            </div>
        </>
    );
};

export default RegisterForm;
