import styled, { css } from "styled-components";
import { colors, flex, below } from "../utilities";

export const Form = styled.form`
    padding-top: ${(props) => (props.paddingTop ? props.paddingTop : ".75")}rem;
    font-family: "PT Sans", sans-serif;
`;

export const Label = styled.label`
    display: block;
    font-size: 1.35rem;
    margin-bottom: 0.33rem;
    margin-left: 0.25rem;
    transition: all 0.2s ease;
    color: black;

    &.error {
        color: ${colors.formLabelError};
    }
`;

export const FormCSS = css`
    border-radius: 12px;
    padding: 0.5rem 1rem;
    border: 2px solid ${colors.formBorder};
    margin-bottom: 0.8rem;
    width: 100%;
    font-family: "PT Sans", sans-serif;
    box-shadow: 2px 3px 12px -6px ${colors.formBoxShadow};
    transition: all 0.2s ease;
    &:focus {
        outline: none;
        border-color: ${colors.formInputSelected};
        box-shadow: 2px 3px 12px -6px ${colors.formInputSelectedBoxShadow};
    }
    &.error {
        color: ${colors.formInputError};
    }
`;

export const Input = styled.input`
    ${FormCSS}
`;

export const Select = styled.select`
    display: block;
    background-color: white;
    ${FormCSS};
`;

export const FlexLine = styled.div`
    ${flex({ justifyContent: "space-between" })}
    ${below.xs`
        flex-direction: column;
        max-width: none;
    `}
`;

export const InputLineGrow = styled.div`
    flex-grow: 1;
    max-width: 35rem;
    width: 100%;
    ${below.s`
        max-width: none;
    `}
    ${below.xs`
        max-width: 27rem;
    `}
    &:not(:last-child) {
        margin-right: 2rem;
    }
`;
