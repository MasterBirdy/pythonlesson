import React from "react";

import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxDuoColorListContainer,
    TitleBoxDouColorListEntry,
    TitleBoxDouColorListText,
} from "../../elements/components";
import styled from "styled-components";
import { below } from "../../elements/utilities";
const compliment = /^[A-Z0-9]*\s?[A-Z0-9]*!/gi;
const experience = /[0-9]{1,3}\sXP/gi;

const ActivityLog = ({ log }) => {
    let newLog = log.map((entry) => {
        let testEntry = entry;
        if (testEntry.match(compliment)) {
            const testVariable = entry.match(compliment)[0];
            testEntry = testEntry.replace(testVariable, `<strong>${testVariable}</strong>`);
        }

        if (testEntry.match(experience)) {
            const testVariable = entry.match(experience)[0];
            testEntry = testEntry.replace(testVariable, `<strong>${testVariable}</strong>`);
        }
        return testEntry;
    });

    return (
        <ActivityLogTitleBox>
            <TitleBoxTitleContainer>
                <TitleBoxTitle>Activity Log</TitleBoxTitle>
            </TitleBoxTitleContainer>
            <TitleBoxDuoColorListContainer>
                {newLog.length !== 0 ? (
                    newLog.map((entry, index) => {
                        return (
                            <TitleBoxDouColorListEntry key={index}>
                                <TitleBoxDouColorListText
                                    dangerouslySetInnerHTML={{ __html: entry }}
                                ></TitleBoxDouColorListText>
                            </TitleBoxDouColorListEntry>
                        );
                    })
                ) : (
                    <TitleBoxDouColorListEntry>
                        <TitleBoxDouColorListText>Looks like there are no entries here!</TitleBoxDouColorListText>
                    </TitleBoxDouColorListEntry>
                )}
            </TitleBoxDuoColorListContainer>
        </ActivityLogTitleBox>
    );
};

const ActivityLogTitleBox = styled(TitleBox)`
    ${below.s`
    order: 2;
    margin-top: 2rem;
    `}
`;

export default ActivityLog;
