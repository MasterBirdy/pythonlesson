import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Input } from "../../elements/components";

const ResetPasswordForm = ({ errors, password, setPassword, confirmPassword, setConfirmPassword, submit, loading }) => {
    return (
        <Form>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="password" className={errors.password ? "error" : ""}>
                        Password
                    </Label>
                    <Input
                        type="password"
                        id="password"
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className={errors.password ? "error" : ""}
                        required
                    ></Input>
                </InputLineGrow>
                <InputLineGrow>
                    <Label htmlFor="confirmpassword" className={errors.email ? "error" : ""}>
                        Confirm Password
                    </Label>
                    <Input
                        type="password"
                        id="confirmpassword"
                        name="confirmpassword"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        className={errors.confirmPassword ? "error" : ""}
                        required
                    ></Input>
                </InputLineGrow>
            </FlexLine>
            <div className="margin-top-small-2 flex-center-helper">
                <Button aFunction={(e) => submit(e)}>Submit</Button>
                {loading && <div className="spinner-loading-alt"></div>}
            </div>
        </Form>
    );
};

export default ResetPasswordForm;
