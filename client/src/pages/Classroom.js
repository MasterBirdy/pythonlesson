import React, { useEffect, useState } from "react";
import axios from "axios";
import ClassroomList from "../components/classroom/ClassroomList";
import AssignmentList from "../components/classroom/AssignmentList";
import ErrorComponent from "../components/ui/ErrorComponent";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus, faQuestion, faPlus } from "@fortawesome/free-solid-svg-icons";
import { makeInfoToast } from "../utils/helpers";
import Pagination from "../components/search/Pagination";
import { usePagination } from "../hooks/usePagination";
import useMobile from "../hooks/useMobile";
import { TitleBox, TitleBoxTitle, TitleBoxTitleContainer, IconsContainer, Container } from "../elements/components/";

const pageLimit = 5;

const Classroom = () => {
    const [yourStudents, setYourStudents] = useState([]);
    const [yourAssignments, setYourAssignments] = useState([]);
    const [loading, setLoading] = useState(true);
    const [invalid, setInvalid] = useState(false);
    const [invalidMessage, setInvalidMessage] = useState(false);
    const [isMobile] = useMobile();
    const {
        currentPage: currentAssignmentPage,
        currentLimit: currentAssignmentLimit,
        paginationHandler: assignmentPaginationHandler,
    } = usePagination(pageLimit, yourAssignments.length);
    const {
        currentPage: currentStudentPage,
        currentLimit: currentStudentLimit,
        paginationHandler: studentPaginationHandler,
    } = usePagination(pageLimit, yourStudents.length);

    useEffect(() => {
        const promises = [
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getstudentsofschool/`,
                withCredentials: true,
            }),
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getownassignments/`,
                withCredentials: true,
            }),
        ];

        Promise.all(promises)
            .then((res) => {
                if (res[0].status === 200) {
                    setYourStudents(res[0].data.teacherStudents);
                }
                if (res[1].status === 200) {
                    setYourAssignments(res[1].data.assignments);
                }
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 401 || err.response.status === 403) {
                        setInvalid(true);
                        setInvalidMessage("You are unauthorized to view this page.");
                    }
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    let slicedStudentsResults = yourStudents.slice(
        currentStudentPage * pageLimit,
        currentStudentPage * pageLimit + pageLimit
    );

    let slicedAssignmentResults = yourAssignments.slice(
        currentAssignmentPage * pageLimit,
        currentAssignmentPage * pageLimit + pageLimit
    );

    slicedAssignmentResults
        .sort((a, b) => {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
        })
        .sort((a, b) => {
            const aDate = new Date(a.due_date);
            const bDate = new Date(b.due_date);
            if (aDate > bDate) return 1;
            if (aDate < bDate) return -1;
            return 0;
        });

    if (loading) {
        return <div className="loader"></div>;
    }

    if (invalid) {
        return (
            <Container>
                <ErrorComponent errorMessage={invalidMessage}></ErrorComponent>
            </Container>
        );
    }

    return (
        <Container>
            <TitleBox marginBottom="1.5" marginTop="2" maxWidth="55rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Student List</TitleBoxTitle>
                    <IconsContainer>
                        <Link to="/addtoclassroom">
                            <FontAwesomeIcon icon={faUserPlus}></FontAwesomeIcon>
                        </Link>
                        <FontAwesomeIcon
                            icon={faQuestion}
                            onClick={() =>
                                makeInfoToast(
                                    `ℹ️ This is your classroom roster, in which you can view a student's stats and edit their profiles. You can add/remove available students from your classroom.`
                                )
                            }
                        ></FontAwesomeIcon>
                    </IconsContainer>
                </TitleBoxTitleContainer>
                <ClassroomList yourStudents={slicedStudentsResults}></ClassroomList>
            </TitleBox>

            <Pagination
                currentPage={currentStudentPage}
                currentLimit={currentStudentLimit}
                totalNumber={yourStudents.length}
                pageLimit={pageLimit}
                paginationHandler={studentPaginationHandler}
                isMobile={isMobile}
            ></Pagination>

            <TitleBox marginTop="2" marginBottom="1.5" maxWidth="55rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Assignments</TitleBoxTitle>
                    <IconsContainer>
                        <Link to="/addassignment">
                            <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                        </Link>
                        <FontAwesomeIcon
                            icon={faQuestion}
                            onClick={() =>
                                makeInfoToast(
                                    `ℹ️ This is your list of current assignments for your classroom. Assignments tell your students to complete lesson modules by a certain date and time.`
                                )
                            }
                        ></FontAwesomeIcon>
                    </IconsContainer>
                </TitleBoxTitleContainer>
                <AssignmentList assignments={slicedAssignmentResults}></AssignmentList>
            </TitleBox>

            <Pagination
                currentPage={currentAssignmentPage}
                currentLimit={currentAssignmentLimit}
                totalNumber={yourAssignments.length}
                pageLimit={pageLimit}
                paginationHandler={assignmentPaginationHandler}
                isMobile={isMobile}
            ></Pagination>
        </Container>
    );
};

export default Classroom;
