import React from "react";
import { Link } from "react-router-dom";
import { parseDate } from "../../utils/helpers";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import {
    TitleBoxDouColorListEntry,
    TitleBoxDouColorListText,
    AssignmentEntryLink,
    AssignmentTextLine,
} from "../../elements/components";
import styled from "styled-components";

const HomeworkEntry = ({ id, subjectName, totalUnits, unitsCompleted, name, dueDate }) => {
    const { date, time, realMonth } = parseDate(dueDate);

    return (
        <TitleBoxDouColorListEntry>
            <AssignmentEntryLink to={`/turtlelesson/${id}`}>
                <TitleBoxDouColorListText className={date < Date.now() ? "due-assignment" : ""}>
                    {name}
                </TitleBoxDouColorListText>
            </AssignmentEntryLink>
            {totalUnits === unitsCompleted && (
                <FontAwesomeIcon icon={faCheckCircle} color="green" style={{ marginLeft: ".33rem" }}></FontAwesomeIcon>
            )}
            <AssignmentTextLine>
                <strong>Total Units:</strong> {totalUnits} - <strong>Completed Units:</strong> {unitsCompleted} -{" "}
                <strong>Subject: </strong> {subjectName}
            </AssignmentTextLine>
            <AssignmentTextLine className="time-line">
                Due: {realMonth}/{date.getDate()}/{date.getFullYear()}, {time}
            </AssignmentTextLine>
        </TitleBoxDouColorListEntry>
    );
};

export default HomeworkEntry;
