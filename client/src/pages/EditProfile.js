import React, { useState, useEffect, useContext } from "react";
import EditProfileForm from "../components/profile/EditProfileForm";

import { ProfileContext } from "../context/ProfileContext";
import { makeToast } from "../utils/helpers";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { TitleBox, TitleBoxContainer, TitleBoxTitle, TitleBoxTitleContainer, Container } from "../elements/components";

const EditProfile = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [grade, setGrade] = useState("");
    const [role, setRole] = useState("");
    const history = useHistory();
    const profile = useContext(ProfileContext);

    const profileHandler = async (e) => {
        e.preventDefault();
        try {
            const res = await axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/editprofileinfo`,
                withCredentials: true,
                data: {
                    firstName,
                    lastName,
                    grade,
                    role,
                },
            });
            if (res.status === 200) {
                profile.update();
                makeToast("Profile updated. 👍");
                history.push("/profile");
            }
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        setFirstName(profile.firstName);
        setLastName(profile.lastName);
        setGrade(profile.grade);
        setRole(profile.role);
    }, [profile]);

    return (
        <Container>
            <TitleBox marginTop="2" maxWidth="50rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Edit Profile</TitleBoxTitle>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <EditProfileForm
                        firstName={firstName}
                        setFirstName={setFirstName}
                        lastName={lastName}
                        setLastName={setLastName}
                        grade={grade}
                        setGrade={setGrade}
                        role={role}
                        profileHandler={profileHandler}
                    ></EditProfileForm>
                </TitleBoxContainer>
            </TitleBox>
        </Container>
    );
};

export default EditProfile;
