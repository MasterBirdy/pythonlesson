import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { makeToast, makeWarningToast, makeInfoToast, makeErrorToast } from "../utils/helpers";
import TimeFormElement from "../components/classroom/TimeFormElement";
import Button from "../components/ui/Button";
import axios from "axios";
import { useParams, useHistory } from "react-router-dom";
import ErrorComponent from "../components/ui/ErrorComponent";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    IconsContainer,
    Container,
    Form,
} from "../elements/components/";

const EditAssignment = () => {
    const [displayAssignment, setDisplayAssignment] = useState("");
    const [dueDate, setDueDate] = useState("");
    const [dueTime, setDueTime] = useState("");
    const [loading, setLoading] = useState(true);
    const [invalid, setInvalid] = useState(false);
    const [invalidMessage, setInvalidMessage] = useState("");
    const { id } = useParams();
    const history = useHistory();

    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/getassignment/${id}`,
            withCredentials: true,
        })
            .then((res) => {
                const date = new Date(res.data.assignment.due_date);
                const realMonth = parseInt(date.getMonth()) + 1;
                setDisplayAssignment(res.data.assignment.name);
                setDueDate(
                    `${date.getFullYear()}-${realMonth < 10 ? "0" + realMonth : realMonth}-${
                        date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
                    }`
                );
                setDueTime(
                    `${date.getHours() < 10 ? "0" + date.getHours() : date.getHours()}:${
                        date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()
                    }`
                );
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 401 || err.response.status === 403) {
                        setInvalid(true);
                        setInvalidMessage("You are unauthorized to view this page.");
                    }
                } else {
                    if (err.response.status === 500) {
                        makeErrorToast(err.response.data.err);
                    }
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    const validate = () => {
        return !!dueDate && !!dueTime;
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!validate()) {
            makeWarningToast("Make sure that all fields are filled out in the Submit Assignment form!");
        } else {
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/editassignment/${id}`,
                data: {
                    dueDate: `${dueDate} ${dueTime}:00`,
                },
                withCredentials: true,
            })
                .then((res) => {
                    if (res.status === 200) {
                        makeToast("Assignment successfully editted.");
                        history.push("/classroom");
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };

    if (loading) {
        return <div className="loader"></div>;
    }

    if (invalid) {
        return (
            <div className="container">
                <ErrorComponent errorMessage={invalidMessage}></ErrorComponent>
            </div>
        );
    }

    return (
        <Container>
            <TitleBox maxWidth="55rem" marginTop="2">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Edit Assignment</TitleBoxTitle>
                    <IconsContainer>
                        <FontAwesomeIcon
                            icon={faQuestion}
                            onClick={() =>
                                makeInfoToast(`ℹ️ You can edit the due date and the due time of your assignment here.`)
                            }
                        ></FontAwesomeIcon>
                    </IconsContainer>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <Form>
                        <TimeFormElement
                            type="input"
                            label="Assignment Name"
                            inputValue={displayAssignment}
                            inputValueHandler={setDisplayAssignment}
                            disabled={true}
                        ></TimeFormElement>
                        <TimeFormElement
                            type="date"
                            label="Assignment Due Date"
                            inputValue={dueDate}
                            inputValueHandler={setDueDate}
                        ></TimeFormElement>
                        <TimeFormElement
                            type="time"
                            label="Assignment Due Time"
                            inputValue={dueTime}
                            inputValueHandler={setDueTime}
                        ></TimeFormElement>
                    </Form>
                    <div className="margin-top-small button-toolbar-helper">
                        <Button
                            aFunction={(e) => {
                                submitHandler(e);
                            }}
                        >
                            Submit
                        </Button>
                        <Button aFunction={() => history.push("/classroom")}>Back</Button>
                    </div>
                </TitleBoxContainer>
            </TitleBox>
        </Container>
    );
};

export default EditAssignment;
