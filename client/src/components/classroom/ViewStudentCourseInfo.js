import React from "react";
import { Link } from "react-router-dom";
import ViewStudentCoursesTrack from "./ViewStudentsCoursesTrack";
import { below } from "../../elements/utilities";
import styled from "styled-components";

const ViewStudentCourseInfo = ({
    title,
    totalUnits,
    completedUnits,

    subjectArea,
    coursesTrack,
    lessonID,
    standard,
}) => {
    return (
        <FilteredUnit>
            <div>
                <Link to={`/turtlelesson/${lessonID}`}>
                    <FilteredUnitTtile>{title}</FilteredUnitTtile>
                </Link>
                <FilteredUnitText>
                    <FilteredUnitSubItem>
                        <span className="title">Total Units:</span> {totalUnits}
                    </FilteredUnitSubItem>
                    <FilteredUnitSubItem>
                        <span className="title">Completed Units:</span> {completedUnits}
                    </FilteredUnitSubItem>
                </FilteredUnitText>
                <FilteredUnitText>
                    <FilteredUnitSubItem>
                        <span className="title">Subject:</span> {subjectArea ? subjectArea : "N/A"}
                    </FilteredUnitSubItem>
                    <FilteredUnitSubItem>
                        <span className="title">Standard:</span> {standard ? standard : "N/A"}
                    </FilteredUnitSubItem>
                </FilteredUnitText>
            </div>
            <ViewStudentCoursesTrack courses={coursesTrack} lessonID={lessonID}></ViewStudentCoursesTrack>
        </FilteredUnit>
    );
};

export const FilteredUnit = styled.div`
    display: flex;
    justify-content: space-between;
    background-color: #f7f7f7;
    border: 1px solid rgb(215, 215, 215);
    border-radius: 5px;
    box-shadow: 0px 4px 10px -7px #aaaaaa;
    padding: 0.5rem 1rem;
    margin-bottom: 1.25rem;
`;

const FilteredUnitTtile = styled.h2`
    font-size: 1.6rem;
    margin-bottom: 0.2rem;
`;

export const FilteredUnitText = styled.p`
    font-size: 1.2rem;
    &:not(:last-child) {
        margin-bottom: 0.125rem;
    }
`;

const FilteredUnitSubItem = styled.span`
    color: rgba(0, 0, 0, 0.925);
    ${below.xs`
    display: block;
    `}

    .title {
        font-weight: 600;
    }

    &:not(:last-child) {
        margin-right: 1.5rem;
    }
`;
export default ViewStudentCourseInfo;
