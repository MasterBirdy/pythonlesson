import React from "react";

import avatar1male from "../images/avatar-male-01a-resized.png";
import avatar2male from "../images/avatar-male-02a-resized.png";
import avatar3male from "../images/avatar-male-03a-resized.png";
import avatar4male from "../images/avatar-male-04a-resized.png";
import avatar1female from "../images/avatar-female-01a-resized.png";
import avatar2female from "../images/avatar-female-02a-resized.png";
import avatar3female from "../images/avatar-female-03a-resized.png";
import avatar4female from "../images/avatar-female-04a-resized.png";
import bat from "../images/profile-bat-resized.png";
import fish from "../images/profile-fish-resized.png";
import flamingo from "../images/profile-flamingo-resized.png";
import goat from "../images/profile-goat-resized.png";
import owl from "../images/profile-owl-resized.png";
import panda from "../images/profile-panda-resized.png";
import raccoon from "../images/profile-raccoon-resized.png";
import sloth from "../images/profile-sloth-resized.png";
import tiger from "../images/profile-tiger-resized.png";
import turtle from "../images/profile-turtle-resized.png";

const AvatarChooser = ({ imageUrl, avatarHandler, currentAvatar, role }) => {
    return (
        <div className="avatar-chooser">
            <div className="avatar-chooser-title-container">
                <p className="avatar-chooser-title">Avatar Chooser</p>
            </div>
            <div className="avatar-chooser-container">
                {role === "student" && (
                    <div className="avatar-grid">
                        <img
                            src={avatar1male}
                            onClick={() => avatarHandler(0)}
                            className={currentAvatar === 0 ? "selected" : ""}
                            alt="Male student avatar"
                        ></img>
                        <img
                            src={avatar2male}
                            className={currentAvatar === 1 ? "selected" : ""}
                            onClick={() => avatarHandler(1)}
                            alt="Male student avatar"
                        ></img>
                        <img
                            src={avatar3male}
                            className={currentAvatar === 2 ? "selected" : ""}
                            onClick={() => avatarHandler(2)}
                            alt="Male student avatar"
                        ></img>
                        <img
                            src={avatar4male}
                            className={currentAvatar === 3 ? "selected" : ""}
                            onClick={() => avatarHandler(3)}
                            alt="Male student avatar"
                        ></img>
                        <img
                            src={avatar1female}
                            className={currentAvatar === 4 ? "selected" : ""}
                            onClick={() => avatarHandler(4)}
                            alt="Female student avatar"
                        ></img>
                        <img
                            src={avatar2female}
                            className={currentAvatar === 5 ? "selected" : ""}
                            onClick={() => avatarHandler(5)}
                            alt="Female student avatar"
                        ></img>
                        <img
                            src={avatar3female}
                            className={currentAvatar === 6 ? "selected" : ""}
                            onClick={() => avatarHandler(6)}
                            alt="Female student avatar"
                        ></img>
                        <img
                            src={avatar4female}
                            className={currentAvatar === 7 ? "selected" : ""}
                            onClick={() => avatarHandler(7)}
                            alt="Female student avatar"
                        ></img>
                    </div>
                )}
                {role === "teacher" && (
                    <div className="avatar-grid">
                        <img
                            src={bat}
                            onClick={() => avatarHandler(8)}
                            className={currentAvatar === 8 ? "selected" : ""}
                            alt="Bat avatar"
                        ></img>
                        <img
                            src={fish}
                            className={currentAvatar === 9 ? "selected" : ""}
                            onClick={() => avatarHandler(9)}
                            alt="Fish avatar"
                        ></img>
                        <img
                            src={flamingo}
                            className={currentAvatar === 10 ? "selected" : ""}
                            onClick={() => avatarHandler(10)}
                            alt="Flamingo avatar"
                        ></img>
                        <img
                            src={goat}
                            className={currentAvatar === 11 ? "selected" : ""}
                            onClick={() => avatarHandler(11)}
                            alt="Goat avatar"
                        ></img>
                        <img
                            src={owl}
                            className={currentAvatar === 12 ? "selected" : ""}
                            onClick={() => avatarHandler(12)}
                            alt="Owl avatar"
                        ></img>
                        <img
                            src={panda}
                            className={currentAvatar === 13 ? "selected" : ""}
                            onClick={() => avatarHandler(13)}
                            alt="Panda avatar"
                        ></img>
                        <img
                            src={raccoon}
                            className={currentAvatar === 14 ? "selected" : ""}
                            onClick={() => avatarHandler(14)}
                            alt="Raccoon avatar"
                        ></img>
                        <img
                            src={sloth}
                            className={currentAvatar === 15 ? "selected" : ""}
                            onClick={() => avatarHandler(15)}
                            alt="Sloth avatar"
                        ></img>
                        <img
                            src={tiger}
                            className={currentAvatar === 16 ? "selected" : ""}
                            onClick={() => avatarHandler(16)}
                            alt="Tiger avatar"
                        ></img>
                        <img
                            src={turtle}
                            className={currentAvatar === 17 ? "selected" : ""}
                            onClick={() => avatarHandler(17)}
                            alt="Turtle avatar"
                        ></img>
                    </div>
                )}
            </div>
            {(role === "student" || role === "teacher") && (
                <div className="avatar-preview">
                    <div className="avatar-preview-title-container">
                        <p className="avatar-preview-title">Your Avatar</p>
                    </div>
                    <div className="avatar-preview-container">
                        <img src={imageUrl} alt="Avatar" className="image"></img>
                    </div>
                </div>
            )}
        </div>
    );
};

export default AvatarChooser;
