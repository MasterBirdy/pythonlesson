import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { SearchResultsEntry, SearchResultsCell, SearchResultsText } from "../search/SearchResults";
const SearchResultRow = ({
    id,
    name,
    subjectAreaName,
    standardName,
    totalLessons,
    completedLessons,
    addHandler,
    grades,
    location,
    selectedAddID,
}) => {
    return (
        <SearchResultsEntry>
            <SearchResultsCell col="2">
                <SearchResultsText>{standardName ? standardName : "N/A"}</SearchResultsText>
            </SearchResultsCell>
            <SearchResultsCell col="4">
                <Link to={`/turtlelesson/${id}`}>
                    <SearchResultsText className="no-shorten">{name}</SearchResultsText>
                </Link>
            </SearchResultsCell>
            <SearchResultsCell col="2">
                <SearchResultsText>{subjectAreaName ? subjectAreaName : "None"}</SearchResultsText>
            </SearchResultsCell>

            {location === "/addassignment" && (
                <SearchResultsCell col="2">
                    <SearchResultsText>
                        {grades.map((grade, index) => {
                            if (index < grades.length - 1) {
                                return `${grade}, `;
                            } else {
                                return `${grade}`;
                            }
                        })}
                    </SearchResultsText>
                </SearchResultsCell>
            )}

            {location === "/addassignment" && (
                <SearchResultsCell onClick={addHandler ? () => addHandler(id, name) : null}>
                    <FontAwesomeIcon
                        icon={id === selectedAddID ? faCheckCircle : faPlusCircle}
                        color={id === selectedAddID ? "green" : "grey"}
                    ></FontAwesomeIcon>
                </SearchResultsCell>
            )}

            {location === "/searchresults" && (
                <SearchResultsCell col="2">
                    <SearchResultsText
                        className={
                            totalLessons === completedLessons
                                ? "completed-dark"
                                : completedLessons !== 0
                                ? "inprogress"
                                : "notstarted"
                        }
                    >
                        {totalLessons === completedLessons
                            ? "Completed"
                            : completedLessons !== 0
                            ? "In Progress"
                            : "Not Started"}
                    </SearchResultsText>
                </SearchResultsCell>
            )}

            {location === "/searchresults" && (
                <SearchResultsCell>
                    <SearchResultsText className={totalLessons === completedLessons ? "completed-dark strong" : ""}>
                        {completedLessons} / {totalLessons}
                    </SearchResultsText>
                </SearchResultsCell>
            )}
        </SearchResultsEntry>
    );
};

export default SearchResultRow;
