module.exports = {
    development: {
        client: "mysql2",
        connection: {
            host: "localhost",
            user: "root",
            password: "",
            database: "hapimonkey",
        },
        migrations: {
            directory: __dirname + "/db/migrations",
        },
        seeds: {
            directory: __dirname + "db/seeds",
        },
    },
    production: {
        client: "mysql2",
        connection: {
            host: "localhost",
            user: "admin",
            password: "r]#9RS>m",
            database: "hapimonkey",
        },
        migrations: {
            directory: __dirname + "/db/migrations",
        },
        seeds: {
            directory: __dirname + "db/seeds",
        },
    },
};
