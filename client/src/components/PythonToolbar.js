import React from "react";
// import { createUseStyles } from "react-jss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faQuestionCircle, faUmbrellaBeach, faPalette } from "@fortawesome/free-solid-svg-icons";
import useMobile from "../hooks/useMobile";

const PythonToolbar = ({
    loading,
    functionHandler,
    handlePageNumber,
    totalPages,
    pageNumber,
    handleStop,
    submitState,
    submitToServer,
    unitsCompleted,
    isSandbox,
    setIsSandbox,
    autoOpen,
    setAutoOpen,
    lockedPage,
}) => {
    const [isMobile] = useMobile();
    const numberMap = [];
    for (let i = 0; i < totalPages; i++) {
        numberMap.push(i + 1);
    }

    return (
        <div className="toolbar">
            <div className="toolbar-center">
                <div className="numbers">
                    {numberMap.map((num) => (
                        <button
                            className={[
                                "number-button",
                                unitsCompleted[num] ? "completed" : "",
                                pageNumber === num ? "active-button" : "",
                            ].join(" ")}
                            key={num}
                            onClick={() => handlePageNumber(num)}
                        >
                            {num}
                        </button>
                    ))}
                </div>
                <button
                    onClick={() => setIsSandbox((prevState) => !prevState)}
                    className={["run-code-button", "turns-grey", isSandbox ? "black" : ""].join(" ")}
                >
                    <FontAwesomeIcon
                        icon={faUmbrellaBeach}
                        color="grey"
                        className={isSandbox ? "yellow" : ""}
                    ></FontAwesomeIcon>
                    {!isMobile && "Sandbox"}
                </button>
                <button
                    onClick={() => setAutoOpen((prevState) => !prevState)}
                    className={["run-code-button", "turns-grey", autoOpen ? "black" : ""].join(" ")}
                >
                    <FontAwesomeIcon
                        icon={faPalette}
                        color="grey"
                        className={autoOpen ? "green" : ""}
                    ></FontAwesomeIcon>
                    {!isMobile && "Canvas"}
                </button>
            </div>
            <div className="flexdiv">
                {unitsCompleted[pageNumber] && <FontAwesomeIcon icon={faCheckCircle} color="green"></FontAwesomeIcon>}
                {loading && (
                    <>
                        <div className="spinner-loading"></div>
                        <button onClick={handleStop} className="run-code-button">
                            Stop
                        </button>
                    </>
                )}

                {submitState && !unitsCompleted[lockedPage.current] && (
                    <>
                        <FontAwesomeIcon icon={faQuestionCircle} color="#ffae42"></FontAwesomeIcon>
                        <button onClick={submitToServer} className="run-code-button">
                            {!isMobile ? "Submit Code" : "Submit"}
                        </button>
                    </>
                )}
                <button onClick={functionHandler} className="run-code-button">
                    {!isMobile ? "Run Code" : "Run"}
                </button>
            </div>
        </div>
    );
};

export default PythonToolbar;
