import styled from "styled-components";
import { below } from "../utilities";

export const Container = styled.div`
    max-width: 900px;
    margin: 0 auto;
    padding: 0 15px;

    .blocked {
        margin-top: 1rem;
        text-align: center;
        p {
            font-size: 1.8rem;
        }
    }

    ${below.s`
        width: 90%;
        padding: 0 0 1rem 0;
    `}
`;
