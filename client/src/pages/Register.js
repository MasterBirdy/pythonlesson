import React, { useState, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import RegisterForm from "../components/register/RegisterForm";
import AvatarChooser from "../components/AvatarChooser";
import axios from "axios";

import { AuthContext } from "../context/AuthContext";
import { Redirect } from "react-router-dom";
import { makeToast, passwordRegex } from "../utils/helpers";

import {
    Container,
    TitleBox,
    TitleBoxContainer,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    Errors,
} from "../elements/components";
import styled from "styled-components";
import { below } from "../elements/utilities";

//eslint-disable-next-line
const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

const Register = (props) => {
    const auth = useContext(AuthContext);
    const history = useHistory();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [currentAvatar, setCurrentAvatar] = useState(0);
    const [passcode, setPasscode] = useState("");
    const [role, setRole] = useState("");
    const [currentSchool, setCurrentSchool] = useState("");
    const [schools, setSchools] = useState([]);
    const [grade, setGrade] = useState("");
    const [errorMessages, setErrorMessages] = useState([]);
    const [errors, setErrors] = useState({
        firstName: false,
        lastName: false,
        email: false,
        password: false,
        passcode: false,
        role: false,
        currentSchool: false,
        grade: false,
    });
    const avatars = [
        "avatar-male-01.png",
        "avatar-male-02.png",
        "avatar-male-03.png",
        "avatar-male-04.png",
        "avatar-female-01.png",
        "avatar-female-02.png",
        "avatar-female-03.png",
        "avatar-female-04.png",
        "bat-full-resized.png",
        "fish-full-resized.png",
        "flamingo-full-resized.png",
        "goat-full-resized.png",
        "owl-full-resized.png",
        "panda-full-resized.png",
        "raccoon-full-resized.png",
        "sloth-full-resized.png",
        "tiger-full-resized.png",
        "turtle-full-resized.png",
    ];

    useEffect(() => {
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/schools`,
        })
            .then((res) => {
                setSchools(res.data.schools);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const registerHandler = async (e) => {
        e.preventDefault();
        if (validate()) {
            try {
                const res = await axios({
                    method: "POST",
                    url: `${
                        process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                    }/api/register`,
                    data: {
                        first_name: firstName,
                        last_name: lastName,
                        email: email.toLowerCase(),
                        password,
                        avatar: currentAvatar,
                        role,
                        passcode,
                        school: currentSchool,
                        grade,
                    },
                });
                if (res.status === 200) {
                    makeToast("Welcome! Please login into your account. 🙂");
                    history.push("/login");
                }
            } catch (err) {
                if (err.response.data) {
                    setErrorMessages((prevState) => [...prevState, err.response.data.err]);
                }
            }
        }
    };

    const validate = () => {
        const errorArray = [];
        const errorObject = {
            firstName: false,
            lastName: false,
            email: false,
            password: false,
            passcode: false,
            role: false,
            currentSchool: false,
            grade: false,
        };
        if (firstName.length < 2) {
            errorArray.push("First name must be longer than 2 characters.");
            errorObject.firstName = true;
        }
        if (lastName.length < 2) {
            errorArray.push("Last name must be longer than 2 characters.");
            errorObject.lastName = true;
        }
        if (!emailRegex.test(email)) {
            errorArray.push("You must put in a valid e-mail address!");
            errorObject.email = true;
        }
        if (!passwordRegex.test(password)) {
            errorArray.push("Passwords must be at least 4 characters and have 1 letter and number.");
            errorObject.password = true;
        }
        if (password !== confirmPassword) {
            errorArray.push("Password and confirm password must match!");
            errorObject.password = true;
        }
        if (currentSchool === "") {
            errorArray.push("You must select a school!");
            errorObject.school = true;
        }
        if (!(role === "student" || role === "teacher")) {
            errorArray.push("Please select a role!");
            errorObject.role = true;
        }
        if (role === "student" && grade === "") {
            errorArray.push("You must put in a grade as a student!");
            errorObject.grade = true;
        }
        setErrorMessages(errorArray);
        setErrors(errorObject);
        return errorArray.length === 0;
    };

    const avatarHandler = (number) => {
        setCurrentAvatar(number);
    };

    const roleHandler = (e) => {
        if (e.target.value === "student") {
            setCurrentAvatar(0);
        } else if (e.target.value === "teacher") {
            setCurrentAvatar(8);
        }
        setRole(e.target.value);
    };

    const gradeHandler = (e) => {
        setGrade(e.target.value);
    };

    const schoolHandler = (e) => {
        setCurrentSchool(e.target.value);
    };

    if (auth.loading) {
        return <div></div>;
    } else {
        if (auth.isAuthenticated()) {
            return <Redirect to="/profile"></Redirect>;
        } else {
            return (
                <RegisterContainer>
                    <TitleBox>
                        <TitleBoxTitleContainer>
                            <TitleBoxTitle>Register</TitleBoxTitle>
                        </TitleBoxTitleContainer>
                        <TitleBoxContainer>
                            <RegisterForm
                                firstName={firstName}
                                setFirstName={setFirstName}
                                lastName={lastName}
                                setLastName={setLastName}
                                email={email}
                                setEmail={setEmail}
                                password={password}
                                setPassword={setPassword}
                                registerHandler={registerHandler}
                                confirmPassword={confirmPassword}
                                setConfirmPassword={setConfirmPassword}
                                role={role}
                                roleHandler={roleHandler}
                                passcode={passcode}
                                setPasscode={setPasscode}
                                currentSchool={currentSchool}
                                schools={schools}
                                schoolHandler={schoolHandler}
                                grade={grade}
                                gradeHandler={gradeHandler}
                                errors={errors}
                            ></RegisterForm>
                        </TitleBoxContainer>
                        {errorMessages.length > 0 && (
                            <Errors>
                                <h2>Errors</h2>
                                <ul>
                                    {errorMessages.map((error) => (
                                        <li>{error}</li>
                                    ))}
                                </ul>
                            </Errors>
                        )}
                    </TitleBox>
                    <div>
                        <AvatarChooser
                            avatarHandler={avatarHandler}
                            imageUrl={"https://images-masterbirdy.s3-us-west-1.amazonaws.com/" + avatars[currentAvatar]}
                            currentAvatar={currentAvatar}
                            role={role}
                        ></AvatarChooser>
                    </div>
                </RegisterContainer>
            );
        }
    }
};

export const RegisterContainer = styled(Container)`
    display: grid;
    margin-top: 2rem;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 1.5rem;

    ${below.s`
        grid-template-columns: 1fr;
    `}
`;

export default Register;
