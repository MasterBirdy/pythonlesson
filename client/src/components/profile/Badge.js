import React from "react";
import hardworkresized from "../../images/hard-work-resized.png";
import highperformer1resized from "../../images/high-performer-1-resized.png";
import highperformer2resized from "../../images/high-performer-2-resized.png";
import highperformer3resized from "../../images/high-performer-3-resized.png";
import quickthinkingresized from "../../images/quick-thinking-resized.png";
import pythonturtleresized from "../../images/python-turtle-resized.png";
import styled from "styled-components";
import { below } from "../../elements/utilities";

const badgeLoader = {
    5: hardworkresized,
    2: highperformer1resized,
    3: highperformer2resized,
    4: highperformer3resized,
    6: quickthinkingresized,
    1: pythonturtleresized,
};

const Badge = ({ name, id }) => {
    return (
        <BadgeFlex>
            <BadgeContainer>
                <BadgeImage src={badgeLoader[id]} alt={name}></BadgeImage>
                <BadgeText>{name}</BadgeText>
            </BadgeContainer>
        </BadgeFlex>
    );
};

const BadgeFlex = styled.div`
    margin: 0 auto;
`;

const BadgeContainer = styled.div`
    display: inline-block;
    max-width: 14.5rem;
    &:not(:last-child) {
        margin-right: 2rem;
    }
`;

const BadgeImage = styled.img`
    width: 100%;
`;

const BadgeText = styled.p`
    font-family: "PT Sans", sans-serif;
    font-weight: 600;
    text-align: center;
    font-size: 1.4rem;
    ${below.s`
    font-size: 1.3rem;
    `}
`;

export default Badge;
