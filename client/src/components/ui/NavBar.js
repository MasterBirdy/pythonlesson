import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";
import { ProfileContext } from "../../context/ProfileContext";
import NavBarButton from "./NavBarButton";
import LinkList from "./LinkList";
import HamburgerButton from "../ui/HamburgerButton";
import MobileMenu from "../ui/MobileMenu";
import useMobile from "../../hooks/useMobile";

const links = [
    { name: "Home", path: "/", color: "orange" },
    { name: "Browse", path: "/browse", color: "yellow" },
    { name: "Search", path: "/search", color: "blue" },
];

const profileLinks = [
    { name: "Profile", path: "/profile" },
    { name: "Edit Profile", path: "/editprofile" },
    { name: "Homework", path: "/assignments", auth: "student" },
    { name: "Classroom", path: "/classroom", auth: "teacher" },
];

const unauthedLinks = [
    { name: "Home", path: "/" },
    { name: "Register", path: "/register" },
    { name: "Login", path: "/login" },
];

const mobileLinks = [...links, ...profileLinks];

const NavBar = () => {
    const auth = useContext(AuthContext);
    const profile = useContext(ProfileContext);
    const [enteredProfile, setEnteredProfile] = useState(false);
    const [openMobileMenu, setOpenMobileMenu] = useState(false);
    const history = useHistory();

    const [isMobile] = useMobile();

    const openHandler = (e) => {
        if (isMobile) {
            setOpenMobileMenu(true);
        }
    };

    const closeHandler = (e) => {
        setOpenMobileMenu(false);
    };

    const filteredMobileLinks = mobileLinks.filter((link) => {
        if (link.auth) {
            return link.auth === profile.role;
        }
        return true;
    });

    return (
        <nav className="navbar">
            <ul className="links-list">
                <li className="list-item list-item-logo" onClick={() => history.push("/")}>
                    <p className="logo">Moving Mindz</p>
                </li>
                {links.map((link) => (
                    <li className="list-item" key={link.name}>
                        <NavBarButton aFunction={() => history.push(link.path)} color={link.color}>
                            <p>{link.name}</p>
                        </NavBarButton>
                    </li>
                ))}
            </ul>
            {auth.isAuthenticated() ? (
                <ul className="links-list">
                    <li className="list-item-right" key="auth-profile">
                        <NavBarButton
                            color="purple"
                            mouseEnter={() => {
                                setEnteredProfile(true);
                            }}
                            mouseLeave={() => setEnteredProfile(false)}
                        >
                            <p>Profile</p>

                            <LinkList isVisible={enteredProfile} links={profileLinks} role={profile.role}></LinkList>
                        </NavBarButton>
                    </li>
                    <li className="list-item-right" key="auth-logout">
                        <NavBarButton aFunction={auth.logout} color="pink">
                            <p>Logout</p>
                        </NavBarButton>
                    </li>
                    <HamburgerButton test={openHandler}></HamburgerButton>
                </ul>
            ) : (
                <ul className="links-list">
                    <li className="list-item-right" key="register">
                        <NavBarButton aFunction={() => history.push("/register")} color="purple">
                            <p>Register</p>
                        </NavBarButton>
                    </li>
                    <li className="list-item-right" key="login">
                        <NavBarButton aFunction={() => history.push("/login")} color="pink">
                            <p>Login</p>
                        </NavBarButton>
                    </li>
                    <HamburgerButton test={openHandler}></HamburgerButton>
                </ul>
            )}
            <MobileMenu
                authed={auth.isAuthenticated()}
                links={auth.isAuthenticated() ? filteredMobileLinks : unauthedLinks}
                isOpen={openMobileMenu}
                closeHandler={closeHandler}
                logout={auth.logout}
            ></MobileMenu>
        </nav>
    );
};

export default NavBar;
