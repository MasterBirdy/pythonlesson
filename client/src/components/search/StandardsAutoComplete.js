import React from "react";

const StandardsAutoComplete = ({
    standardInput,
    standards,
    autoCompleteRef,
    showAutoComplete,
    autoCompleteHandler,
    standardInputHandler,
}) => {
    const selectedStandards = standards.slice(0, 4);
    return (
        <div className="flex-line">
            <div className="input-item-grow relative" ref={autoCompleteRef}>
                <label htmlFor="standard">Standard</label>
                <input
                    type="text"
                    id="standard"
                    name="standard"
                    value={standardInput}
                    autoComplete="off"
                    onChange={(e) => standardInputHandler(e)}
                ></input>
                <div className={["auto-suggestions", showAutoComplete ? "" : "disabled"].join(" ")}>
                    {selectedStandards.map((standard) => {
                        return (
                            <div
                                className="auto-suggestion"
                                key={standard.id}
                                onClick={() => autoCompleteHandler(standard.name)}
                            >
                                {standard.name} - {standard.title}
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};

export default StandardsAutoComplete;
