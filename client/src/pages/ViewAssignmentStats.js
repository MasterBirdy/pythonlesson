import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import useMobile from "../hooks/useMobile";
import useSearchResults from "../hooks/useSearchResults";
import ViewAssignmentStatsResults from "../components/assignments/ViewAssignmentStatsResults";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { TitleBox, TitleBoxTitle, TitleBoxTitleContainer, TitleBoxContainer, Container } from "../elements/components/";

const sortBy = (a, b, label) => {
    let aChosen = a[label];
    let bChosen = b[label];
    if (!aChosen) {
        aChosen = "None";
    }
    if (!bChosen) {
        bChosen = "None";
    }
    if (aChosen < bChosen) return -1;
    if (aChosen > bChosen) return 1;
    return 0;
};

const sortGrade = (a, b) => {
    const aGrade = a.grades ? a.grades[0] : Infinity;
    const bGrade = b.grades ? b.grades[0] : Infinity;
    if (aGrade < bGrade) return -1;
    if (aGrade > bGrade) return 1;
    return 0;
};

const progressSortMethod = (a, b) => {
    const aString = a.total_units ? (a.total_units === b.unitsCompleted ? "Completed" : "In Progress") : "Not Started";
    const bString = b.total_units ? (b.total_units === b.unitsCompleted ? "Completed" : "In Progress") : "Not Started";
    if (aString < bString) return -1;
    if (aString > bString) return 1;
    return 0;
};

const unitsSortMethod = (a, b) => {
    const aUnits = a.total_units ? 1 : 0;
    const bUnits = b.total_units ? 1 : 0;
    if (aUnits < bUnits) return -1;
    if (aUnits > bUnits) return 1;
    return 0;
};

const ViewAssignmentStats = () => {
    const { id } = useParams();
    const [loading, setLoading] = useState(true);
    const [students, setStudents] = useState([]);
    const [isMobile] = useMobile();
    const [firstNameSort, setFirstNameSort] = useState(false);
    const [lastNameSort, setLastNameSort] = useState(true);
    const [gradeSort, setGradeSort] = useState(false);
    const [progressSort, setProgressSort] = useState(false);
    const [unitsSort, setUnitsSort] = useState(false);
    const [reverse, setReverse] = useState(false);
    useEffect(() => {
        const promises = [
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getstudentsofschool/`,
                withCredentials: true,
            }),
            axios({
                method: "GET",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/getassignmentcompletion/${id}`,
                withCredentials: true,
            }),
        ];

        Promise.all(promises)
            .then((res) => {
                if (res[0].status === 200 && res[1].status === 200) {
                    const newStudents = res[0].data.teacherStudents.map((student) => {
                        if (res[1].data.students[student.id]) {
                            return { ...student, ...res[1].data.students[student.id] };
                        } else {
                            return { ...student };
                        }
                    });
                    setStudents(newStudents);
                }
            })
            .catch((err) => {
                console.log(err);
            })
            .finally(() => {
                setLoading(false);
            });
    }, []);

    let sortedStudents = students;
    console.log(students);
    if (firstNameSort) {
        sortedStudents = students
            .concat()
            .sort((a, b) => sortBy(a, b, "last_name"))
            .sort(reverse ? (a, b) => sortBy(a, b, "first_name") * -1 : (a, b) => sortBy(a, b, "first_name"));
    } else if (lastNameSort) {
        sortedStudents = students
            .concat()
            .sort((a, b) => sortBy(a, b, "first_name"))
            .sort(reverse ? (a, b) => sortBy(a, b, "last_name") * -1 : (a, b) => sortBy(a, b, "last_name"));
    } else if (unitsSort) {
        sortedStudents = students
            .concat()
            .sort((a, b) => sortBy(a, b, "first_name"))
            .sort((a, b) => sortBy(a, b, "last_name"))
            .sort(reverse ? (a, b) => unitsSortMethod(a, b) * -1 : (a, b) => unitsSortMethod(a, b));
    } else if (progressSort) {
        sortedStudents = students
            .concat()
            .sort((a, b) => sortBy(a, b, "first_name"))
            .sort((a, b) => sortBy(a, b, "last_name"))
            .sort(reverse ? (a, b) => progressSortMethod(a, b) * -1 : (a, b) => progressSortMethod(a, b));
    } else if (gradeSort) {
        sortedStudents = students
            .concat()
            .sort((a, b) => sortBy(a, b, "first_name"))
            .sort((a, b) => sortBy(a, b, "last_name"))
            .sort(reverse ? (a, b) => sortGrade(a, b) * -1 : (a, b) => sortGrade(a, b));
    }

    if (loading) {
        return <div className="loader"></div>;
    }

    return (
        <Container>
            <TitleBox marginTop="2">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Assignment List</TitleBoxTitle>
                </TitleBoxTitleContainer>

                <ViewAssignmentStatsResults
                    students={sortedStudents}
                    isMobile={isMobile}
                    reverse={reverse}
                    firstNameSort={firstNameSort}
                    setFirstNameSort={setFirstNameSort}
                    lastNameSort={lastNameSort}
                    setLastNameSort={setLastNameSort}
                    gradeSort={gradeSort}
                    setGradeSort={setGradeSort}
                    progressSort={progressSort}
                    setProgressSort={setProgressSort}
                    unitsSort={unitsSort}
                    setUnitsSort={setUnitsSort}
                ></ViewAssignmentStatsResults>
            </TitleBox>
        </Container>
    );
};

export default ViewAssignmentStats;
