const knex = require("../database/db.js");
const jwt_decode = require("jwt-decode");

const gradeRegex = /^\d{1,2}$/;
const quotedStringsRegex = /"([^\\"]|\\")*"/g;

module.exports = [
    {
        method: "GET",
        path: "/api/search",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const subject_areas = await knex("subject_areas");

                if (!subject_areas.length) {
                    throw new Error("Subject areas could not be found!");
                }
                const allSubjectAreas = {};

                for (const subjectArea of subject_areas) {
                    allSubjectAreas[subjectArea.id] = {
                        ...subjectArea,
                    };
                }

                const standards = await knex("standards");

                const standardsObject = {
                    3: [],
                    4: [],
                    5: [],
                    6: [],
                    7: [],
                    8: [],
                };

                standards.forEach((standard) => {
                    if (standardsObject[standard.name.slice(0, 1)]) {
                        standardsObject[standard.name.slice(0, 1)].push(standard);
                    }
                });

                return h.response({ subjectAreas: allSubjectAreas, standards: standardsObject }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/searchresults",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const results = await knex("lessons")
                    .select(
                        "lessons.id",
                        "lessons.name",
                        "subject_areas.name AS subjectAreaName",
                        "subject_areas.id AS subjectAreaID",
                        "standards.id AS standardID",
                        "standards.name AS standardName"
                    )
                    .count("* AS totalLessons")

                    .modify((queryBuilder) => {
                        if (req.query.grade) {
                            if (gradeRegex.test(req.query.grade)) {
                                queryBuilder
                                    .innerJoin("lessons_grades", "lessons.id", "=", "lessons_grades.lesson_id")
                                    .where("grade_id", "=", req.query.grade);
                            } else {
                                throw new Error("Grade format incorrect!");
                            }
                        }
                    })
                    .modify((queryBuilder) => {
                        if (req.query.subjectareas) {
                            const subjectsArray = req.query.subjectareas.split(",");
                            queryBuilder.join(
                                "lessons_subject_areas",
                                "lessons.id",
                                "=",
                                "lessons_subject_areas.lesson_id"
                            );
                            queryBuilder.where(function () {
                                subjectsArray.forEach((subject, index) => {
                                    if (index === 0) {
                                        this.where("subject_area_id", "=", subject);
                                    } else {
                                        this.orWhere("subject_area_id", "=", subject);
                                    }
                                });
                            });
                        } else {
                            queryBuilder.leftJoin(
                                "lessons_subject_areas",
                                "lessons.id",
                                "lessons_subject_areas.lesson_id"
                            );
                        }
                        queryBuilder.leftJoin(
                            "subject_areas",
                            "lessons_subject_areas.subject_area_id",
                            "subject_areas.id"
                        );
                        queryBuilder.leftJoin("lessons_standards", "lessons.id", "lessons_standards.lesson_id");
                        queryBuilder.leftJoin("standards", "standards.id", "lessons_standards.standard_id");
                    })
                    .modify((queryBuilder) => {
                        if (req.query.standard) {
                            queryBuilder.where("standards.name", "like", `%${req.query.standard}%`);
                        }
                    })
                    .leftJoin("units", function () {
                        this.on("lessons.id", "=", "units.lesson_id");
                    })
                    .groupBy("lessons.id")
                    .modify((queryBuilder) => {
                        if (req.query.name) {
                            let startingKeywords = req.query.name;
                            const quotedKeywords = startingKeywords.match(quotedStringsRegex);
                            let allKeywords = [];
                            if (quotedKeywords) {
                                quotedKeywords.forEach((keyword) => {
                                    allKeywords.push(keyword.replace(/"/g, ""));
                                    startingKeywords = startingKeywords.replace(keyword, "");
                                });
                            }
                            let startingArray = startingKeywords.split(" ");
                            startingArray.forEach((keyword) => {
                                if (keyword) {
                                    allKeywords.push(keyword);
                                }
                            });
                            allKeywords.forEach((keyword, index) => {
                                if (index === 0) {
                                    queryBuilder.having("lessons.name", "like", `%${keyword}%`);
                                } else {
                                    queryBuilder.orHaving("lessons.name", "like", `%${keyword}%`);
                                }
                            });
                        }
                    })
                    .modify((queryBuilder) => {
                        if (req.query.subjectareas) {
                            const subjectsArray = req.query.subjectareas.split(",");
                            queryBuilder.having(function () {
                                subjectsArray.forEach((subject, index) => {
                                    if (index === 0) {
                                        this.having("subjectAreaID", "=", subject);
                                    } else {
                                        this.orHaving("subjectAreaID", "=", subject);
                                    }
                                });
                            });
                        }
                    });

                if (results.length) {
                    const lessonObject = results.reduce((acc, cur) => {
                        acc[cur.id] = 0;
                        return acc;
                    }, {});

                    const gradeObject = results.reduce((acc, cur) => {
                        acc[cur.id] = [];
                        return acc;
                    }, {});

                    const allUserProgress = await knex("users_units")
                        .select("unit_id", "user_id", "lesson_id")
                        .count("* as completedLessons")
                        .groupBy("lesson_id")
                        .where("user_id", "=", decoded.id)
                        .modify((queryBuilder) => {
                            queryBuilder.where(function () {
                                Object.keys(lessonObject).forEach((key, index) => {
                                    if (index === 0) {
                                        this.where("lesson_id", "=", key);
                                    } else {
                                        this.orWhere("lesson_id", "=", key);
                                    }
                                });
                            });
                        });

                    allUserProgress.forEach((lesson) => {
                        lessonObject[lesson.lesson_id] = lesson.completedLessons;
                    });

                    const allGrades = await knex("lessons_grades")
                        .select("lesson_id", "grade_id")
                        .modify((queryBuilder) => {
                            queryBuilder.where(function () {
                                Object.keys(gradeObject).forEach((key, index) => {
                                    if (index === 0) {
                                        this.where("lesson_id", "=", key);
                                    } else {
                                        this.orWhere("lesson_id", "=", key);
                                    }
                                });
                            });
                        });

                    allGrades.forEach((lesson) => {
                        gradeObject[lesson.lesson_id].push(lesson.grade_id);
                    });

                    results.forEach((result) => {
                        result.completedLessons = lessonObject[result.id];
                        result.grades = gradeObject[result.id];
                    });
                }
                return h.response({ results }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/search",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const results = await knex("lessons")
                    .modify((queryBuilder) => {
                        if (req.payload.name) {
                            queryBuilder.where("name", "like", `%${req.payload.name}%`);
                        }
                    })
                    .modify((queryBuilder) => {
                        if (req.payload.grades) {
                            queryBuilder.innerJoin("lessons_grades", "lessons.id", "=", "lessons_grades.lesson_id");
                        }
                    })
                    .modify((queryBuilder) => {
                        if (req.payload.grades) {
                            req.payload.grades.forEach((grade, index) => {
                                if (index === 0) {
                                    queryBuilder.where("grade_id", "=", grade);
                                } else {
                                    queryBuilder.orWhere("grade_id", "=", grade);
                                }
                            });
                        }
                    })
                    .select("id", "name", "exp")
                    .distinct("id");
                return h.response({ results }).code(200);
            } catch (err) {
                return h.response({ err: err.message }).code(500);
            }
        },
    },
];
