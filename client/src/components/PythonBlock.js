import React from "react";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/duotone-light.css";
import "codemirror/theme/base16-dark.css";
import "codemirror/theme/dracula.css";
import "codemirror/theme/lucario.css";
import "codemirror/addon/hint/show-hint.js";
import "codemirror/mode/python/python.js";
import { createUseStyles } from "react-jss";

import { Controlled as CodeMirror } from "react-codemirror2";

const PythonBlock = ({ value, setValue, options, grow, consoleRef }) => {
    const useStyles = createUseStyles({
        component: {
            flexGrow: grow ? 1 : 0,
        },
    });
    const styles = useStyles();

    return (
        <CodeMirror
            className={styles.component}
            value={value}
            options={options}
            onBeforeChange={(editor, data, value) => {
                setValue(value);
            }}
            ref={consoleRef}
        />
    );
};

export default PythonBlock;
