const knex = require("../database/db.js");
const jwt_decode = require("jwt-decode");
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const moment = require("moment");
const passwordRegex = /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}/;
const bcrypt = require("bcrypt");

require("dotenv").config();

const getProfileInfo = async (id) => {
    const users = await knex
        .select("first_name", "last_name", "email", "grade", "xp", "avatar", "student_at", "teacher_at")
        .from("users")
        .where("id", id)
        .limit(1);
    if (users.length <= 0) {
        throw new Error("Could not find specified user!");
    }
    const user = users[0];
    let role = "";
    let school = "";
    if (user.student_at) {
        school = await knex.select("name").from("schools").where("id", user.student_at).limit(1);
        role = "student";
    } else if (user.teacher_at) {
        school = await knex.select("name").from("schools").where("id", user.teacher_at).limit(1);
        role = "teacher";
    } else {
        throw new Error("Error with role!");
    }
    if (school.length <= 0) {
        throw new Error("No school found!");
    }
    const badges = await knex("users_badges")
        .select("badge_id", "badges.name")
        .join("badges", function () {
            this.on("badges.id", "=", "users_badges.badge_id");
        })
        .where("user_id", id);
    const log = await knex
        .select("message")
        .from("event_log")
        .where("user_id", id)
        .orderBy("completed_on", "desc")
        .limit(10);
    const returnLog = log.map((entry) => {
        return entry.message;
    });
    const currentXP = user.xp;
    const level = await knex("levels").whereRaw(`${currentXP} - min_xp >= 0`).max({ level: "level", minXP: "min_xp" });
    if (level.length <= 0) {
        throw new Error("Error with XP");
    }
    const maxXP = await knex("levels").whereRaw(`${currentXP} >= min_xp`).max({ maxXP: "min_xp" });
    const minXP = await knex("levels").whereRaw(`${currentXP} < min_xp`).min({ minXP: "min_xp" });
    if (minXP.length <= 0) {
        throw new Error("Error with XP");
    }
    return {
        firstName: user.first_name,
        lastName: user.last_name,
        email: user.name,
        grade: user.grade,
        avatar: user.avatar,
        schoolName: school[0].name,
        role,
        message: "Success!",
        log: returnLog,
        currentXP,
        level,
        badges,
        maxXP: maxXP[0].maxXP,
        minXP: minXP[0].minXP,
    };
};

module.exports = [
    {
        method: "GET",
        path: "/api/profileinfo",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);
                const data = await getProfileInfo(decoded.id);

                return h.response(data).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "GET",
        path: "/api/profileinfo/{user_id}",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                const data = await getProfileInfo(req.params.user_id);

                return h.response(data).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/editprofileinfo",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            try {
                const decoded = jwt_decode(req.state.token);

                let data;
                if (req.payload.role === "student") {
                    data = {
                        first_name: req.payload.firstName,
                        last_name: req.payload.lastName,
                        grade: req.payload.grade,
                    };
                } else if (req.payload.role === "teacher") {
                    data = {
                        first_name: req.payload.firstName,
                        last_name: req.payload.lastName,
                    };
                } else {
                    throw new Error("Error with role!");
                }
                const check = await knex("users").where("id", decoded.id).update(data);
                if (check === 1) {
                    return h.response({ message: "success!" }).code(200);
                } else {
                    throw new Error("Error with updating profile.");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/forgotpassword",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                if (!req.payload.email) {
                    throw new Error("Must contain an email.");
                }
                const user = await knex("users").where("email", req.payload.email).limit(1);
                if (!user.length) {
                    throw new Error("No users found with that email");
                }
                const buf = crypto.randomBytes(20).toString("hex");

                const time = moment.utc().add(24, "hours").format("YYYY-MM-DD HH:mm:ss");
                const users = await knex("users")
                    .update({
                        password_reset_token: buf,
                        password_reset_date: time,
                    })
                    .where("email", req.payload.email)
                    .limit(1);
                const transporter = nodemailer.createTransport({
                    service: "gmail",
                    auth: {
                        user: process.env.EMAIL,
                        pass: process.env.PASSWORD,
                    },
                });
                const mailOptions = {
                    from: "support@movingmindz.com",
                    to: req.payload.email,
                    subject: "Password Request Email",
                    text: `Hello! You are receiving this because you (or someone else) have requested to reset the password for your account.
                    
Please click on the following link or paste this into your browser to complete the process within one hour of receiving it:

https://www.movingmindz.xyz/resetpassword/${buf}?email=${req.payload.email}

If you did not request this, please ignore this email and your password will remained unchanged.`,
                };

                let info = await transporter.sendMail(mailOptions);
                return h.response({ message: "Mail sent!" }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/confirmpasswordtoken",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                if (!req.payload.token) {
                    throw new Error("Invalid token.");
                }
                if (!req.payload.email) {
                    throw new Error("No email given!");
                }
                const users = await knex("users")
                    .where("email", req.payload.email)
                    .andWhere("password_reset_token", req.payload.token)
                    .limit(1);
                if (!users.length) {
                    throw new Error("No valid users found!");
                }
                const user = users[0];
                const time = moment(user.password_reset_date);
                if (!moment().isBefore(time)) {
                    throw new Error("Token has expired!");
                }
                if (!passwordRegex.test(req.payload.password)) {
                    throw new Error("Password does not meet password requirements.");
                }
                if (req.payload.password !== req.payload.confirmPassword) {
                    throw new Error("Password must be same as confirm password.");
                }
                const hash = await bcrypt.hash(req.payload.password, 10);

                const update = await knex("users")
                    .update({
                        password: hash,
                    })
                    .where("email", req.payload.email);
                return h.response({ message: "Password changed! Please log in with your new password." }).code(200);
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message, confirmedMatch: false }).code(500);
            }
        },
    },
];
