import React from "react";
import PythonBlock from "./PythonBlock";
import PythonToolbar from "./PythonToolbar";
import Console from "./Console";
import styled from "styled-components";

const PythonSection = ({
    code,
    setCode,
    response,
    setResponse,
    loading,
    sendCode,
    handleStop,
    handlePageNumber,
    pageNumber,
    totalPages,
    children,
    submitState,
    submitToServer,
    unitsCompleted,
    consoleRef,
    openConsoleToolbar,
    openConsoleHandler,
    consoleChevronRef,
    isError,
    setIsError,
    isSandbox,
    setIsSandbox,
    autoOpen,
    setAutoOpen,
    lockedPage,
}) => {
    return (
        <MainPythonHolder>
            <PythonBlock
                value={code}
                setValue={setCode}
                options={{
                    mode: "python",
                    theme: "lucario",
                    lineNumbers: true,
                }}
                grow={true}
            ></PythonBlock>
            <Console
                value={response}
                setValue={setResponse}
                consoleRef={consoleRef}
                openConsoleToolbar={openConsoleToolbar}
                openConsoleHandler={openConsoleHandler}
                consoleChevronRef={consoleChevronRef}
                setIsError={setIsError}
                isError={isError}
            ></Console>
            {children}
            <PythonToolbar
                loading={loading}
                functionHandler={sendCode}
                handlePageNumber={handlePageNumber}
                handleStop={handleStop}
                pageNumber={pageNumber}
                totalPages={totalPages}
                submitState={submitState}
                submitToServer={submitToServer}
                unitsCompleted={unitsCompleted}
                isSandbox={isSandbox}
                setIsSandbox={setIsSandbox}
                autoOpen={autoOpen}
                setAutoOpen={setAutoOpen}
                lockedPage={lockedPage}
            ></PythonToolbar>
        </MainPythonHolder>
    );
};

const MainPythonHolder = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    height: 100%;
`;

export default PythonSection;
