"use strict";

const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");
const bcrypt = require("bcrypt");
const knex = require("../database/db.js");
const Joi = require("@hapi/joi");

process.env.SECRET_KEY = "secret";

const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
const passwordRegex = /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}/;

const studentSchema = Joi.object({
    first_name: Joi.string().trim().alphanum().min(3).required(),
    last_name: Joi.string().trim().alphanum().min(3).required(),
    password: Joi.string().trim().regex(passwordRegex).required(),
    avatar: Joi.number().required(),
    email: Joi.string().trim().email().required(),
    student_at: Joi.string().trim().alphanum().required(),
    grade: Joi.number().required(),
});

const teacherSchema = Joi.object({
    first_name: Joi.string().trim().alphanum().min(3).required(),
    last_name: Joi.string().trim().alphanum().min(3).required(),
    password: Joi.string().trim().regex(passwordRegex).required(),
    avatar: Joi.number().required(),
    email: Joi.string().trim().email().required(),
    teacher_at: Joi.string().trim().alphanum().required(),
});

const loginSchema = Joi.object({
    email: Joi.string().trim().email().required(),
    password: Joi.string().trim().regex(passwordRegex).required(),
});

var cookie_options = {
    ttl: 365 * 24 * 60 * 60 * 1000,
    encoding: "none", // we already used JWT to encode
    isSecure: false, // warm & fuzzy feelings
    isHttpOnly: true, // prevent client alteration
    isSameSite: false,
    clearInvalid: false, // remove invalid cookies
    path: "/", // set the cookie for all routes
};

module.exports = [
    {
        method: "POST",
        path: "/api/register",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                let userData;
                if (req.payload.role === "student") {
                    userData = {
                        first_name: req.payload.first_name,
                        last_name: req.payload.last_name,
                        email: req.payload.email,
                        password: req.payload.password,
                        avatar: req.payload.avatar,
                        student_at: req.payload.school,
                        grade: req.payload.grade,
                    };
                    const validation = studentSchema.validate(userData, { escapeHtml: true });
                    if (validation.error) {
                        throw new Error("There was an error with validating the submitted data");
                    }
                } else if (req.payload.role === "teacher") {
                    userData = {
                        first_name: req.payload.first_name,
                        last_name: req.payload.last_name,
                        email: req.payload.email,
                        password: req.payload.password,
                        avatar: req.payload.avatar,
                        teacher_at: req.payload.school,
                    };
                    const validation = teacherSchema.validate(userData, { escapeHtml: true });
                    if (validation.error) {
                        throw new Error("There was an error with validating the submitted data");
                    }
                } else {
                    throw new Error("Role not found!");
                }

                const schoolValid = await knex.select().from("schools").where("id", req.payload.school).limit(1);
                if (schoolValid.length < 1) {
                    throw new Error("School does not exist!");
                }
                if (req.payload.role === "teacher") {
                    if (!req.payload.passcode) {
                        throw new Error("No passcode!");
                    }
                    const match = await bcrypt.compare(req.payload.passcode, schoolValid[0].passcode);
                    if (!match) {
                        throw new Error("Passcode incorrect!");
                    }
                }
                let user = await knex.select().from("users").where("email", req.payload.email).limit(1);
                if (user.length < 1) {
                    const hash = await bcrypt.hash(req.payload.password, 10);
                    userData.password = hash;
                    const finalUser = await knex("users").insert(userData);

                    if (finalUser.length > 0) {
                        return h.response({ message: "Success!" }).code(200);
                    } else {
                        throw new Error("Something happend with database insertion.");
                    }
                } else {
                    throw new Error("User already exists!");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/login",
        config: {
            auth: false,
        },
        handler: async (req, h) => {
            try {
                const validation = loginSchema.validate(
                    { email: req.payload.email, password: req.payload.password },
                    { escapeHtml: true }
                );
                if (validation.error) {
                    throw new Error("There was an error with either email or password validation.");
                }
                const users = await knex.select().from("users").where("email", req.payload.email).limit(1);
                if (users.length > 0) {
                    const match = await bcrypt.compare(req.payload.password, users[0].password);
                    if (match) {
                        const userObject = {
                            id: users[0].id,
                            first_name: users[0].first_name,
                            last_name: users[0].last_name,
                            email: users[0].email,
                            student_at: users[0].student_at,
                            teacher_at: users[0].teacher_at,
                            grade: users[0].grade,
                            avatar: users[0].avatar,
                            xp: users[0].xp,
                        };
                        const token = jwt.sign(userObject, process.env.JWT_SECRET, {
                            expiresIn: "7d",
                        });
                        const { exp } = jwt_decode(token);
                        return h
                            .response({
                                message: "Success!",
                                expiresAt: exp,
                                userInfo: userObject,
                            })
                            .code(200)
                            .state("token", token, cookie_options);
                    } else {
                        throw new Error("Incorrect password!");
                    }
                } else {
                    throw new Error("User doesn't exist!");
                }
            } catch (err) {
                console.log(err);
                return h.response({ err: err.message }).code(500);
            }
        },
    },
    {
        method: "POST",
        path: "/api/dashboard",
        config: {
            auth: "jwt",
        },
        handler: async (req, h) => {
            return h.response({ message: "Woo-hoo!" }).code(200);
        },
    },
    {
        method: "POST",
        path: "/api/logout",
        config: { auth: false },
        handler: async (req, h) => {
            return h.response("done").unstate("token", cookie_options);
        },
    },
];
