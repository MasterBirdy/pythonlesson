import React, { useState, useRef, useEffect, useContext } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import PythonSection from "../components/PythonSection";
import CanvasOverlay from "../components/CanvasOverlay";
import { v4 as uuidv4 } from "uuid";
import { AuthContext } from "../context/AuthContext";
import { ProfileContext } from "../context/ProfileContext";
import { makeToast } from "../utils/helpers";
import styled from "styled-components";
import { below, navBarSizes } from "../elements/utilities";

const defaultTimeLimit = window.Sk.execLimit;

const tempID = uuidv4();

const PythonLesson = () => {
    const regex = new RegExp(tempID, "g");
    const consoleRef = useRef(null);
    const canvasRef = useRef(null);
    const consoleChevronRef = useRef(null);
    const canvasChevronRef = useRef(null);
    const [code, setCode] = useState(``);
    const [submission, setSubmission] = useState(``);
    const [submitState, setSubmitState] = useState(false);
    const [response, setResponse] = useState("");
    const [isError, setIsError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [allPageData, setAllPageData] = useState("");
    const [unitsCompleted, setUnitsCompleted] = useState({});
    const [pageNumber, setPageNumber] = useState(1);
    const [openCanvasToolbar, setOpenCanvasToolbar] = useState(false);
    const [openConsoleToolbar, setOpenConsoleToolbar] = useState(true);
    const [isSandbox, setIsSandbox] = useState(false);
    const [autoOpen, setAutoOpen] = useState(true);
    const lockedPage = useRef(null);
    let { lessonID } = useParams();
    const auth = useContext(AuthContext);
    const profile = useContext(ProfileContext);

    let lastPress = useRef(0);

    useEffect(() => {
        setLoading(true);
        axios({
            method: "GET",
            url: `${
                process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
            }/api/lessons/${lessonID}`,
            withCredentials: true,
        })
            .then((res) => {
                setAllPageData(res.data.units);
                setUnitsCompleted(res.data.unitsCompleted);
                setCode(res.data.units[0].sample_code);
                setAutoOpen(!res.data.units[0].need_print_log);
            })
            .catch((err) => {
                if (err.response) {
                    if (err.response.status === 401 || err.response.status === 403) {
                        auth.logout();
                    }
                }
            })
            .finally(() => {
                setLoading(false);
            });
    }, [auth, lessonID]);

    const settingTheResponse = (response, continuous) => {
        if (continuous) {
            if (regex.test(response)) {
                setSubmission((prevState) => prevState + response);
            } else {
                if (allPageData[lockedPage.current - 1].need_print_log === 1 && response !== "\n") {
                    setSubmission((prevState) => prevState + `[${tempID}]:!?!?` + response + "|||");
                    setResponse((prevState) => prevState + response);
                } else {
                    setResponse((prevState) => prevState + response);
                }
            }
        } else {
            setResponse(response);
        }
    };

    const submitToServer = async () => {
        setIsError(false);
        setLoading(true);
        try {
            if (!submitState) {
                throw new Error("Not ready to submit!");
            }
            if (submitState === "" || !regex.test(submission)) {
                throw new Error("Improper submission.");
            }
            const res = await axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/turtleLessons/${lessonID}/${lockedPage.current}`,
                withCredentials: true,
                data: {
                    submission,
                },
            });

            if (res.data.isCorrect) {
                setUnitsCompleted(res.data.unitsCompleted);
                if (res.data.eventsObject) {
                    Object.keys(res.data.eventsObject).forEach((key) => {
                        makeToast(res.data.eventsObject[key]);
                    });
                }

                const res2 = await axios({
                    method: "POST",
                    url: `${
                        process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                    }/api/checkforbadges`,
                    withCredentials: true,
                });

                if (res2.data.eventsObject) {
                    Object.keys(res2.data.eventsObject).forEach((key) => {
                        makeToast(res2.data.eventsObject[key]);
                    });
                }
                profile.update();
            }
            setResponse(res.data.message);
            openConsoleHandler(true);
            openCanvasHandler(false);
        } catch (err) {
            setIsError(true);
            openConsoleHandler(true);
            if (err.message) {
                setResponse(err.message);
            } else if (err.response) {
                setResponse(err.response.data.message);
            } else {
                setResponse("Unknown error!");
            }
        } finally {
            setSubmitState(false);
            setSubmission("");
            setLoading(false);
        }
    };

    const handleCanvas = async () => {
        setIsError(false);
        setLoading(true);
        setSubmitState(false);
        setSubmission("");
        lockedPage.current = pageNumber;
        if (autoOpen) {
            openConsoleHandler(false);
        }
        try {
            await runit();
            if (!isSandbox) {
                setSubmitState(true);
            }
        } catch (err) {
            setSubmitState(false);
            setIsError(true);
            openConsoleHandler(true);
            let errorString = "";
            if (err.args) {
                errorString = err.args.v.reduce((acc, curr) => {
                    if (curr.v) return acc + curr.v + "\n";
                    return acc;
                }, "Error:");
            } else {
                errorString = "Error: " + err.message;
            }
            console.log(err);
            setResponse(errorString);
        } finally {
            setLoading(false);
        }
    };

    const handleStop = () => {
        stopRun();
    };

    const handlePageNumber = (number) => {
        setPageNumber(number);
        setCode(allPageData[number - 1].sample_code);
        setAutoOpen(!allPageData[number - 1].need_print_log);
    };

    const builtInRead = (x) => {
        if (window.Sk.builtinFiles === undefined || window.Sk.builtinFiles["files"][x] === undefined)
            throw new Error("File not found: '" + x + "'");
        return window.Sk.builtinFiles["files"][x];
    };

    const outf = (text) => {
        settingTheResponse(text, true);
    };

    const runit = async () => {
        if (autoOpen) {
            openCanvasHandler(true);
        }
        const verifyCode = allPageData[pageNumber - 1].verification_code;
        let verifyCodeToSend = ``;
        if (!isSandbox && verifyCode) {
            const verifyCodeArray = verifyCode.split(" &");
            verifyCodeArray.forEach((code, index) => {
                const test = `print('[${tempID}]:!?!?' + ${code} + ${
                    index >= verifyCodeArray.length - 1 ? "''" : "'|||'"
                })\n`;
                verifyCodeToSend += test;
            });
        }
        const codeToSend = code + "\n" + (verifyCodeToSend ? verifyCodeToSend : `print('[${tempID}]')`);
        settingTheResponse("", false);
        window.Sk.pre = "output";
        window.Sk.configure({
            inputfun: function (prompt) {
                return new Promise((resolve, reject) => {
                    let confirmed = window.prompt(prompt);
                    resolve(confirmed);
                });
            },
            inputfunTakesPrompt: true,
            output: outf,
            read: builtInRead,
        });
        canvasRef.current.style.height = "400px";
        (window.Sk.TurtleGraphics || (window.Sk.TurtleGraphics = {})).target = canvasRef.current.id;
        let myPromise = window.Sk.misceval.asyncToPromise(function () {
            return window.Sk.importMainWithBody("<stdin>", false, codeToSend, true);
        });
        return myPromise;
    };

    function stopRun() {
        window.Sk.execLimit = 1;
        window.Sk.timeoutMsg = function () {
            window.Sk.execLimit = defaultTimeLimit;
            return "Manually stopped.";
        };
    }

    const playItAgain = (e) => {
        e.stopPropagation();
        runit();
    };

    const openConsoleHandler = (shouldOpen) => {
        if (shouldOpen) {
            setOpenConsoleToolbar(true);
            consoleRef.current.ref.firstChild.style.maxHeight = "175px";
        } else {
            setOpenConsoleToolbar(false);
            consoleRef.current.ref.firstChild.style.maxHeight = "0";
        }
    };

    const openCanvasHandler = (shouldOpen) => {
        if (shouldOpen) {
            setOpenCanvasToolbar(true);
            canvasRef.current.style.height = "400px";
        } else {
            setOpenCanvasToolbar(false);
            canvasRef.current.style.height = "0";
        }
    };

    const clickMaker = (e) => {
        if (e.target.classList.contains("superText") || e.target.classList.contains("hiddenText")) {
            return e.target.parentNode;
        } else if (
            e.target.parentNode.classList.contains("superText") ||
            e.target.parentNode.classList.contains("hiddenText")
        ) {
            return e.target.parentNode.parentNode;
        }
    };

    const pressHandler = (e) => {
        e.persist();
        const clickedNode = clickMaker(e);
        const newestPress = new Date().getTime();
        const delta = newestPress - lastPress;
        const DOUBLE_PRESS_DELAY = 350;
        if (delta < DOUBLE_PRESS_DELAY) {
            if (clickedNode) {
                const hiddenText = clickedNode.querySelector(".hiddenText");
                if (hiddenText.classList.contains("visible")) {
                    hiddenText.classList.remove("visible");
                }
            }
        } else {
            if (clickedNode) {
                const hiddenText = clickedNode.querySelector(".hiddenText");
                hiddenText.classList.add("visible");
            }
        }
        lastPress = newestPress;
    };

    return (
        <PythonGrid>
            <div
                className="htmlpage"
                dangerouslySetInnerHTML={{ __html: allPageData ? allPageData[pageNumber - 1].page : "" }}
                onMouseDown={(e) => pressHandler(e)}
            ></div>

            <PythonSection
                code={code}
                setCode={setCode}
                response={response}
                setResponse={setResponse}
                loading={loading}
                sendCode={handleCanvas}
                handleStop={handleStop}
                pageNumber={pageNumber}
                totalPages={allPageData.length}
                handlePageNumber={handlePageNumber}
                submitState={submitState}
                submitToServer={submitToServer}
                unitsCompleted={unitsCompleted}
                consoleRef={consoleRef}
                openConsoleHandler={openConsoleHandler}
                openConsoleToolbar={openConsoleToolbar}
                consoleChveronRef={consoleChevronRef}
                isError={isError}
                setIsError={setIsError}
                isSandbox={isSandbox}
                setIsSandbox={setIsSandbox}
                autoOpen={autoOpen}
                setAutoOpen={setAutoOpen}
                lockedPage={lockedPage}
            >
                <CanvasOverlay
                    canvasRef={canvasRef}
                    openCanvasToolbar={openCanvasToolbar}
                    openCanvasHandler={openCanvasHandler}
                    playItAgain={playItAgain}
                    canvasChevronRef={canvasChevronRef}
                ></CanvasOverlay>
            </PythonSection>
        </PythonGrid>
    );
};

const PythonGrid = styled.div`
    display: flex;
    height: calc(100vh - ${navBarSizes.regular});
    ${below.s`
        flex-direction: column;
        height: calc(100vh - ${navBarSizes.mobile});
    `}
`;

export default PythonLesson;
