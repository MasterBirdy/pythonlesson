import React from "react";
import Button from "../ui/Button";
import { useHistory } from "react-router-dom";

const ViewStudentForm = ({
    children,
    parameters,
    setParameters,
    subjectAreas,

    grade,
    setGrade,
    chosenSubjectAreas,
    setChosenSubjectAreas,
    clear,
    search,
}) => {
    const history = useHistory();

    return (
        <form className="form-component">
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="parameters">Name</label>
                    <input
                        type="text"
                        id="parameters"
                        name="parameters"
                        value={parameters}
                        onChange={(e) => setParameters(e.target.value)}
                    ></input>
                </div>
            </div>
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="grade">Grade Level</label>
                    <select name="grade" id="grade" value={grade} onChange={(e) => setGrade(e.target.value)}>
                        <option value="">All</option>
                        <option value="3">3rd</option>
                        <option value="4">4th</option>
                        <option value="5">5th</option>
                        <option value="6">6th</option>
                        <option value="7">7th</option>
                        <option value="8">8th</option>
                    </select>
                </div>
            </div>
            <div className="flex-line">
                <div className="input-item-grow">
                    <label htmlFor="grade">Subject Areas</label>
                    <select
                        className="multiple-items"
                        name="grade"
                        id="grade"
                        value={chosenSubjectAreas}
                        multiple
                        size="3"
                        onChange={(e) => {
                            setChosenSubjectAreas([].slice.call(e.target.selectedOptions).map((o) => o.value));
                        }}
                    >
                        {Object.keys(subjectAreas).map((key) => {
                            return (
                                <option key={subjectAreas[key].id} value={subjectAreas[key].id}>
                                    {subjectAreas[key].name}
                                </option>
                            );
                        })}
                    </select>
                </div>
            </div>
            {children}
            <div className="button-toolbar-helper">
                {search && <Button aFunction={(e) => search(e)}>Search</Button>}
                {clear && <Button aFunction={(e) => clear(e)}>Clear</Button>}
                <Button aFunction={() => history.push("/classroom")}>Back</Button>
            </div>
        </form>
    );
};

export default ViewStudentForm;
