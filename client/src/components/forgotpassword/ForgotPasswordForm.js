import React from "react";
import Button from "../ui/Button";
import { Form, FlexLine, InputLineGrow, Label, Input } from "../../elements/components";

const ForgotPasswordForm = ({ email, setEmail, submit, errors, loading }) => {
    return (
        <Form>
            <FlexLine>
                <InputLineGrow>
                    <Label htmlFor="email" className={errors.email ? "error" : ""}>
                        Email
                    </Label>
                    <Input
                        type="text"
                        id="email"
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className={errors.email ? "error" : ""}
                        required
                    ></Input>
                </InputLineGrow>
            </FlexLine>
            <div className="margin-top-small-2 flex-center-helper">
                <Button aFunction={submit}>Submit</Button>
                {loading && <div className="spinner-loading-alt"></div>}
            </div>
        </Form>
    );
};

export default ForgotPasswordForm;
