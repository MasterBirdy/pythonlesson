import React from "react";
import ViewStudentsCourseTrack from "../classroom/ViewStudentsCoursesTrack";
import { SearchResultsEntry, SearchResultsCell, SearchResultsText } from "../search/SearchResults";
import styled from "styled-components";

const ViewAssignmentStatsRow = ({
    lessonID,
    firstName,
    lastName,
    grade,
    totalUnits,
    unitsCompleted,
    unitsCompletedArray,
}) => {
    return (
        <ViewAssignmentEntry>
            <SearchResultsCell col="2">
                <SearchResultsText>{firstName}</SearchResultsText>
            </SearchResultsCell>
            <SearchResultsCell col="2">
                <SearchResultsText>{lastName}</SearchResultsText>
            </SearchResultsCell>
            <SearchResultsCell col="2">
                <>{grade}</>
            </SearchResultsCell>
            <SearchResultsCell col="2">
                <SearchResultsText
                    className={
                        unitsCompleted ? (unitsCompleted === totalUnits ? "completed" : "inprogress") : "notstarted"
                    }
                >
                    {unitsCompleted ? (unitsCompleted === totalUnits ? "Completed" : "In Progress") : "Not Started"}
                </SearchResultsText>
            </SearchResultsCell>
            <SearchResultsCell col="4">
                {unitsCompletedArray ? (
                    <ViewStudentsCourseTrack
                        lessonID={lessonID}
                        courses={unitsCompletedArray}
                    ></ViewStudentsCourseTrack>
                ) : (
                    "No Units!"
                )}
            </SearchResultsCell>
        </ViewAssignmentEntry>
    );
};

const ViewAssignmentEntry = styled(SearchResultsEntry)`
    grid-template-columns: repeat(12, 1fr);
`;

export default ViewAssignmentStatsRow;
