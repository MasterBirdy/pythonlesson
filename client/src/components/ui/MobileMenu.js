import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

const MobileMenu = ({ links, isOpen, closeHandler, logout, authed }) => {
    return (
        <div className={["mobile-menu", isOpen ? "is-open" : ""].join(" ")}>
            <ul className="links-list">
                {links.map((link) => {
                    return (
                        <li key={link.name}>
                            <Link to={link.path}>{link.name}</Link>
                        </li>
                    );
                })}
                {authed && (
                    <li className="mobile-logout" key="mobile-logout">
                        <p onClick={() => logout()}>Logout</p>
                    </li>
                )}
            </ul>

            <div className="icon-holder" onClick={closeHandler}>
                <FontAwesomeIcon icon={faTimes} size="3x"></FontAwesomeIcon>
            </div>
        </div>
    );
};

export default MobileMenu;
