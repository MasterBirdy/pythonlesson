import React, { useContext } from "react";
import math from "../images/math.svg";
import english from "../images/english.svg";
import science from "../images/science.svg";
import standingturtle from "../images/standingturtle.svg";
import { useHistory } from "react-router-dom";
import { ProfileContext } from "../context/ProfileContext";
import { Container } from "../elements/components/";
import { below, colors } from "../elements/utilities";
import styled from "styled-components";

const Browse = () => {
    const profile = useContext(ProfileContext);
    const history = useHistory();
    let urlExtension = "";
    if (profile.grade) {
        urlExtension += `grade=${profile.grade}&`;
    }
    return (
        <Container>
            <BrowseGrid>
                <BrowseGridItem onClick={() => history.push(`searchresults?${urlExtension}subjectareas=1`)}>
                    <BrowseGridImage src={math} alt="Math chalkboard"></BrowseGridImage>
                    <p>Math</p>
                </BrowseGridItem>
                <BrowseGridItem onClick={() => history.push(`searchresults?${urlExtension}subjectareas=3`)}>
                    <BrowseGridImage src={english} alt="Books"></BrowseGridImage>
                    <p>English</p>
                </BrowseGridItem>
                <BrowseGridItem onClick={() => history.push(`searchresults?${urlExtension}subjectareas=2`)}>
                    <BrowseGridImage src={science} alt="Science molecule"></BrowseGridImage>
                    <p>Science</p>
                </BrowseGridItem>
                <BrowseGridItem onClick={() => history.push(`searchresults?${urlExtension}name=Turtle%20Quest`)}>
                    <BrowseGridImage src={standingturtle} alt="Happy turtle"></BrowseGridImage>
                    <p>Turtle Lessons</p>
                </BrowseGridItem>
            </BrowseGrid>
        </Container>
    );
};

export const BrowseGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    margin: 7% auto 0 auto;
    grid-row-gap: 5rem;
    grid-column-gap: 1rem;
    padding-bottom: 1rem;
    max-width: 60rem;
    ${below.xs`
    grid-template-columns: 1fr;
    grid-row-gap: 2rem;
    `}
`;

export const BrowseGridItem = styled.div`
    border: 1px ${colors.browseGridItemBorder} solid;
    box-shadow: 3px 6px 11px 1px ${colors.browseGridBoxShadow};
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    padding: 2.75rem 2.75rem 1rem 2.75rem;
    width: 100%;
    max-width: 25rem;
    max-height: 25rem;
    margin: 0 auto;
    transition: all 0.1s ease-out;
    &:hover {
        transform: translateY(-2px);
        box-shadow: 3px 6px 12px 2px ${colors.browseGridItemHover};
    }

    &:active {
        transform: translateY(-1px);
        box-shadow: 3px 6px 11px 1px ${colors.browseGridItemActive};
    }
    cursor: pointer;
    p {
        margin-top: 1rem;
        font-size: 2.3em;
        font-weight: 600;
        letter-spacing: 1px;
        color: ${colors.browseGridText};
    }

    img {
        object-fit: contain;
        width: 100%;
        max-width: 15rem;
    }
`;

export const BrowseGridImage = styled.img`
    object-fit: contain;
    width: 100%;
    max-width: 15rem;
`;

export default Browse;
