import { useState, useEffect } from "react";

export const usePagination = (pageLimit, length) => {
    const [currentPage, setCurrentPage] = useState(0);
    const [currentLimit, setCurrentLimit] = useState(0);

    const paginationHandler = (i) => {
        setCurrentLimit((prev) => {
            if (i < prev && ![i < 0]) {
                return prev--;
            } else if (i + pageLimit > prev && !(prev * pageLimit > length)) {
                return prev++;
            } else {
                return prev;
            }
        });
        setCurrentPage((prev) => {
            return !(i < 0) && !(i * pageLimit >= length) ? i : prev;
        });
    };

    return { currentPage, currentLimit, paginationHandler };
};
