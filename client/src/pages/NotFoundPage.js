import React from "react";
import turtle from "../images/turtle-svgrepo.svg";
import { Link } from "react-router-dom";
import { Container } from "../elements/components/";
import styled from "styled-components";
const NotFoundPage = () => {
    return (
        <Container>
            <NotFoundMain>
                <h1>Oops!</h1>
                <p>Your page was not found.</p>
                <img src={turtle} alt="Turtle swimming"></img>
                <h2>Suggested Links</h2>
                <NotFoundLinks>
                    <Link to="/">Home</Link> • <Link to="/register">Register</Link> • <Link to="/login">Login</Link> •{" "}
                    <Link to="/profile">Profile</Link>
                </NotFoundLinks>
            </NotFoundMain>
        </Container>
    );
};

export const NotFoundLinks = styled.div`
    margin-top: 0.5rem;
    font-size: 1.6rem;

    a {
        text-decoration: none;
    }
`;

export const NotFoundMain = styled.main`
    font-family: "PT Sans", sans-serif;
    text-align: center;
    margin-top: 1rem;

    h1 {
        font-size: 3.6rem;
    }

    h2 {
        font-size: 2.1rem;
    }
    p {
        font-size: 1.6rem;
    }

    img {
        width: 100%;
        max-width: 15rem;
    }
`;

export default NotFoundPage;
