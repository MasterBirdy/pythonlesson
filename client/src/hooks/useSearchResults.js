import { useState, useEffect } from "react";
import axios from "axios";

const sortBy = (a, b, label) => {
    let aChosen = a[label];
    let bChosen = b[label];
    if (!aChosen) {
        aChosen = "None";
    }
    if (!bChosen) {
        bChosen = "None";
    }
    if (aChosen < bChosen) return -1;
    if (aChosen > bChosen) return 1;
    return 0;
};

const sortGrade = (a, b) => {
    const aGrade = a.grades ? a.grades[0] : Infinity;
    const bGrade = b.grades ? b.grades[0] : Infinity;
    if (aGrade < bGrade) return -1;
    if (aGrade > bGrade) return 1;
    return 0;
};

const progressSortMethod = (a, b) => {
    const aString =
        a.totalLessons === a.completedLessons ? "Completed" : a.completedLessons !== 0 ? "In Progress" : "Not Started";
    const bString =
        b.totalLessons === b.completedLessons ? "Completed" : b.completedLessons !== 0 ? "In Progress" : "Not Started";
    if (aString < bString) return -1;
    if (aString > bString) return 1;
    return 0;
};

export const useSearchResults = (url, allow) => {
    const [loading, setLoading] = useState(true);
    const [results, setResults] = useState([]);
    const [reverse, setReverse] = useState(false);
    const [titleSort, setTitleSort] = useState(true);
    const [subjectAreaSort, setSubjectAreaSort] = useState(false);
    const [standardSort, setStandardSort] = useState(false);
    const [progressSort, setProgressSort] = useState(false);
    const [lessonSort, setLessonSort] = useState(false);
    const [gradeSort, setGradeSort] = useState(false);

    useEffect(() => {
        setLoading(true);
        if (allow) {
            axios({
                method: "GET",
                url,
                withCredentials: true,
            })
                .then((res) => {
                    console.log(res.data.results);
                    setResults(res.data.results);
                })
                .catch((err) => {
                    console.log(err);
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    }, [url, allow]);

    let sortedResults = results;

    if (titleSort) {
        sortedResults = results
            .concat()
            .sort(reverse ? (a, b) => sortBy(a, b, "name") * -1 : (a, b) => sortBy(a, b, "name"));
    } else if (subjectAreaSort) {
        sortedResults = results
            .concat()
            .sort((a, b) => sortBy(a, b, "name"))
            .sort(reverse ? (a, b) => sortBy(a, b, "subjectAreaName") * -1 : (a, b) => sortBy(a, b, "subjectAreaName"));
    } else if (standardSort) {
        sortedResults = results
            .concat()
            .sort((a, b) => sortBy(a, b, "name"))
            .sort(reverse ? (a, b) => sortBy(a, b, "standardName") * -1 : (a, b) => sortBy(a, b, "standardName"));
    } else if (progressSort) {
        sortedResults = results
            .concat()
            .sort((a, b) => sortBy(a, b, "name"))
            .sort(reverse ? (a, b) => progressSortMethod(a, b) * -1 : (a, b) => progressSortMethod(a, b));
    } else if (lessonSort) {
        sortedResults = results
            .concat()
            .sort((a, b) => sortBy(a, b, "name"))
            .sort((a, b) => sortBy(a, b, "totalLessons"))
            .sort(
                reverse ? (a, b) => sortBy(a, b, "completedLessons") * -1 : (a, b) => sortBy(a, b, "completedLessons")
            )
            .sort(reverse ? (a, b) => progressSortMethod(a, b) * -1 : (a, b) => progressSortMethod(a, b));
    } else if (gradeSort) {
        sortedResults = results
            .concat()
            .sort((a, b) => sortBy(a, b, "name"))
            .sort(reverse ? (a, b) => sortGrade(a, b) * -1 : (a, b) => sortGrade(a, b));
    }

    const reset = (parameter) => {
        setTitleSort(parameter === "title");
        setSubjectAreaSort(parameter === "subjectarea");
        setStandardSort(parameter === "standard");
        setProgressSort(parameter === "progress");
        setLessonSort(parameter === "lesson");
        setGradeSort(parameter === "grade");
        setReverse(false);
    };

    const titleHandler = (e) => {
        if (!titleSort) {
            reset("title");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    const subjectAreaHandler = (e) => {
        if (!subjectAreaSort) {
            reset("subjectarea");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    const standardHandler = (e) => {
        if (!standardSort) {
            reset("standard");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    const progressHandler = (e) => {
        if (!progressSort) {
            reset("progress");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    const lessonHandler = (e) => {
        if (!lessonSort) {
            reset("lesson");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    const gradeHandler = (e) => {
        if (!gradeSort) {
            reset("grade");
        } else {
            setReverse((prevState) => !prevState);
        }
    };

    return {
        results,
        sortedResults,
        reverse,
        loading,
        titleSort,
        titleHandler,
        subjectAreaSort,
        subjectAreaHandler,
        standardSort,
        standardHandler,
        progressSort,
        progressHandler,
        lessonSort,
        lessonHandler,
        gradeSort,
        gradeHandler,
    };
};
