"use strict";

const Hapi = require("@hapi/hapi");
const Inert = require("@hapi/inert");
// const User = require("./models/User.js");
const knex = require("./database/db");
const python = require("./routes/Python.js");
const user = require("./routes/Users.js");
const school = require("./routes/School.js");
const profile = require("./routes/Profile.js");
const search = require("./routes/Search.js");
const badges = require("./routes/Badges.js");
const assignments = require("./routes/Assignments.js");
const path = require("path");

require("dotenv").config();

const init = async () => {
    const server = new Hapi.Server({
        host: "localhost",
        port: "5000",
        routes: {
            cors: {
                origin: ["*"],
                credentials: true,
            },
        },
    });

    await server.register(require("hapi-auth-jwt2"));

    const validate = async function (decoded, request, h) {
        const account = await knex.select().from("users").where("id", decoded.id);

        if (!account) {
            return { isValid: false };
        } else {
            return { isValid: true };
        }
    };

    const teacherValidate = async function (decoded, request, h) {
        const account = await knex.select().from("users").where("id", decoded.id);
        if (!account) {
            return { isValid: false };
        } else {
            return { isValid: !!account[0].teacher_at };
        }
    };

    const studentValidate = async function (decoded, request, h) {
        const account = await knex.select().from("users").where("id", decoded.id);
        if (!account) {
            return { isValid: false };
        } else {
            return { isValid: !account[0].teacher_at };
        }
    };

    server.auth.strategy("jwt", "jwt", {
        key: process.env.JWT_SECRET,
        validate,
        verifyOptions: { algorithms: ["HS256"] },
    });

    server.auth.strategy("teacher", "jwt", {
        key: process.env.JWT_SECRET,
        validate: teacherValidate,
        verifyOptions: { algorithms: ["HS256"] },
    });

    server.auth.strategy("student", "jwt", {
        key: process.env.JWT_SECRET,
        validate: studentValidate,
        verifyOptions: { algorithms: ["HS256"] },
    });

    server.auth.default("jwt");

    server.route([].concat(python, user, school, profile, search, badges, assignments));

    await server.start((err) => {
        if (err) {
            throw err;
        }
        console.log("Server running now!");
    });
};

init();
