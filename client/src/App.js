import React, { useContext } from "react";
import { Route, BrowserRouter, Switch, Redirect } from "react-router-dom";

import Login from "./pages/Login";
import Register from "./pages/Register";
import HomePage from "./pages/HomePage";
import Profile from "./pages/Profile";
import PythonLessonCanvas from "./pages/PythonLessonCanvas";
import { AuthProvider, AuthContext } from "./context/AuthContext";
import { ProfileProvider } from "./context/ProfileContext";
import Search from "./pages/Search";
import NormalLayout from "./layouts/NormalLayout";
import SearchResultsPage from "./pages/SearchResultsPage";
import EditProfile from "./pages/EditProfile";
import EditStudent from "./pages/EditStudent";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import { ToastContainer } from "react-toastify";
import Classroom from "./pages/Classroom";
import AddToClassroom from "./pages/AddToClassroom";
import ViewStudent from "./pages/ViewStudent";
import PublicProfile from "./pages/PublicProfile";
import NotFoundPage from "./pages/NotFoundPage";
import AddAssignment from "./pages/AddAssignmentPage";
import EditAssignment from "./pages/EditAssignment";
import DeleteAssignment from "./pages/DeleteAssignment";
import AssignmentDashboard from "./pages/AssignmentDashboard";
import ViewAssignmentStats from "./pages/ViewAssignmentStats";
import Browse from "./pages/Browse";

const AuthenticatedRoute = ({ component: Component, noLayout, auth, ...rest }) => {
    const authContext = useContext(AuthContext);
    return (
        <Route
            {...rest}
            render={() => {
                return authContext.isAuthenticated() ? (
                    <NormalLayout>
                        <Component></Component>
                    </NormalLayout>
                ) : (
                    <Redirect to="/login"></Redirect>
                );
            }}
        ></Route>
    );
};

function App() {
    const routes = [
        { path: "/", name: "homepage", Component: HomePage, noLayout: true },
        { path: "/login", name: "login", Component: Login },
        { path: "/forgotpassword", name: "forgotpassword", Component: ForgotPassword },
        { path: "/register", name: "register", Component: Register },
        { path: "/profile", name: "profile", Component: Profile, auth: true },
        { path: "/turtlelesson/:lessonID", name: "canvas", Component: PythonLessonCanvas, auth: true },
        { path: "/search", name: "search", Component: Search, auth: true },
        { path: "/searchresults", name: "searchresults", Component: SearchResultsPage, auth: true },
        { path: "/editprofile", name: "editprofile", Component: EditProfile, auth: true },
        { path: "/classroom", name: "classroom", Component: Classroom, auth: "teacher" },
        { path: "/browse", name: "browse", Component: Browse, auth: true },
        { path: "/assignments", name: "assignments", Component: AssignmentDashboard, auth: "student" },
        { path: "/addtoclassroom", name: "addtoclassroom", Component: AddToClassroom, auth: "teacher" },
        { path: "/addassignment/", name: "addassignment", Component: AddAssignment, auth: "teacher" },
        { path: "/editstudent/:id", name: "editstudent", Component: EditStudent, auth: "teacher" },
        { path: "/viewstudent/:id", name: "viewstudent", Component: ViewStudent, auth: "teacher" },
        {
            path: "/viewassignmentstats/:id",
            name: "viewassignmentstats",
            Component: ViewAssignmentStats,
            auth: "teacher",
        },
        { path: "/editassignment/:id", name: "editassignment", Component: EditAssignment, auth: "teacher" },
        { path: "/deleteassignment/:id", name: "deleteassignment", Component: DeleteAssignment, auth: "teacher" },
        { path: "/profile/:id", name: "publicprofile", Component: PublicProfile },
        { path: "/resetpassword/:token", name: "resetpassword", Component: ResetPassword },
        { path: "*", name: "notfoundpage", Component: NotFoundPage, noLayout: true },
    ];

    return (
        <BrowserRouter>
            <AuthProvider>
                <ProfileProvider>
                    <Switch>
                        {routes.map(({ path, Component, auth, noLayout, ...rest }) => {
                            if (auth) {
                                return (
                                    <AuthenticatedRoute
                                        {...rest}
                                        auth={auth}
                                        key={path}
                                        exact
                                        path={path}
                                        component={Component}
                                    />
                                );
                            } else {
                                return (
                                    <Route
                                        {...rest}
                                        key={path}
                                        exact
                                        path={path}
                                        render={() =>
                                            noLayout ? (
                                                <Component></Component>
                                            ) : (
                                                <NormalLayout>
                                                    <Component></Component>
                                                </NormalLayout>
                                            )
                                        }
                                    />
                                );
                            }
                        })}
                    </Switch>
                </ProfileProvider>
            </AuthProvider>

            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick={false}
                rtl={false}
                pauseOnFocusLoss={false}
                draggable={false}
                pauseOnHover={false}
            />
        </BrowserRouter>
    );
}

export default App;
