import { toast } from "react-toastify";

export function makeToast(message) {
    toast.success(message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
    });
}

export function makeInfoToast(message) {
    toast.info(message, {
        position: "bottom-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
    });
}

export function makeWarningToast(message) {
    toast.warn(message, {
        position: "bottom-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
    });
}

export function makeErrorToast(message) {
    toast.error(message, {
        position: "bottom-right",
        autoClose: 10000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
    });
}

export const passwordRegex = /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}/;

export function parseDate(theDate) {
    const date = new Date(theDate);
    const AMPM = date.getHours() > 11 ? "PM" : "AM";
    const hour = date.getHours() ? (date.getHours() > 12 ? date.getHours() - 12 : date.getHours()) : 12;
    const minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    const time = `${hour}:${minutes} ${AMPM}`;
    const realMonth = parseInt(date.getMonth()) + 1;
    return { date, AMPM, hour, minutes, time, realMonth };
}
