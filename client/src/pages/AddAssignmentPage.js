import React, { useContext, useState, useEffect, useMemo } from "react";
import { makeToast, makeInfoToast, makeWarningToast, makeErrorToast } from "../utils/helpers";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import { useAutoComplete } from "../hooks/useAutoComplete";
import AllParametersForm from "../components/classroom/AllParametersForm";
import StandardsAutoComplete from "../components/search/StandardsAutoComplete";
import TimeFormElement from "../components/classroom/TimeFormElement";
import { usePagination } from "../hooks/usePagination";
import { useSearchResults } from "../hooks/useSearchResults";
import SearchResults from "../components/search/SearchResults";
import Pagination from "../components/search/Pagination";
import { useLocation } from "react-router-dom";
import Button from "../components/ui/Button";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { ProfileContext } from "../context/ProfileContext";
import ErrorComponent from "../components/ui/ErrorComponent";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    IconsContainer,
    Container,
    Form,
} from "../elements/components/";
import { below } from "../elements/utilities";
import styled from "styled-components";

const pageLimit = 5;

const AddAssignment = () => {
    const [parameters, setParameters] = useState("");
    const [grade, setGrade] = useState("");
    const [subjectAreas, setSubjectAreas] = useState("");
    const [chosenSubjectAreas, setChosenSubjectAreas] = useState([]);
    const [standards, setStandards] = useState({});
    const [loading, setLoading] = useState(true);
    const [hasSearched, setHasSearched] = useState(false);
    // const [isMobile] = useMobile();
    const [searchURL, setSearchURL] = useState("");
    const [selectedAssignment, setSelectedAssignment] = useState({});
    const [displayAssignment, setDisplayAssignment] = useState("");
    const [dueDate, setDueDate] = useState("");
    const [dueTime, setDueTime] = useState("00:00");
    const [isError, setIsError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const location = useLocation();
    const history = useHistory();
    const profile = useContext(ProfileContext);
    const auth = useContext(AuthContext);
    const {
        results,
        sortedResults,
        reverse,
        loading: resultsLoading,
        titleSort,
        subjectAreaSort,
        standardSort,
        gradeSort,
        gradeHandler,
        titleHandler,
        subjectAreaHandler,
        standardHandler,
    } = useSearchResults(
        `${
            process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
        }/api/searchresults${searchURL}`,
        hasSearched
    );
    const { currentPage, currentLimit, paginationHandler } = usePagination(pageLimit, results.length);

    const [
        showAutoComplete,
        standardInput,
        setStandardInput,
        autoCompleteRef,
        standardInputHandler,
        autoCompleteHandler,
    ] = useAutoComplete();

    useEffect(() => {
        if (auth.isAuthenticated() && profile.role) {
            if (profile.role === "teacher") {
                axios({
                    method: "GET",
                    url: `${
                        process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                    }/api/search`,
                    withCredentials: true,
                })
                    .then((res) => {
                        setSubjectAreas(res.data.subjectAreas);
                        setStandards(res.data.standards);
                    })
                    .catch((err) => {
                        console.log(err);
                    })
                    .finally(() => {
                        setLoading(false);
                    });
            } else {
                setIsError(true);
                setErrorMessage("You are unauthorized to view this page.");
                setLoading(false);
            }
        }
    }, [auth, profile]);

    const filteredStandards = useMemo(() => {
        if (grade) {
            return standards[grade].filter((standard) => {
                return standard.name.toLowerCase().indexOf(standardInput.toLowerCase()) > -1;
            });
        } else {
            let standardsArray = [];
            Object.keys(standards).forEach((key) => {
                standards[key].forEach((standard) => {
                    if (standard.name.toLowerCase().indexOf(standardInput.toLowerCase()) > -1) {
                        standardsArray.push(standard);
                    }
                });
            });
            return standardsArray.sort((a, b) => {
                if (a.name < b.name) return -1;
                if (a.name > b.name) return 1;
                return 0;
            });
        }
    }, [grade, standards, standardInput]);

    const formHandler = (e) => {
        e.preventDefault();

        let hasBeenSet = false;
        let urlConstructor = ``;
        if (parameters) {
            urlConstructor += `name=${parameters}`;
            hasBeenSet = true;
        }

        if (grade) {
            if (hasBeenSet) {
                urlConstructor += `&`;
            }
            urlConstructor += `grade=${grade}`;
            hasBeenSet = true;
        }

        if (chosenSubjectAreas.length) {
            if (hasBeenSet) {
                urlConstructor += `&`;
            }
            urlConstructor += `subjectareas=`;
            chosenSubjectAreas.forEach((subjectArea, index) => {
                urlConstructor += `${subjectArea}`;
                if (!(index >= chosenSubjectAreas.length - 1)) {
                    urlConstructor += `,`;
                }
            });
            hasBeenSet = true;
        }

        if (standardInput) {
            if (hasBeenSet) {
                urlConstructor += `&`;
            }
            urlConstructor += `standard=${standardInput}`;
            hasBeenSet = true;
        }

        if (urlConstructor !== ``) {
            urlConstructor = `?` + urlConstructor;
        }

        setSearchURL(urlConstructor);
        setHasSearched(true);
    };

    const clearHandler = (e) => {
        e.preventDefault();
        setParameters("");
        setGrade("");
        setChosenSubjectAreas([]);
        setStandardInput("");
    };

    const clearHandler2 = (e) => {
        e.preventDefault();
        setDueDate("");
        setDueTime("00:00");
        setSelectedAssignment({});
    };

    const addHandler = (id, name) => {
        setSelectedAssignment({ id, name });
    };

    const validate = () => {
        return !!selectedAssignment.id && !!dueDate && !!dueTime;
    };

    const submitHandler = (e) => {
        e.preventDefault();
        if (!validate()) {
            makeWarningToast("Make sure that all fields are filled out in the Submit Assignment form!");
        } else {
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/submitassignment`,
                data: {
                    lessonID: selectedAssignment.id,
                    dueDate: `${dueDate} ${dueTime}:00`,
                },
                withCredentials: true,
            })
                .then((res) => {
                    if (res.status === 200) {
                        makeToast("Assignment successfully added.");
                        history.push("/classroom");
                    }
                })
                .catch((err) => {
                    if (err.response) {
                        makeErrorToast(err.response.data.err);
                    }
                });
        }
    };

    useEffect(() => {
        setDisplayAssignment(selectedAssignment.name ? selectedAssignment.name : "");
    }, [selectedAssignment]);

    let slicedResults = sortedResults.slice(currentPage * pageLimit, currentPage * pageLimit + pageLimit);

    if (loading) {
        return <div className="loader"></div>;
    }

    if (isError) {
        return (
            <div className="container">
                <ErrorComponent errorMessage={errorMessage}></ErrorComponent>
            </div>
        );
    }

    return (
        <Container>
            <AddAssignmentGrid>
                <TitleBox>
                    <TitleBoxTitleContainer>
                        <TitleBoxTitle>Add Assignment</TitleBoxTitle>
                        <IconsContainer>
                            <FontAwesomeIcon
                                icon={faQuestion}
                                onClick={() =>
                                    makeInfoToast(
                                        `ℹ️ Search for an assignment to add to your classroom by filling out any of the form parameters and then pressing Search.`
                                    )
                                }
                            ></FontAwesomeIcon>
                        </IconsContainer>
                    </TitleBoxTitleContainer>
                    <TitleBoxContainer>
                        <AllParametersForm
                            parameters={parameters}
                            setParameters={setParameters}
                            grade={grade}
                            setGrade={setGrade}
                            subjectAreas={subjectAreas}
                            setSubjectAreas={setSubjectAreas}
                            chosenSubjectAreas={chosenSubjectAreas}
                            setChosenSubjectAreas={setChosenSubjectAreas}
                            search={formHandler}
                            clear={clearHandler}
                        >
                            <StandardsAutoComplete
                                standardInputHandler={standardInputHandler}
                                autoCompleteRef={autoCompleteRef}
                                standardInput={standardInput}
                                standards={filteredStandards}
                                showAutoComplete={showAutoComplete}
                                autoCompleteHandler={autoCompleteHandler}
                            ></StandardsAutoComplete>
                        </AllParametersForm>
                    </TitleBoxContainer>
                </TitleBox>

                {hasSearched &&
                    (resultsLoading ? (
                        <div className="loader"></div>
                    ) : (
                        <>
                            <SubmitAssignmentTitleBox>
                                <TitleBoxTitleContainer>
                                    <TitleBoxTitle>Submit Assignment</TitleBoxTitle>
                                    <IconsContainer>
                                        <FontAwesomeIcon
                                            icon={faQuestion}
                                            onClick={() =>
                                                makeInfoToast(
                                                    `ℹ️ Add the selected assignment to your classroom by specifying the due date and due time.`
                                                )
                                            }
                                        ></FontAwesomeIcon>
                                    </IconsContainer>
                                </TitleBoxTitleContainer>
                                <TitleBoxContainer>
                                    <Form>
                                        <TimeFormElement
                                            type="input"
                                            label="Assignment Name"
                                            inputValue={displayAssignment}
                                            inputValueHandler={setDisplayAssignment}
                                            disabled={true}
                                        ></TimeFormElement>
                                        <TimeFormElement
                                            type="date"
                                            label="Assignment Due Date"
                                            inputValue={dueDate}
                                            inputValueHandler={setDueDate}
                                        ></TimeFormElement>
                                        <TimeFormElement
                                            type="time"
                                            label="Assignment Due Time"
                                            inputValue={dueTime}
                                            inputValueHandler={setDueTime}
                                        ></TimeFormElement>
                                    </Form>
                                    <div className="margin-top-small button-toolbar-helper">
                                        <Button aFunction={(e) => submitHandler(e)}>Submit</Button>
                                        <Button aFunction={(e) => clearHandler2(e)}>Clear</Button>
                                    </div>
                                </TitleBoxContainer>
                            </SubmitAssignmentTitleBox>
                            <div>
                                <Pagination
                                    currentPage={currentPage}
                                    currentLimit={currentLimit}
                                    totalNumber={results.length}
                                    pageLimit={pageLimit}
                                    paginationHandler={paginationHandler}
                                    isMobile={true}
                                ></Pagination>
                                <TitleBox marginTop="1.5">
                                    <TitleBoxTitleContainer>
                                        <TitleBoxTitle>Search Results</TitleBoxTitle>
                                        <IconsContainer>
                                            <FontAwesomeIcon
                                                icon={faQuestion}
                                                onClick={() =>
                                                    makeInfoToast(
                                                        `ℹ️ Add an assignment to your classroom by selecting the plus icon for the assignment.`
                                                    )
                                                }
                                            ></FontAwesomeIcon>
                                        </IconsContainer>
                                    </TitleBoxTitleContainer>

                                    <SearchResults
                                        results={slicedResults}
                                        titleSort={titleSort}
                                        titleHandler={titleHandler}
                                        subjectAreaHandler={subjectAreaHandler}
                                        subjectAreaSort={subjectAreaSort}
                                        standardSort={standardSort}
                                        standardHandler={standardHandler}
                                        reverse={reverse}
                                        isMobile={true}
                                        gradeSort={gradeSort}
                                        gradeHandler={gradeHandler}
                                        location={location.pathname}
                                        addHandler={addHandler}
                                        selectedAddID={selectedAssignment.id ? selectedAssignment.id : -1}
                                    ></SearchResults>
                                </TitleBox>
                            </div>
                        </>
                    ))}
            </AddAssignmentGrid>
        </Container>
    );
};

export const AddAssignmentGrid = styled.div`
    display: grid;
    margin-top: 2rem;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 1.5rem;
    font-family: "PT Sans", sans-serif;
    font-weight: 400;
    ${below.s`
        grid-template-columns: 1fr;
    `}
`;

export const SubmitAssignmentTitleBox = styled(TitleBox)`
    ${below.s`
    margin-top: 1.5rem;
`}
`;

export default AddAssignment;
