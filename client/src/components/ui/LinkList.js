import React from "react";
import { useHistory } from "react-router-dom";

const LinkList = ({ isVisible, links, role }) => {
    const history = useHistory();

    const filteredLinks = links.filter((link) => {
        if (link.auth === "teacher" && role !== "teacher") return false;
        if (link.auth === "student" && role !== "student") return false;
        return true;
    });
    return (
        <ul className={["link-list", isVisible ? "visible" : ""].join(" ")}>
            {filteredLinks.map((link) => {
                return (
                    <li onClick={() => history.push(link.path)} key={link.name}>
                        <p>{link.name}</p>
                    </li>
                );
            })}
        </ul>
    );
};

export default LinkList;
