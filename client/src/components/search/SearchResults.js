import React from "react";
import SearchResultRow from "./SearchResultRow";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { TitleBoxDuoColorListContainer, TitleBoxDouColorListEntry } from "../../elements/components/TitleBox";
import { colors, below } from "../../elements/utilities";
import { darken, lighten } from "polished";
import styled from "styled-components";

const pageLimit = 10;

const SearchResults = ({
    results,
    reverse,
    titleHandler,
    titleSort,
    subjectAreaSort,
    subjectAreaHandler,
    standardHandler,
    standardSort,
    isMobile,
    progressSort,
    progressHandler,
    lessonSort,
    lessonHandler,
    gradeSort,
    gradeHandler,
    addHandler,
    location,
    selectedAddID,
}) => {
    let returnResults = <SearchResultsEntry className="empty">No results returned.</SearchResultsEntry>;

    if (results.length) {
        returnResults = results.map((result) => {
            return (
                <SearchResultRow
                    id={result.id}
                    key={result.id}
                    name={result.name}
                    completedLessons={result.completedLessons}
                    totalLessons={result.totalLessons}
                    subjectName={result.subjectName}
                    subjectAreaName={result.subjectAreaName}
                    subjectID={result.subject_id}
                    standardName={result.standardName}
                    isMobile={isMobile}
                    grades={result.grades}
                    location={location}
                    addHandler={addHandler}
                    selectedAddID={selectedAddID}
                ></SearchResultRow>
            );
        });
    }
    // return (
    //     <div className="search-results-grid">
    //         <div className="search-result-row search-result-row-heading">
    //             <div className="search-result-cell search-result-cell-heading col-2" onClick={standardHandler}>
    //                 <p>{isMobile ? "Stan." : "Standard"}</p>
    //                 {standardSort && (
    //                     <FontAwesomeIcon
    //                         icon={reverse ? faCaretUp : faCaretDown}
    //                         color={reverse ? "crimson" : "green"}
    //                     ></FontAwesomeIcon>
    //                 )}
    //             </div>
    //             <div className="search-result-cell search-result-cell-heading col-4" onClick={titleHandler}>
    //                 <p>Title</p>
    //                 {titleSort && (
    //                     <FontAwesomeIcon
    //                         icon={reverse ? faCaretUp : faCaretDown}
    //                         color={reverse ? "crimson" : "green"}
    //                     ></FontAwesomeIcon>
    //                 )}
    //             </div>
    //             <div className="search-result-cell search-result-cell-heading col-2" onClick={subjectAreaHandler}>
    //                 <p>{isMobile ? "Sub" : "Subject"}</p>
    //                 {subjectAreaSort && (
    //                     <FontAwesomeIcon
    //                         icon={reverse ? faCaretUp : faCaretDown}
    //                         color={reverse ? "crimson" : "green"}
    //                     ></FontAwesomeIcon>
    //                 )}
    //             </div>
    //             {location === "/addassignment" && (
    //                 <div
    //                     className={["search-result-cell", "search-result-cell-heading", "col-2"].join(" ")}
    //                     onClick={gradeHandler}
    //                 >
    //                     <p>Grade</p>
    //                     {gradeSort && (
    //                         <FontAwesomeIcon
    //                             icon={reverse ? faCaretUp : faCaretDown}
    //                             color={reverse ? "crimson" : "green"}
    //                         ></FontAwesomeIcon>
    //                     )}
    //                 </div>
    //             )}
    //             {location === "/addassignment" && (
    //                 <div className="search-result-cell search-result-cell-heading">
    //                     <p>Add</p>
    //                 </div>
    //             )}
    //             {location === "/searchresults" && (
    //                 <div
    //                     className={["search-result-cell", "search-result-cell-heading", "col-2"].join(" ")}
    //                     onClick={progressHandler}
    //                 >
    //                     <p>{isMobile ? "Prog." : "Progress"}</p>
    //                     {progressSort && (
    //                         <FontAwesomeIcon
    //                             icon={reverse ? faCaretUp : faCaretDown}
    //                             color={reverse ? "crimson" : "green"}
    //                         ></FontAwesomeIcon>
    //                     )}
    //                 </div>
    //             )}
    //             {location === "/searchresults" && (
    //                 <div className="search-result-cell search-result-cell-heading" onClick={lessonHandler}>
    //                     <p>{isMobile ? "Les." : "Lessons"}</p>
    //                     {lessonSort && (
    //                         <FontAwesomeIcon
    //                             icon={reverse ? faCaretUp : faCaretDown}
    //                             color={reverse ? "crimson" : "green"}
    //                         ></FontAwesomeIcon>
    //                     )}
    //                 </div>
    //             )}
    //         </div>
    //         {returnResults}
    //     </div>
    // );

    return (
        <SearchResultsGrid>
            <SearchResultsHeading>
                <SearchResultsCellHeading col="2" onClick={standardHandler}>
                    <SearchResultsText>{isMobile ? "Stan." : "Standard"}</SearchResultsText>
                    {standardSort && (
                        <FontAwesomeIcon
                            icon={reverse ? faCaretUp : faCaretDown}
                            color={reverse ? "crimson" : "green"}
                        ></FontAwesomeIcon>
                    )}
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="4" onClick={titleHandler}>
                    <SearchResultsText>Title</SearchResultsText>
                    {titleSort && (
                        <FontAwesomeIcon
                            icon={reverse ? faCaretUp : faCaretDown}
                            color={reverse ? "crimson" : "green"}
                        ></FontAwesomeIcon>
                    )}
                </SearchResultsCellHeading>
                <SearchResultsCellHeading col="2" onClick={subjectAreaHandler}>
                    <SearchResultsText>{isMobile ? "Sub" : "Subject"}</SearchResultsText>
                    {subjectAreaSort && (
                        <FontAwesomeIcon
                            icon={reverse ? faCaretUp : faCaretDown}
                            color={reverse ? "crimson" : "green"}
                        ></FontAwesomeIcon>
                    )}
                </SearchResultsCellHeading>
                {location === "/addassignment" && (
                    <SearchResultsCellHeading col="2" onClick={gradeHandler}>
                        <SearchResultsText>Grade</SearchResultsText>
                        {gradeSort && (
                            <FontAwesomeIcon
                                icon={reverse ? faCaretUp : faCaretDown}
                                color={reverse ? "crimson" : "green"}
                            ></FontAwesomeIcon>
                        )}
                    </SearchResultsCellHeading>
                )}
                {location === "/addassignment" && (
                    <SearchResultsCellHeading>
                        <SearchResultsText>Add</SearchResultsText>
                    </SearchResultsCellHeading>
                )}
                {location === "/searchresults" && (
                    <SearchResultsCellHeading col="2" onClick={progressHandler}>
                        <SearchResultsText>{isMobile ? "Prog." : "Progress"}</SearchResultsText>
                        {progressSort && (
                            <FontAwesomeIcon
                                icon={reverse ? faCaretUp : faCaretDown}
                                color={reverse ? "crimson" : "green"}
                            ></FontAwesomeIcon>
                        )}
                    </SearchResultsCellHeading>
                )}
                {location === "/searchresults" && (
                    <SearchResultsCellHeading onClick={lessonHandler}>
                        <>{isMobile ? "Les." : "Lessons"}</>
                        {lessonSort && (
                            <FontAwesomeIcon
                                icon={reverse ? faCaretUp : faCaretDown}
                                color={reverse ? "crimson" : "green"}
                            ></FontAwesomeIcon>
                        )}
                    </SearchResultsCellHeading>
                )}
            </SearchResultsHeading>
            {returnResults}
        </SearchResultsGrid>
    );
};

export const SearchResultsGrid = styled(TitleBoxDuoColorListContainer)`
    border: 1px solid #bbb;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    border-top: 0;
`;

export const SearchResultsEntry = styled(TitleBoxDouColorListEntry)`
    display: grid;
    grid-template-columns: repeat(11, 1fr);
    font-family: "PT Sans", sans-serif;
    text-align: center;
    padding: 0;
    background-color: ${darken(0.02, colors.secondaryColor)};
    &.empty {
        display: block;
        padding: 0.5rem 1rem;
        font-size: 1.3rem;
    }

    &:nth-child(even) {
        background-color: ${lighten(0.02, colors.secondaryColor)};
    }

    &:last-child {
        padding-bottom: 0;
    }

    ${below.m`
        font-size: 1.3rem;
    `}
`;

export const SearchResultsCell = styled.div`
    border-right: 1px solid rgba(0, 0, 0, 0.1);
    padding: 0.7rem 0.7rem;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.3rem;
    text-overflow: ellipsis;
    grid-column: span ${(props) => (props.col ? props.col : 1)};

    a {
        text-decoration: none;
    }

    &.clickable {
        cursor: pointer;
    }

    &:last-child {
        border-right: 0;
    }

    ${below.m`
    padding: 0.4rem 0.5rem;
    `}
`;

export const SearchResultsHeading = styled(SearchResultsEntry)`
    border-bottom: 3px solid ${colors.primaryColor};
    background-color: #e6e6e6;
    cursor: pointer;
    padding: 0;
    svg {
        margin-left: 0.33rem;
    }
`;

export const SearchResultsCellHeading = styled(SearchResultsCell)`
    border-right: 1px solid rgba(black, 0.15);
    font-weight: 600;
    font-size: 1.5rem;
    border-bottom: 0;
    padding: 0.6rem 0.7rem;
    ${below.m`
    padding: .33rem .5rem;
    `}
    &:last-child {
        border-right: 0;
    }
`;

export const SearchResultsText = styled.p`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    &.no-shorten {
        white-space: normal;
        overflow: visible;
    }

    &.completed-dark {
        color: #37971a;
    }

    &.notstarted {
        color: #4fa3a3;
    }

    &.inprogress {
        color: #d17a28;
    }

    &.completed {
        color: #47b924;
    }
`;

export default SearchResults;
