import { css } from "styled-components";

export const flex = ({ justifyContent = "flex-start", alignItems = "flex-start" }) => {
    return css`
        display: flex;
        justify-content: ${justifyContent};
        align-items: ${alignItems};
    `;
};
