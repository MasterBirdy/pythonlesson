import React, { useState } from "react";
import ForgotPasswordForm from "../components/forgotpassword/ForgotPasswordForm";
import axios from "axios";
import { makeToast } from "../utils/helpers";
import {
    TitleBox,
    TitleBoxTitle,
    TitleBoxTitleContainer,
    TitleBoxContainer,
    Container,
    Errors,
} from "../elements/components/";

//eslint-disable-next-line
const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

const ForgotPassword = () => {
    const [email, setEmail] = useState("");
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState({
        email: false,
    });
    const [errorMessages, setErrorMessages] = useState([]);

    const validate = () => {
        const errorArray = [];
        const errorObject = {
            email: false,
        };
        if (!emailRegex.test(email)) {
            errorArray.push("You must put in a valid e-mail address!");
            errorObject.email = true;
        }
        setErrorMessages(errorArray);
        setErrors(errorObject);
        return errorArray.length === 0;
    };

    const formHandler = (e) => {
        e.preventDefault();
        setLoading(true);
        if (validate()) {
            axios({
                method: "POST",
                url: `${
                    process.env.NODE_ENV === "production" ? process.env.REACT_APP_API_URL : "http://localhost:5000"
                }/api/forgotpassword`,
                data: {
                    email: email.toLowerCase(),
                },
            })
                .then((res) => {
                    if (res.status === 200) {
                        makeToast("Password reset e-mail sent.");
                    } else {
                        throw new Error("error");
                    }
                })
                .catch((err) => {
                    if (err.response) {
                        setErrorMessages((prevState) => [...prevState, err.response.data.err]);
                    }
                })
                .finally(() => {
                    setLoading(false);
                });
        } else {
            setLoading(false);
        }
    };

    return (
        <Container>
            <TitleBox marginTop="2" maxWidth="40rem">
                <TitleBoxTitleContainer>
                    <TitleBoxTitle>Forgot Password</TitleBoxTitle>
                </TitleBoxTitleContainer>
                <TitleBoxContainer>
                    <ForgotPasswordForm
                        email={email}
                        setEmail={setEmail}
                        errors={errors}
                        submit={formHandler}
                        loading={loading}
                    ></ForgotPasswordForm>
                </TitleBoxContainer>
            </TitleBox>
            {errorMessages.length > 0 && (
                <Errors>
                    <h2>Errors</h2>
                    <ul>
                        {errorMessages.map((error) => (
                            <li>{error}</li>
                        ))}
                    </ul>
                </Errors>
            )}
        </Container>
    );
};

export default ForgotPassword;
