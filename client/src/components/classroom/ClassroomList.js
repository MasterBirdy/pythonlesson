import React from "react";
import ClassroomListEntry, { TitleBoxClassroomListEntry } from "../classroom/ClassroomListEntry";
import {
    TitleBoxDuoColorListContainer,
    TitleBoxDouColorListEntry,
    TitleBoxDouColorListText,
} from "../../elements/components";

import styled from "styled-components";

const ClassroomList = ({ yourStudents }) => {
    return (
        <TitleBoxDuoColorListContainer>
            {yourStudents.length !== 0 ? (
                yourStudents.map((student) => {
                    return (
                        <ClassroomListEntry
                            firstName={student.first_name}
                            lastName={student.last_name}
                            key={student.id}
                            id={student.id}
                        ></ClassroomListEntry>
                    );
                })
            ) : (
                <TitleBoxClassroomListEntry>
                    <TitleBoxDouColorListText>Looks like no students have been added!</TitleBoxDouColorListText>
                </TitleBoxClassroomListEntry>
            )}
        </TitleBoxDuoColorListContainer>
    );
};
export default ClassroomList;
