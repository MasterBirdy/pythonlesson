import React from "react";
import turtle from "../../images/turtleface.svg";
import Button from "../ui/Button";
import { useHistory } from "react-router-dom";

const ErrorComponent = ({ errorMessage }) => {
    const history = useHistory();
    return (
        <div className="not-found-page">
            <h1>Error!</h1>
            <p>{errorMessage}</p>
            <img src={turtle} alt="Turtle face"></img>
            <div>
                <Button aFunction={() => history.goBack()}>Go Back</Button>
            </div>
        </div>
    );
};

export default ErrorComponent;
