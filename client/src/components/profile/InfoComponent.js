import React from "react";

import avatar1male from "../../images/avatar-male-01a-resized.png";
import avatar2male from "../../images/avatar-male-02a-resized.png";
import avatar3male from "../../images/avatar-male-03a-resized.png";
import avatar4male from "../../images/avatar-male-04a-resized.png";
import avatar1female from "../../images/avatar-female-01a-resized.png";
import avatar2female from "../../images/avatar-female-02a-resized.png";
import avatar3female from "../../images/avatar-female-03a-resized.png";
import avatar4female from "../../images/avatar-female-04a-resized.png";
import bat from "../../images/profile-bat-resized.png";
import fish from "../../images/profile-fish-resized.png";
import flamingo from "../../images/profile-flamingo-resized.png";
import goat from "../../images/profile-goat-resized.png";
import owl from "../../images/profile-owl-resized.png";
import panda from "../../images/profile-panda-resized.png";
import raccoon from "../../images/profile-raccoon-resized.png";
import sloth from "../../images/profile-sloth-resized.png";
import tiger from "../../images/profile-tiger-resized.png";
import turtle from "../../images/profile-turtle-resized.png";
import XPBar from "./XPBar";
import styled from "styled-components";

const avatarLoader = {
    0: avatar1male,
    1: avatar2male,
    2: avatar3male,
    3: avatar4male,
    4: avatar1female,
    5: avatar2female,
    6: avatar3female,
    7: avatar4female,
    8: bat,
    9: fish,
    10: flamingo,
    11: goat,
    12: owl,
    13: panda,
    14: raccoon,
    15: sloth,
    16: tiger,
    17: turtle,
};

const InfoComponent = ({
    avatar,
    firstName,
    lastName,
    level,
    currentXP,
    minXPNeeded,
    maxXPLastLevel,
    role,
    grade,
    schoolName,
    altMinBox,
    lessonsCompleted,
    unitsCompleted,
}) => {
    return (
        <InfoComponentContainer>
            {avatar > -1 && <InfoComponentAvatar src={avatarLoader[avatar]} alt="Chosen avatar"></InfoComponentAvatar>}
            <InfoComponentText className="name">
                {firstName} {lastName}
            </InfoComponentText>
            <InfoComponentText className="level">Level {level}</InfoComponentText>
            <XPBar current={currentXP} minNeeded={minXPNeeded} maxXPLastLevel={maxXPLastLevel}></XPBar>
            <InfoComponentText className="role">{role.slice(0, 1).toUpperCase() + role.slice(1)}</InfoComponentText>
            <InfoComponentBox>
                {!altMinBox && (
                    <InfoComponentMinibox>
                        {grade && role === "student" && (
                            <InfoComponentMiniboxEntry>
                                <span className="info-label">Grade: </span>
                                <p>{grade}</p>
                            </InfoComponentMiniboxEntry>
                        )}
                        {schoolName && (
                            <InfoComponentMiniboxEntry>
                                <span className="info-label">School: </span>
                                <p>{schoolName}</p>
                            </InfoComponentMiniboxEntry>
                        )}
                    </InfoComponentMinibox>
                )}
                {altMinBox && (
                    <InfoComponentMinibox>
                        <InfoComponentMiniboxEntry>
                            <span className="info-label">Lessons Completed:</span> <p>{lessonsCompleted}</p>
                        </InfoComponentMiniboxEntry>
                        <InfoComponentMiniboxEntry>
                            <span className="info-label">Units Completed:</span> <p>{unitsCompleted}</p>
                        </InfoComponentMiniboxEntry>
                    </InfoComponentMinibox>
                )}
            </InfoComponentBox>
        </InfoComponentContainer>
    );
};

const InfoComponentContainer = styled.div`
    font-family: "PT Sans", sans-serif;
    padding: 1rem;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const InfoComponentAvatar = styled.img`
    max-width: 17.5rem;
    width: 100%;
    margin-bottom: 0.5rem;
`;

const InfoComponentText = styled.p`
    &.name {
        font-size: 1.75rem;
        font-weight: 600;
    }

    &.level {
        font-size: 1.3rem;
        margin-bottom: 0.25rem;
    }

    &.role {
        margin-top: 0.33rem;
        font-size: 1.125rem;
        color: grey;
    }
`;

const InfoComponentBox = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    max-width: 250px;
`;

const InfoComponentMinibox = styled.div`
    margin-top: 0.5rem;
    max-width: 200px;
    font-family: "PT Sans", sans-serif;
    background-color: rgb(247, 247, 247);
    border: 1px solid rgba(209, 209, 209, 0.8);
    box-shadow: 5px 10px 20px -18px rgba(172, 172, 172, 0.541);
    font-size: 1.2125rem;
    padding: 0.75rem 1.25rem;
    border-radius: 10px;
`;

const InfoComponentMiniboxEntry = styled.div`
    display: flex;
    margin-bottom: 0.3em;

    .info-label {
        font-weight: 600;
        color: rgb(32, 32, 32);
        margin-right: 0.4rem;
    }

    p {
        color: rgb(26, 26, 26);
        flex-grow: 1;
    }
`;
export default InfoComponent;
