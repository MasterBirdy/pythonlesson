// const Sequelize = require("sequelize");
// const sequelize = new Sequelize("hapimonkey", "root", "", {
//     host: "localhost",
//     dialect: "mysql",
//     operatorAlises: false,
//     pool: {
//         max: 5,
//         min: 0,
//         acquire: 30000,
//         idle: 0,
//     },
// });
require("dotenv").config();

console.log("TESTING TESTING", process.env.NODE_ENV);
const environment = process.env.NODE_ENV || "development";
const config = require("../knexfile.js")[environment];
module.exports = require("knex")(config);
