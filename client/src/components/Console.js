import React from "react";
import SectionToolbar from "./SectionToolbar";
import PythonBlock from "./PythonBlock";

const Console = ({ value, setValue, consoleRef, openConsoleToolbar, openConsoleHandler, isError, setIsError }) => {
    return (
        <div
            onClick={
                setIsError
                    ? () => {
                          setIsError(false);
                      }
                    : () => {}
            }
        >
            <SectionToolbar
                name={"Console"}
                type={"consoleToolbar"}
                isOpen={openConsoleToolbar}
                openToolbarHandler={openConsoleHandler}
                isError={isError}
                setIsError={setIsError}
            ></SectionToolbar>
            <PythonBlock
                value={value}
                setValue={setValue}
                options={{
                    mode: "python",
                    theme: "base16-dark",
                }}
                grow={false}
                consoleRef={consoleRef}
            ></PythonBlock>
        </div>
    );
};

export default Console;
