import styled from "styled-components";

export const Button = styled.button``;

export const ButtonHelper = styled.div`
    ${Button}:not(:last-child) {
        margin-right: 0.5rem;
    }
`;
