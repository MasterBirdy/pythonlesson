import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserEdit, faSmile, faChalkboardTeacher } from "@fortawesome/free-solid-svg-icons";
import { TitleBoxDouColorListEntry, TitleBoxDouColorListText, IconsContainer } from "../../elements/components";
import styled from "styled-components";

const ClassroomListEntry = ({ firstName, lastName, id }) => {
    return (
        <TitleBoxClassroomListEntry>
            <TitleBoxDouColorListText>{firstName + " " + lastName}</TitleBoxDouColorListText>
            <IconsContainer>
                <Link to={`/profile/${id}`} title="View Student Profile">
                    <FontAwesomeIcon icon={faSmile} size="lg"></FontAwesomeIcon>
                </Link>
                <Link to={`/editstudent/${id}`} title="Edit Student Profile">
                    <FontAwesomeIcon icon={faUserEdit} size="lg"></FontAwesomeIcon>
                </Link>
                <Link to={`/viewstudent/${id}`} title="View Student Stats">
                    <FontAwesomeIcon icon={faChalkboardTeacher} size="lg"></FontAwesomeIcon>
                </Link>
            </IconsContainer>
        </TitleBoxClassroomListEntry>
    );
};

export const TitleBoxClassroomListEntry = styled(TitleBoxDouColorListEntry)`
    display: flex;
    justify-content: space-between;
    &:last-child {
        border: 0;
        padding-bottom: 0.75rem;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
    }
`;

export default ClassroomListEntry;
